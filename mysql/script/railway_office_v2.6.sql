-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema railway_office
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema railway_office
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `railway_office` DEFAULT CHARACTER SET utf8 ;
USE `railway_office` ;

-- -----------------------------------------------------
-- Table `railway_office`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `railway_office`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(64) NOT NULL,
  `password` VARCHAR(32) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `firstname` VARCHAR(64) NOT NULL,
  `lastname` VARCHAR(64) NOT NULL,
  `phone` VARCHAR(45) NULL,
  `role` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `railway_office`.`train`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `railway_office`.`train` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `railway_office`.`coach`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `railway_office`.`coach` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(255) NOT NULL,
  `number` INT NOT NULL,
  `train_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_coach_train1_idx` (`train_id` ASC) INVISIBLE,
  CONSTRAINT `fk_coach_train1`
    FOREIGN KEY (`train_id`)
    REFERENCES `railway_office`.`train` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `railway_office`.`seat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `railway_office`.`seat` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number` INT NOT NULL,
  `coach_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_seats_coach_idx` (`coach_id` ASC) VISIBLE,
  CONSTRAINT `fk_seats_coach`
    FOREIGN KEY (`coach_id`)
    REFERENCES `railway_office`.`coach` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `railway_office`.`station`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `railway_office`.`station` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `railway_office`.`route`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `railway_office`.`route` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `arrival` BIGINT NOT NULL,
  `departure` BIGINT NOT NULL,
  `train_id` INT NOT NULL,
  `from_id` INT NOT NULL,
  `to_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_route_train1_idx` (`train_id` ASC) VISIBLE,
  INDEX `fk_route_station1_idx` (`from_id` ASC) VISIBLE,
  INDEX `fk_route_station2_idx` (`to_id` ASC) VISIBLE,
  CONSTRAINT `fk_route_train1`
    FOREIGN KEY (`train_id`)
    REFERENCES `railway_office`.`train` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_route_station1`
    FOREIGN KEY (`from_id`)
    REFERENCES `railway_office`.`station` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_route_station2`
    FOREIGN KEY (`to_id`)
    REFERENCES `railway_office`.`station` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `railway_office`.`seat_booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `railway_office`.`seat_booking` (
  `route_id` INT NOT NULL,
  `seat_id` INT NOT NULL,
  `seat_status` VARCHAR(45) NOT NULL,
  `seat_price` INT NOT NULL,
  `coach_id` INT NOT NULL,
  PRIMARY KEY (`route_id`, `seat_id`),
  INDEX `fk_seat_has_route_route1_idx` (`route_id` ASC) VISIBLE,
  INDEX `fk_seat_has_route_seat1_idx` (`seat_id` ASC) VISIBLE,
  CONSTRAINT `fk_seat_has_route_seat1`
    FOREIGN KEY (`seat_id`)
    REFERENCES `railway_office`.`seat` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_seat_has_route_route1`
    FOREIGN KEY (`route_id`)
    REFERENCES `railway_office`.`route` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


-- -----------------------------------------------------
-- Table `railway_office`.`ticket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `railway_office`.`ticket` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(255) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  `ticket_type` VARCHAR(45) NOT NULL,
  `price` INT NOT NULL,
  `seat_booking_route_id` INT NOT NULL,
  `seat_booking_seat_id` INT NOT NULL,
  `coach` INT NOT NULL,
  `seat` INT NOT NULL,
  `user_id` INT NOT NULL,
  `time_stamp` BIGINT NOT NULL,
  PRIMARY KEY (`id`, `firstname`),
  INDEX `fk_passenger_seat_booking1_idx` (`seat_booking_route_id` ASC, `seat_booking_seat_id` ASC) VISIBLE,
  INDEX `fk_passenger_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_passenger_seat_booking1`
    FOREIGN KEY (`seat_booking_route_id` , `seat_booking_seat_id`)
    REFERENCES `railway_office`.`seat_booking` (`route_id` , `seat_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_passenger_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `railway_office`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
