<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    isELIgnored="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>

<html>
	<head>
		<meta charset="utf-8">
		<title><fmt:message key="registration.title" bundle="${lang}"/></title>
		<link rel="stylesheet" type="text/css" href="css/railway_office.css" />
	</head>
	<body>
	<jsp:include page="header.jsp" />
	<div class="body">
	    <div class="content">
		<div class="reg-form">
		<form method="post" action="${pageContext.request.contextPath}/reg">
			<table class="reg-table">
			<tr>
			<td class="reg-table-label"><label for="login"><fmt:message key="registration.login" bundle="${lang}"/>* </label></td>
			<td class="reg-table-input"><input id="login" type="text" name="login" required pattern="[\w\d]{1,30}" /><br/></td>
			</tr>
			<tr>
			<td class="reg-table-label"><label for="password"><fmt:message key="registration.password" bundle="${lang}"/>* </label></td>
			<td class="reg-table-input"><input id="password" type="password" name="password" required pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,20}$" /><br/></td>
			</tr>
			<tr>
			<td class="reg-table-label"><label for="firstname"><fmt:message key="registration.firstname" bundle="${lang}"/>* </label></td>
			<td class="reg-table-input"><input id="firstname" type="text" name="firstname" required pattern="[A-Z][a-z]{0,63}" /><br/></td>
			</tr>
			<tr>
			<td class="reg-table-label"><label for="lastname"><fmt:message key="registration.lastname" bundle="${lang}"/>* </label></td>
			<td class="reg-table-input"><input id="lastname" type="text" name="lastname" required pattern="[A-Z][a-z]{0,63}" /><br/></td>
			</tr>
			<tr>
			<td class="reg-table-label"><label for="email"><fmt:message key="registration.email" bundle="${lang}"/>* </label></td>
			<td class="reg-table-input"><input id="email" type="text" name="email" required pattern="^[a-zA-Z\d_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z\d.-]+$" /><br/></td>
			</tr>
			<tr>
			<td class="reg-table-label"><label for="phone"><fmt:message key="registration.phone" bundle="${lang}"/> </label></td>
			<td class="reg-table-input"><input id="phone" type="text" name="phone" pattern="\+\d{12}" /><br/></td>
			</tr>
			</table>
			${requestScope.message}<br/>
			<input type="submit" value=<fmt:message key="registration.register" bundle="${lang}"/>>
		</form>
		</div>
		<div style="text-align: center;">
		    <br>
		    <br>
		    <a href="${pageContext.request.contextPath}/auth"><fmt:message key="registration.link.auth" bundle="${lang}"/></a>
		</div>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
	</body>
</html>