<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ml" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>
<html>
	<head>
		<meta charset="utf-8">
		<title><fmt:message key="stations.title" bundle="${lang}"/></title>
		<link rel="stylesheet" type="text/css" href="../css/custom_table.css" />
        <link rel="stylesheet" type="text/css" href="../css/modal_window.css" />
        <link rel="stylesheet" type="text/css" href="../css/railway_office.css" />
	</head>
	<body>
	<jsp:include page="../header.jsp" />
	<div class="body">
	    <div class="content">
        <div>
            <form action="${pageContext.request.contextPath}/admin_edit/stations" method="post">
            <table class="table">
            <tr>
            <c:choose>
            <c:when test="${requestScope.action=='edit'}">
            <td class="common-td">
                <a href="#openModal" disabled><div align="center" >+ <fmt:message key="stations.add" bundle="${lang}"/></div><a/>
            </td>
            <td class="common-td">
                <input type="submit" name="action" value="commit" />
                <input type="submit" name="action" value="cancel" />
            </td>
            </c:when>
            <c:otherwise>
            <td class="common-td">
                <a href="#openModal"><div align="center" >+ <fmt:message key="stations.add" bundle="${lang}"/></div><a/>
            </td>
            <td class="common-td">
                <input type="submit" name="action" value="edit" />
                <input type="submit" name="action" value="delete" />
            </td>
            </c:otherwise>
            </c:choose>
            </tr>
            <tr>
            <td class="head-td" style="width:360px;"><fmt:message key="stations.name" bundle="${lang}"/></td>
            <td class="head-td" style="width:70px;"></td>
            </tr>
            <c:choose>
            <c:when test="${action=='edit'}">
                <ml:stedit stationList="${requestScope.stationList}" changes="${requestScope.changeList}" />
            </c:when>
            <c:otherwise>
                <ml:stcontent stationList="${requestScope.stationList}" />
            </c:otherwise>
            </c:choose>
            </table>
            </form>
        </div>
        <div>
			<fmt:message var="var1" key="stations.add" bundle="${lang}"/>
            <ml:modal mheader="${var1}">
            <form action="${pageContext.request.contextPath}/admin_edit/stations" method="post">
            <input type="hidden" name="action" value="add" />
            <p><fmt:message key="stations.add.name" bundle="${lang}"/>: <input id="name" type="text" name="name" required pattern="[\w\d]{1,30}" /></p>
            <p><input type="submit" value=<fmt:message key="stations.add.create" bundle="${lang}"/> /></p>
            </form>
            </ml:modal>
        </div>
        </div>
    </div>
	<jsp:include page="../footer.jsp" />
	</body>
</html>