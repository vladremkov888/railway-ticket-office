<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ml" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>
<html>
	<head>
		<meta charset="utf-8">
		<title><fmt:message key="trains.title" bundle="${lang}"/></title>
		<link rel="stylesheet" type="text/css" href="../css/custom_table.css" />
        <link rel="stylesheet" type="text/css" href="../css/modal_window.css" />
        <link rel="stylesheet" type="text/css" href="../css/railway_office.css" />
		<script>
		function addcoach() {
		var completelist= document.getElementById("list");
		var form= document.getElementById("form");

		completelist.innerHTML += "<li>" + document.getElementById('input').value + "</li>";

		form.innerHTML += "<input type=\"hidden\" name=\"coach\" value=\""+document.getElementById('input').value+"\"/>";
		}
		</script>
	</head>
	<body>
	<jsp:include page="../header.jsp" />
	<div class="body">
	    <div class="content">
		<div>
            <form action="${pageContext.request.contextPath}/admin_edit/trains" method="post">
            <table class="table">
            <tr>
            <td class="common-td" colspan="2">
                <a href="#openModal"><div align="center" >+ <fmt:message key="trains.add" bundle="${lang}"/></div><a/>
            </td>
            <td class="common-td">
                <input type="submit" name="action" value="delete" />
            </td>
            </tr>
            <tr>
            <td class="head-td" style="width:120px;"><fmt:message key="trains.name" bundle="${lang}"/></td>
			<td class="head-td" style="width:360px;"><fmt:message key="trains.coaches" bundle="${lang}"/></td>
            <td class="head-td" style="width:70px;"></td>
            </tr>
            <ml:ttcontent trainList="${requestScope.trainList}" />
            </table>
            </form>
        </div>
        <div>
			<fmt:message var="var1" key="trains.add" bundle="${lang}"/>
            <ml:modal mheader="${var1}">
			<div>
				<select name="Coach" id="input">
				<option value="Berth"><fmt:message key="trains.berth" bundle="${lang}"/></option>
				<option value="Compartment"><fmt:message key="trains.compartment" bundle="${lang}"/></option>
				<option value="DeLuxe"><fmt:message key="trains.deluxe" bundle="${lang}"/></option>
				</select>
				<button onclick="addcoach();"><fmt:message key="trains.add_coach" bundle="${lang}"/></button>

				<br/>
				<ul id="list">
				</ul>
				<form action="${pageContext.request.contextPath}/admin_edit/trains" method="post" id="form">
				<input type="hidden" name="action" value="add" />
				<p><fmt:message key="trains.name" bundle="${lang}"/>: <input id="name" type="text" name="name" required pattern="\w\d{3}" /></p>
				<p><input type="submit" value=<fmt:message key="trains.create" bundle="${lang}"/> /></p>
				</form>
			</div>
            </ml:modal>
        </div>
        </div>
    </div>
	<jsp:include page="../footer.jsp" />
	</body>
</html>