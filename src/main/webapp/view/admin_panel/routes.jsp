<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ml" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="mm" tagdir="/WEB-INF/tags/tagz"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>
<html>
	<head>
		<meta charset="utf-8">
		<title><fmt:message key="routes.title" bundle="${lang}"/></title>
		<link rel="stylesheet" type="text/css" href="../css/custom_table.css" />
        <link rel="stylesheet" type="text/css" href="../css/modal_window.css" />
        <link rel="stylesheet" type="text/css" href="../css/railway_office.css" />
	</head>
	<body>
	<jsp:include page="../header.jsp" />
	<div class="body">
	    <div class="content">
        <div>
            <form action="${pageContext.request.contextPath}/admin_edit/routes" method="post">
            <table class="table">
            <tr>
            <c:choose>
            <c:when test="${requestScope.action=='edit'}">
            <td colspan="5">
                <a href="#openModal" disabled><div align="center" >+ <fmt:message key="routes.add" bundle="${lang}"/></div><a/>
            </td>
            <td class="common-td">
                <input type="submit" name="action" value="commit" />
                <input type="submit" name="action" value="cancel" />
            </td>
            </c:when>
            <c:otherwise>
            <td colspan="5">
                <a href="#openModal"><div align="center" >+ <fmt:message key="routes.add" bundle="${lang}"/></div><a/>
            </td>
            <td class="common-td">
                <input type="submit" name="action" value="edit" />
                <input type="submit" name="action" value="delete" />
            </td>
            </c:otherwise>
            </c:choose>
            </tr>
            <tr>
            <td class="head-td" style="width:200px;"><fmt:message key="routes.departure" bundle="${lang}"/></td>
			<td class="head-td" style="width:200px;"><fmt:message key="routes.arrival" bundle="${lang}"/></td>
            <td class="head-td" style="width:100x;"><fmt:message key="routes.train" bundle="${lang}"/></td>
            <td class="head-td" style="width:160px;"><fmt:message key="routes.from" bundle="${lang}"/></td>
            <td class="head-td" style="width:160px;"><fmt:message key="routes.to" bundle="${lang}"/></td>
            <td class="head-td" style="width:70px;"></td>
            </tr>
            <c:choose>
            <c:when test="${action=='edit'}">
                <ml:rtedit routeList="${requestScope.routeList}" stationList="${requestScope.stationList}" trainList="${requestScope.trainList}" changes="${requestScope.changeList}" />
            </c:when>
            <c:otherwise>
                <ml:rtcontent routeList="${requestScope.routeList}" />
            </c:otherwise>
            </c:choose>
            </table>
            </form>
        </div>
        <div>
			<fmt:message var="var1" key="routes.add" bundle="${lang}"/>
            <ml:modal mheader="${var1}">
            <form action="${pageContext.request.contextPath}/admin_edit/routes" method="post">
            <input type="hidden" name="action" value="add" />
            <p><label for="departure"><fmt:message key="routes.add.departure" bundle="${lang}"/>:</label>
            <input id="departure" type="datetime-local" name="departure" required /></p>
			<p><label for="arrival"><fmt:message key="routes.add.arrival" bundle="${lang}"/>:</label>
            <input id="arrival" type="datetime-local" name="arrival" required /></p>
            <p><label for="train"><fmt:message key="routes.add.train" bundle="${lang}"/>:</label>
            <mm:tselect trainList="${trainList}" /></p>
            <p><label for="from"><fmt:message key="routes.add.start_station" bundle="${lang}"/>:</label>
            <mm:sselect name="from" stationList="${stationList}" /></p>
            <p><label for="to"><fmt:message key="routes.add.finish_station" bundle="${lang}"/>:</label>
            <mm:sselect name="to" stationList="${stationList}" /></p>
            <p><input type="submit" value=<fmt:message key="routes.add.create" bundle="${lang}"/> /></p>
            </form>
            </ml:modal>
        </div>
        </div>
    </div>
	<jsp:include page="../footer.jsp" />
	</body>
</html>