<%@ page contentType="text/html; charset=UTF-8" language="java"
    pageEncoding="UTF-8"
    isELIgnored="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ml" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>

<html>
	<head>
		<title><fmt:message key="booking.title" bundle="${lang}"/></title>
		<link rel="stylesheet" type="text/css" href="css/railway_office.css" />
		<link rel="stylesheet" type="text/css" href="css/custom_table.css" />
	</head>
	<body>
	<jsp:include page="header.jsp" />
	<div class="body">
		<div class="booking-flex">
		<div class="booking-search-form">
		<form method="get" action="${pageContext.request.contextPath}/book">
		    <input type="hidden" name="action" value="viewAll"/>
			<input type="text" name="start" required pattern="[\w\d]{1,30}" />
			<input type="text" name="finish" required pattern="[\w\d]{1,30}" />
			<input type="date" name="date"/>
			<input type="submit" value=<fmt:message key="booking.search" bundle="${lang}"/>/>
		</form>
		</div>
		<c:choose>
		<c:when test="${action=='viewAll'}">
		<div class="booking-results">
		<table class="table">
			<tr>
			<td class="head-td" style="width:120x;"><fmt:message key="booking.train" bundle="${lang}"/></td>
            <td class="head-td" style="width:230px;"><fmt:message key="booking.departure" bundle="${lang}"/></td>
			<td class="head-td" style="width:80px;"><fmt:message key="booking.travel_time" bundle="${lang}"/></td>
			<td class="head-td" style="width:230px;"><fmt:message key="booking.arrival" bundle="${lang}"/></td>
            <td class="head-td" style="width:120px;"><fmt:message key="booking.seats" bundle="${lang}"/></td>
			<td class="head-td" style="width:110px;"><fmt:message key="booking.price" bundle="${lang}"/></td>
            <td class="head-td" style="width:60px;"></td>
            </tr>
			<ml:utable routeList="${routeList}" />
		</table>
		</div>
		</c:when>
		<c:when test="${action=='view'}">
		<div class="booking-view">
		<table class="table">
			<tr>
			<td class="head-td" style="width:140x;"><fmt:message key="booking.train" bundle="${lang}"/></td>
            <td class="head-td" style="width:300px;"><fmt:message key="booking.departure" bundle="${lang}"/></td>
			<td class="head-td" style="width:150px;"><fmt:message key="booking.travel_time" bundle="${lang}"/></td>
			<td class="head-td" style="width:300px;"><fmt:message key="booking.arrival" bundle="${lang}"/></td>
			</tr>
			<tr>
			<td class="common-td">${route.getTrainName()}</td>
			<td class="common-td">${route.getFromName()}   ${route.getDepartureTime()}</td>
			<td class="common-td">${route.getTravelTime()}</td>
			<td class="common-td">${route.getToName()}   ${route.getArrivalTime()}</td>
			</td>
			</tr>
		</table>
		</div>
		<form action="${pageContext.request.contextPath}/book" method="post">
		<div class="booking-tickets">
		<p style="margin:10px; padding:10px"> <fmt:message key="booking.wagon" bundle="${lang}"/>: ${coach.getType()}</p>
		<div class="booking-wagons">
		<div class="list">
		<c:forEach var="x" items="${route.getCoaches()}">
		<a class="booking-wagon" href="${pageContext.request.contextPath}/book?action=view&id=${route.getId()}&coach=${x.getId()}">${x.getNumber()}</a>
		</c:forEach>
		</div>
		</div>
		<div class="booking-results">
		<c:choose>
		<c:when test="${route.getCoach().getType()=='Berth'}">
		<div class="booking-coach-container" style="width:800px; height:160px;">
		<ml:dBerth coach="${route.getCoach()}" />
		</div>
		</c:when>
		<c:when test="${route.getCoach().getType()=='Compartment'}">
		<div class="booking-coach-container" style="width:800px; height:120px;">
		<ml:dCompartment coach="${route.getCoach()}" />
		</div>
		</c:when>
		<c:when test="${route.getCoach().getType()=='DeLuxe'}">
		<div class="booking-coach-container" style="width:850px; height:100px;">
		<ml:dDeLuxe coach="${route.getCoach()}" />
		</div>
		</c:when>
		</c:choose>
		</div>
		<p style="color:red;">${message}</p>
		<input type="hidden" name="route_id" value="${route.getId()}" />
		<input type="submit" value="Buy tickets" class="booking-button" />
		</div>
		</form>
		</c:when>
		</c:choose>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
	</body>
</html>