<%@ page contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>
<header>
	<div class="header-flex">
	<div class="header-logo">
    <strong>My<br/>Company</strong>
	</div>
    <div class="header-blank">
	</div>
	<div class="header-contacts">
	<p><fmt:message key="header.support" bundle="${lang}"/><br/>+380000000000</p>
	</div>
	<div class="header-mini-1">
	<c:choose>
        <c:when test="${sessionScope.user==null}">
            <a href="${pageContext.request.contextPath}/auth"><img class="header-icon" src="/railway-ticket-office/icons/log_in.png" alt=<fmt:message key="header.log_in" bundle="${lang}"/>></a>
        </c:when>
        <c:otherwise>
			<c:choose>
			<c:when test="${sessionScope.user.getRole()=='admin'}">
				<a href="${pageContext.request.contextPath}/admin_edit"><img class="header-icon" src="/railway-ticket-office/icons/gear.png" alt=<fmt:message key="header.management" bundle="${lang}"/>></a>
			</c:when>
			<c:otherwise>
				<a href="${pageContext.request.contextPath}/cabinet/cart"><img class="header-icon" src="/railway-ticket-office/icons/cart.png" alt=<fmt:message key="header.cart" bundle="${lang}"/>></a>
			</c:otherwise>
			</c:choose>
        </c:otherwise>
    </c:choose>
	
	</div>
	<div class="header-mini-2">
	<a href="${pageContext.request.contextPath}/cabinet"><img class="header-icon" src="/railway-ticket-office/icons/profile.png" alt=<fmt:message key="header.profile" bundle="${lang}"/>></a>
	</div>
	<div class="header-mini-3">
	<form action="${pageContext.request.contextPath}/do" method="post">
		<input type="hidden" name="command" value="lang"/>
		<div class="dropdown">
		<button style="background-color: #87ceeb;"><fmt:message key="header.lang.short" bundle="${lang}"/></button>
		<div class="dropdown-content">
			<input type="submit" name="lang" value="en"/>
			<input type="submit" name="lang" value="ua"/>
		</div>
		</div>
	</form>
	</div>
	</div>
</header>