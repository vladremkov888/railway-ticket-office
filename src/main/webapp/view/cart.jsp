<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ml" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>
<html>
	<head>
		<meta charset="utf-8">
		<title><fmt:message key="cart.title" bundle="${lang}"/></title>
		<link rel="stylesheet" type="text/css" href="/railway-ticket-office/css/railway_office.css" />
		<link rel="stylesheet" type="text/css" href="/railway-ticket-office/css/custom_table.css" />
	</head>
	<body>
	<jsp:include page="header.jsp" />
	<div class="body">
	    <div class="content">
		<div class="cabinet-container">
			<c:choose>
			<c:when test="${sessionScope.user==null}">
				<p><fmt:message key="cart.message.log_in" bundle="${lang}"/></p>
			</c:when>
			<c:when test="${sessionScope.tickets.size()>0}">
				<ml:cart ticketList="${sessionScope.tickets}" />
			</c:when>
			</c:choose>
			<div>
			<form action="${pageContext.request.contextPath}/cabinet/cart" method="post">
			<input type="hidden" name="action" value="pay" />
			<input type="submit" value=<fmt:message key="cart.pay" bundle="${lang}"/> class="pay-button" />
			</form>
			</div>
		</div>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
	</body>
</html>