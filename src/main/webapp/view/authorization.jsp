<%@ page contentType="text/html; charset=UTF-8" language="java"
    pageEncoding="UTF-8"
    isELIgnored="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>

<html>
	<head>
		<title><fmt:message key="authorization.title" bundle="${lang}"/></title>
		<link rel="stylesheet" type="text/css" href="css/railway_office.css" />
	</head>
	<body>
	<jsp:include page="header.jsp" />
	<div class="body">
	    <div class="content">
		<div class="reg-form">
		<form method="post" action="${pageContext.request.contextPath}/auth">
			<input type="text" name="login" required /><br/>
			<input type="password" name="password" required /><br/>
			<input type="submit" value=<fmt:message key="authorization.log_in" bundle="${lang}"/> />
		</form>
		<div>
			<p>
				${requestScope.message}
			</p>
		</div>
		<div style="text-align: center;">
		    <br>
		    <br>
		    <a href="${pageContext.request.contextPath}/reg"><fmt:message key="authorization.link.register" bundle="${lang}"/></a>
		</div>
		</div>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
	</body>
</html>