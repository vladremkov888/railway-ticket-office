<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ml" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>
<html>
	<head>
		<meta charset="utf-8">
		<title><fmt:message key="message.error.title" bundle="${lang}"/></title>
		<link rel="stylesheet" type="text/css" href="/railway-ticket-office/css/railway_office.css" />
	</head>
	<body>
	<jsp:include page="header.jsp" />
	<div class="body">
	    <div class="content">
			<fmt:message key="${message}" bundle="${lang}"/>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
	</body>
</html>