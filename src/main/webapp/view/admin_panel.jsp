<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ml" tagdir="/WEB-INF/tags"%>
<html>
	<head>
		<meta charset="utf-8">
		<title>Stations and routes administration</title>
		<link rel="stylesheet" type="text/css" href="css/railway_office.css" />
	</head>
	<body>
	<jsp:include page="header.jsp" />
	<div class="body">
	    <div class="content">
		<a href="${pageContext.request.contextPath}/admin_edit?action=edit_stations"> 
		<button>
			Stations list
		</button>
		</a>
		<a href="${pageContext.request.contextPath}/admin_edit?action=edit_routes"> 
		<button>
			Routes list
		</button>
		</a>
		<a href="${pageContext.request.contextPath}/admin_edit?action=view_trains"> 
		<button>
			Trains list
		</button>
		</a>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
	</body>
</html>