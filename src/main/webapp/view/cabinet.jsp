<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ml" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>
<html>
	<head>
		<meta charset="utf-8">
		<title><fmt:message key="cabinet.title" bundle="${lang}"/></title>
		<link rel="stylesheet" type="text/css" href="css/railway_office.css" />
	</head>
	<body>
	<jsp:include page="header.jsp" />
	<div class="body">
	    <div class="content">
		<div>
		<a class="booking-button" href="${pageContext.request.contextPath}/book"><img class="cabinet-arrow" src="/railway-ticket-office/icons/back.png" alt=<fmt:message key="cabinet.back" bundle="${lang}"/>></a>
		</div>
		<div class="cabinet-container">
			<h3>${sessionScope.user.getLogin()}</h3>
			<table class="cabinet-table">
			<tr class="cabinet-table-collumn">
			<td style="height:50px;"><fmt:message key="cabinet.firstname" bundle="${lang}"/>:</td>
			<td style="height:50px;">${sessionScope.user.getFirstName()}</td>
			</tr>
			<tr class="cabinet-table-collumn">
			<td style="height:50px;"><fmt:message key="cabinet.lastname" bundle="${lang}"/>:</td>
			<td style="height:50px;">${sessionScope.user.getLastName()}</td>
			</tr>
			<tr class="cabinet-table-collumn">
			<td style="height:50px;"><fmt:message key="cabinet.email" bundle="${lang}"/>:</td>
			<td style="height:50px;">${sessionScope.user.getEmail()}</td>
			</tr>
			<tr class="cabinet-table-collumn">
			<td style="height:50px;"><fmt:message key="cabinet.phone" bundle="${lang}"/>:</td>
			<td style="height:50px;">${sessionScope.user.getPhoneNumber()}</td>
			</tr>
			</table>
		</div>
		<div style="morgin:40px; padding:10px;">
		<form method="post" action="${pageContext.request.contextPath}/cabinet">
		    <input type="hidden" name="out" value="out"/>
			<input type="submit" value=<fmt:message key="cabinet.log_out" bundle="${lang}"/>>
		</form>
		</div>
		<div>
		<a class="cabinet-button" href="${pageContext.request.contextPath}/cabinet?action=my_tickets"><fmt:message key="cabinet.my_tickets" bundle="${lang}"/></a>
		</div>
		</div>
	</div>
	<jsp:include page="footer.jsp" />
	</body>
</html>