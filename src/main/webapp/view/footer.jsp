<%@ page contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    isELIgnored="false" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>
<footer>
	<div class="footer-flex">
	<div class="footer-copyright">
		© My copyright
	</div>
	<div class="footer-blank">
	</div>
	<div class="footer-info">
		<u><fmt:message key="footer.about" bundle="${lang}"/><br/><fmt:message key="footer.info" bundle="${lang}"/></u>
	</div>
	</div>
</footer>