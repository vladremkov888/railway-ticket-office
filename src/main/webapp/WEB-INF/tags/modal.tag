<%@ tag pageEncoding='UTF-8' isELIgnored="false" %>

<%@ attribute name="mheader" type="java.lang.String" required="true" %>
<%@ attribute name="mindex" type="java.lang.Integer" required="false" %>

<div id="openModal${mindex}" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">${mheader}</h3>
        <a href="#close" title="Close" class="close">×</a>
      </div>
      <div class="modal-body">
		<jsp:doBody/>
      </div>
    </div>
  </div>
</div>