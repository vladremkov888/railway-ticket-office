<%@ tag pageEncoding='UTF-8' isELIgnored="false"%>
<%@ attribute name="coach" type="com.company.app.entity.Coach" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div style="display: inline-block;
    vertical-align: top;
    white-space: normal;">
<c:forEach var="i" begin="2" end="36" step="2">
<input type="checkbox" id="check_${i}" name="seat" value="${coach.getSeatList().get(i-1).getId()}">
<label for="check_${i}" style="min-width:18px; left:${10+(3*((i-2)/2))}px; top:5px;">${i}</label>
</c:forEach>
</div>

<div style="display: inline-block;
    vertical-align: top;
    white-space: normal;">
<c:forEach var="i" begin="1" end="35" step="2">
<input type="checkbox" id="check_${i}" name="seat" value="${coach.getSeatList().get(i-1).getId()}">
<label for="check_${i}" style="min-width:18px; left:${10+(3*((i-2)/2))}px; top:10px;">${i}</label>
</c:forEach>
</div>