<%@ tag pageEncoding='UTF-8' isELIgnored="false"%>
<%@ attribute name="stationList" type="java.util.List" required="true" %>
<%@ attribute name="changes" type="java.util.List" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:forEach var="x" items="${stationList}">
<tr>
	<c:choose>
		<c:when test="${changes.contains(x.getId())}">
			<td class="common-td">
			<input type="text" name="name" value="${x.getName()}" />
			</td>
			<td class="common-td">
			<input align="center" type="checkbox" name="checkbox" value="${x.getId()}" checked="checked" onclick="return false;" />
			</td>
		</c:when>
		<c:otherwise>
			<td class="common-td">
			${x.getName()}
			</td>
			<td class="common-td">
			<input align="center" type="checkbox" name="checkbox" value="${x.getName()}" disabled />
			</td>
		</c:otherwise>
	</c:choose>
</tr>
</c:forEach>