<%@ tag pageEncoding='UTF-8' isELIgnored="false"%>
<%@ attribute name="routeList" type="java.util.List" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:forEach var="x" items="${routeList}">
<tr>
	<td class="common-td">${x.getTrainName()}</td>
	<td class="common-td">${x.getFromName()}   ${x.getDepartureTime()}</td>
	<td class="common-td">${x.getTravelTime()}</td>
	<td class="common-td">${x.getToName()}   ${x.getArrivalTime()}</td>
	<td class="common-td">
	<c:forEach var="y" items="${x.getCoachesInfo().keySet()}">
		${y}: ${x.getCoachesInfo().get(y)}<br/>
	</c:forEach>
	</td>
	<td class="common-td">
	<c:forEach var="z" items="${x.getPriceList()}">
		${z}<br/>
	</c:forEach>
	</td>
	<td>
		<a href="${pageContext.request.contextPath}/book?action=view&id=${x.getId()}&coach=0"><img class="booking-arrow" src="/railway-ticket-office/icons/arrow.png" alt="My cart"></a>
	</td>
</tr>
</c:forEach>

