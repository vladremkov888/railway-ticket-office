<%@ tag pageEncoding='UTF-8' isELIgnored="false"%>
<%@ attribute name="routeList" type="java.util.List" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:forEach var="x" items="${routeList}">
<tr>
<td class="common-td">
${x.getDepartureTime()}
</td>
<td class="common-td">
${x.getArrivalTime()}
</td>
<td class="common-td">
${x.getTrainName()}
</td>
<td class="common-td">
${x.getFromName()}
</td>
<td class="common-td">
${x.getToName()}
</td>
<td class="common-td">
<input align="center" type="checkbox" name="checkbox" value="${x.getId()}" />
</td>
</tr>
</c:forEach>