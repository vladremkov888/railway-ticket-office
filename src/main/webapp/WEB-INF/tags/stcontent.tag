<%@ tag pageEncoding='UTF-8' isELIgnored="false"%>
<%@ attribute name="stationList" type="java.util.List" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:forEach var="x" items="${stationList}">
<tr>
	<td class="common-td">
	${x.getName()}
	</td>
	<td class="common-td">
	<input align="center" type="checkbox" name="checkbox" value="${x.getId()}" />
	</td>
</tr>
</c:forEach>