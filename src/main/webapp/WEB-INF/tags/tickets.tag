<%@ tag pageEncoding='UTF-8' isELIgnored="false"%>
<%@ attribute name="ticketList" type="java.util.List" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="lang.text" var="lang"/>

<c:forEach var="x" items="${ticketList}">
<div class="ticket-container">
<table class="table">
	<tr>
	<td class="solid-head-td-left" style="width:300px;"><fmt:message key="ticket.firstname" bundle="${lang}"/></td>
	<td class="solid-head-td-center" style="width:300px;"><fmt:message key="ticket.lastname" bundle="${lang}"/></td>
	<td class="solid-head-td-center" style="width:120px;"><fmt:message key="ticket.ticket_type" bundle="${lang}"/></td>
	<td class="solid-head-td-center" style="width:90px;"><fmt:message key="ticket.coach" bundle="${lang}"/></td>
	<td class="solid-head-td-center" style="width:90px;"><fmt:message key="ticket.seat" bundle="${lang}"/></td>
	<td class="solid-head-td-right" style="width:120px;"><fmt:message key="ticket.price" bundle="${lang}"/></td>
	<td class="solid-head-td-right" style="width:100px;"><fmt:message key="ticket.status" bundle="${lang}"/></td>
	<td class="solid-head-td-right" style="width:50px;"></td>
	</tr>
	<tr>
	<td class="solid-common-td-left">${x.getFirstname()}</td>
	<td class="solid-common-td-center">${x.getLastname()}</td>
	<td class="solid-common-td-center">${x.getTicketType()}</td>
	<td class="solid-common-td-center">${x.getCoach()}</td>
	<td class="solid-common-td-center">${x.getSeat()}</td>
	<td class="solid-common-td-center">${x.getPrice()}</td>
	<td class="solid-common-td-center">${x.getStatus()}</td>
	<td class="solid-common-td-right">
	<c:choose>
	<c:when test="{test.getStatus()=='active'}">
		<form action="${pageContext.request.contextPath}/cabinet/cart" method="post">
		<input type="hidden" name="action" value="email" />
		<input type="hidden" name="id" value="${x.getId()}" />
		<input type="submit" value="Send to<br/>email" />
		</form>
	</c:when>
	</c:choose>
	</td>
	</tr>
</table>
</div>
</c:forEach>