<%@ tag pageEncoding='UTF-8' isELIgnored="false"%>
<%@ attribute name="routeList" type="java.util.List" required="true" %>
<%@ attribute name="stationList" type="java.util.List" required="true" %>
<%@ attribute name="trainList" type="java.util.List" required="true" %>
<%@ attribute name="changes" type="java.util.List" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mm" tagdir="/WEB-INF/tags/tagz"%>

<c:forEach var="x" items="${routeList}">
<c:choose>
<c:when test="${changes.contains(x.getId())}">
<tr>
<td class="common-td">
<input type="datetime-local" name="departure" value="${x.getHTMLFormatTime(x.getDeparture())}" />
</td>
<td class="common-td">
<input type="datetime-local" name="arrival" value="${x.getHTMLFormatTime(x.getArrival())}" />
</td>
<td class="common-td">
<mm:tselect trainList="${trainList}" />
</td>
<td class="common-td">
<mm:sselect name="from" stationList="${stationList}" />
</td>
<td class="common-td">
<mm:sselect name="to" stationList="${stationList}" />
</td>
<td class="common-td">
<input align="center" type="checkbox" name="checkbox" value="${x.getId()}" checked="checked" onclick="return false;" />
</td>
</tr>
</c:when>
<c:otherwise>
<tr>
<td class="common-td">
${x.getDepartureTime()}
</td>
<td class="common-td">
${x.getArrivalTime()}
</td>
<td class="common-td">
${x.getTrainName()}
</td>
<td class="common-td">
${x.getFromName()}
</td>
<td class="common-td">
${x.getToName()}
</td>
<td class="common-td">
<input align="center" type="checkbox" name="checkbox" value="${x.getId()}" disabled />
</td>
</tr>
</c:otherwise>
</c:choose>
</c:forEach>