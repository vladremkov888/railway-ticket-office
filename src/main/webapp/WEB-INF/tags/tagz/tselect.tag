<%@ tag pageEncoding='UTF-8' isELIgnored="false"%>
<%@ attribute name="trainList" type="java.util.List" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<select name="train">
<c:forEach var="x" items="${trainList}">
<option value="${x.getId()}">${x.getName()}</option>
</c:forEach>
</select>