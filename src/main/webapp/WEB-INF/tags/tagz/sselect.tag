<%@ tag pageEncoding='UTF-8' isELIgnored="false"%>
<%@ attribute name="stationList" type="java.util.List" required="true" %>
<%@ attribute name="name" type="java.lang.String" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<select name="${name}">
<c:forEach var="x" items="${stationList}">
<option value="${x.getId()}">${x.getName()}</option>
</c:forEach>
</select>