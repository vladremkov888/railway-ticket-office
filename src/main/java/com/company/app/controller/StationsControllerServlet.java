package com.company.app.controller;

import com.company.app.database.DAO.mysql.*;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.Station;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.ErrorMessages;
import com.company.app.service.StationService;
import com.company.app.service.factory.ServiceFactory;
import com.company.app.service.factory.ServiceType;
import com.company.app.service.implementation.StationServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.*;

@WebServlet("/admin_edit/stations")
public class StationsControllerServlet extends HttpServlet {

    private StationService stationService;
    private final static Logger logger = LogManager.getLogger(StationsControllerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get request to stations controller servlet");
        List<Station> stationList = getStationList(req, resp);
        req.setAttribute("stationList", stationList);
        logger.debug("Redirecting to stations view page");
        req.getRequestDispatcher("/view/admin_panel/stations.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Post request to stations controller servlet");
        String action = req.getParameter("action");

        if (action != null) {
            logger.debug("Found command");
            switch (action) {
                case "add" -> executeAdd(req, resp);
                case "edit" -> executeEdit(req, resp);
                case "commit" -> executeCommit(req, resp);
                case "cancel" -> executeCancel(req, resp);
                case "delete" -> executeDelete(req, resp);
                default -> {
                    logger.debug("Unknown command. Redirecting to stations controller");
                    resp.sendRedirect(req.getContextPath() + "/admin_edit/stations");
                }
            }
        }
    }

    private void executeCancel(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.debug("Cancel command handling. Edited stations are not saved");
        resp.sendRedirect(req.getContextPath() + "/admin_edit?action=edit_stations");
    }

    private void executeDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Delete selected stations command handling");
        try {
            stationService.deleteStations(req.getParameterValues("checkbox"));
            logger.debug("Selected stations successfully deleted");
        } catch (ApplicationException e) {
            logger.error("Error during removing stations", e);
            req.setAttribute("message", ErrorMessages.STATIONS_MANAGEMENT_ERROR.get());
            logger.debug("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        resp.sendRedirect(req.getContextPath() + "/admin_edit?action=edit_stations");
    }

    private void executeCommit(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Commit edited changes command handling");
        try {
            stationService.editStations(req.getParameterValues("checkbox"), req.getParameterValues("name"));
        } catch (ApplicationException e) {
            req.setAttribute("message", ErrorMessages.STATIONS_MANAGEMENT_ERROR.get());
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        resp.sendRedirect(req.getContextPath() + "/admin_edit?action=edit_stations");
    }

    private void executeEdit(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Edit command handling");
        List<Station> stationList = getStationList(req, resp);
        req.setAttribute("action", "edit");
        req.setAttribute("stationList", stationList);
        String[] changes = req.getParameterValues("checkbox");
        if(changes != null) {
            List<Integer> changeList = Arrays.stream(changes)
                    .map(Integer::parseInt)
                    .toList();
            req.setAttribute("changeList", changeList);
            logger.debug("Forward to edit stations view page");
            req.getRequestDispatcher("/view/admin_panel/stations.jsp").forward(req, resp);
        } else {
            resp.sendRedirect(req.getContextPath()+"/admin_edit?action=edit_stations");
        }
    }

    private void executeAdd(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Add command handling");
        try {
            if (!stationService.addStation(req.getParameter("name"))) {
                //Fix this!!!!
                logger.debug("Invalid station");
                req.setAttribute("message", ErrorMessages.STATIONS_MANAGEMENT_ERROR.get());
                logger.debug("Forward to exceptions handling page");
                req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
            }
            logger.debug("Added new station successfully");
        } catch (ApplicationException e) {
            logger.error("Error during adding new station", e);
            req.setAttribute("message", ErrorMessages.STATIONS_MANAGEMENT_ERROR.get());
            logger.debug("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        resp.sendRedirect(req.getContextPath() + "/admin_edit?action=edit_stations");
    }

    private List<Station> getStationList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Station> stationList = null;
        try {
            stationList = stationService.getStationList();
            logger.debug("Current list of stations successfully gotten");
        } catch (ApplicationException e) {
            logger.error("Error during getting current list of stations", e);
            req.setAttribute("message", ErrorMessages.STATIONS_MANAGEMENT_ERROR.get());
            logger.error("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        return stationList;
    }

    @Override
    public void destroy() {
        logger.info("Stations controller servlet destroyed");
    }

    @Override
    public void init() throws ServletException {
        stationService = (StationService) ServiceFactory.getService(ServiceType.STATION_SERVICE);
        logger.info("Stations controller servlet initialized");
    }
}
