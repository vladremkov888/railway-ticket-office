package com.company.app.controller;

import com.company.app.database.DAO.factory.DaoFactory;
import com.company.app.database.DAO.mysql.UserDAOImpl;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.User;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.ErrorMessages;
import com.company.app.service.UserService;
import com.company.app.service.factory.ServiceFactory;
import com.company.app.service.factory.ServiceType;
import com.company.app.service.implementation.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/auth")
public class AuthServlet extends HttpServlet {

    private UserService userService;
    private final static Logger logger = LogManager.getLogger(AuthServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get request to authorization servlet");
        req.getRequestDispatcher("/view/authorization.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Post request to authorization servlet");
        User user = null;

        try {
            user = userService.logInUser(req.getParameter("login"), req.getParameter("password"));
        } catch (ApplicationException e) {
            logger.error("Error during user authorization", e);
            req.setAttribute("message", ErrorMessages.AUTHORIZATION_ERROR.get());
            logger.error("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }

        if(user != null) {
            logger.debug("Successfully logged user");
            req.getSession().setAttribute("is_logged", true);
            req.getSession().setAttribute("user", user);
            logger.debug("Redirecting to cabinet");
            resp.sendRedirect(req.getContextPath()+"/cabinet");
        } else {
            logger.debug("User input invalid login or password");
            req.setAttribute("message", "Invalid login or password");
            logger.debug("Forward to authorization view page");
            req.getRequestDispatcher("/view/authorization.jsp").forward(req, resp);
        }
    }

    @Override
    public void destroy() {
        logger.info("Authorization servlet destroyed");
        super.destroy();
    }

    @Override
    public void init() throws ServletException {
        userService = (UserService) ServiceFactory.getService(ServiceType.USER_SERVICE);
        logger.info("Authorization servlet initialized");
    }
}
