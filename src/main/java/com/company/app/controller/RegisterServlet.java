package com.company.app.controller;

import com.company.app.database.DAO.mysql.UserDAOImpl;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.User;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.ErrorMessages;
import com.company.app.service.UserService;
import com.company.app.service.factory.ServiceFactory;
import com.company.app.service.factory.ServiceType;
import com.company.app.service.implementation.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/reg")
public class RegisterServlet extends HttpServlet {

    private UserService userService;
    private final static Logger logger = LogManager.getLogger(RegisterServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get request to registration servlet");
        req.getRequestDispatcher("/view/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Post request to registration servlet");
        User user = null;

        try {
            user = userService.registerUser(getUser(req));
        } catch (ApplicationException e) {
            logger.error("Error during user registration", e);
            req.setAttribute("message", ErrorMessages.REGISTRATION_ERROR.get());
            logger.error("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }

        if(user != null) {
            logger.debug("Successfully registered new user");
            req.getSession().setAttribute("is_logged", true);
            req.getSession().setAttribute("user", user);
            logger.debug("User authorized. Redirecting to cabinet");
            resp.sendRedirect(req.getContextPath()+"/cabinet");
        } else {
            logger.debug("Incorrect fields are introduced. Registration denied");
            logger.debug("Forward to registration view page with error message");
            req.setAttribute("message", "Incorrect fields");
            req.getRequestDispatcher("/view/registration.jsp").forward(req, resp);
        }
    }

    private User getUser(HttpServletRequest req) {
        User user = new User();
        user.setLogin(req.getParameter("login"));
        user.setPassword(req.getParameter("password"));
        user.setFirstName(req.getParameter("firstname"));
        user.setLastName(req.getParameter("lastname"));
        user.setEmail(req.getParameter("email"));
        user.setPhoneNumber(req.getParameter("phone"));
        user.setRole("user");
        return user;
    }

    @Override
    public void destroy() {
        logger.info("Registration servlet destroyed");
    }

    @Override
    public void init() throws ServletException {
        userService = (UserService) ServiceFactory.getService(ServiceType.USER_SERVICE);
        logger.info("Registration servlet initialized");
    }
}
