package com.company.app.controller;

import com.company.app.database.DAO.mysql.BookingManagerImpl;
import com.company.app.database.DAO.mysql.TicketDAOImpl;
import com.company.app.database.DAO.mysql.TrainDAOImpl;
import com.company.app.database.DAO.mysql.TransactionImpl;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.Ticket;
import com.company.app.entity.User;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.ErrorMessages;
import com.company.app.service.CartService;
import com.company.app.service.factory.ServiceFactory;
import com.company.app.service.factory.ServiceType;
import com.company.app.service.implementation.CartServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

@WebServlet("/cabinet/cart")
public class CartServlet extends HttpServlet {

    private final static Logger logger = LogManager.getLogger(CartServlet.class);
    private CartService cartService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get request to cart servlet");
        User user = (User) req.getSession().getAttribute("user");
        if(user==null) {
            resp.sendRedirect(req.getContextPath()+"/auth");
        } else {
            req.getRequestDispatcher("/view/cart.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Post request to cart servlet");
        String action = req.getParameter("action");
        switch (action) {
            case "pay" -> executePay(req, resp);
            case "delete" -> executeDelete(req, resp);
            default -> {
                logger.debug("Unknown command. Redirecting to cart servlet");
                resp.sendRedirect(req.getContextPath() + "/cabinet/cart");
            }
        }
    }

    private void executeDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Ticket> ticketList = (List<Ticket>) req.getSession().getAttribute("tickets");
        String s = req.getParameter("id");
        int id = 0;
        try {
            id = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            logger.error("Error during deleting ticket", e);
            req.setAttribute("message", "Ticket deleting error");
            logger.debug("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        User user = (User) req.getSession().getAttribute("user");
        if(user==null) {
            resp.sendRedirect(req.getContextPath()+"/auth");
        } else {
            int finalId = id;
            List<Ticket> ticketList1 = ticketList.stream().filter((x)->x.getId() == finalId).toList();
            ticketList.remove(ticketList1.get(0));
            resp.sendRedirect(req.getContextPath() + "/cabinet/cart");
        }
    }

    private void executePay(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Ticket> ticketList = (List<Ticket>) req.getSession().getAttribute("tickets");
        try {
            cartService.buyTickets(ticketList);
        } catch (ApplicationException e) {
            logger.error("Unable to buy tickets");
            req.setAttribute("message", ErrorMessages.BUY_TICKET_ERROR);
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        ticketList.clear();
        req.setAttribute("command", "email");
        req.getRequestDispatcher("/do").include(req, resp);
        if(req.getAttribute("success").equals(true)){
            resp.sendRedirect(req.getContextPath()+"/cabinet?action=my_tickets");
        } else {
            req.setAttribute("message", ErrorMessages.SEND_MAIL_ERROR.get());
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
    }

    @Override
    public void destroy() {
        logger.info("Cart servlet destroyed");
    }

    @Override
    public void init() throws ServletException {
        cartService = (CartService) ServiceFactory.getService(ServiceType.CART_SERVICE);
        logger.info("Cart servlet initialized");
    }
}
