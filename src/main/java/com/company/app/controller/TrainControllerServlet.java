package com.company.app.controller;

import com.company.app.database.DAO.mysql.*;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.Train;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.ErrorMessages;
import com.company.app.service.TrainService;
import com.company.app.service.factory.ServiceFactory;
import com.company.app.service.factory.ServiceType;
import com.company.app.service.implementation.TrainServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

@WebServlet("/admin_edit/trains")
public class TrainControllerServlet extends HttpServlet {

    private TrainService trainService;
    private final static Logger logger = LogManager.getLogger(TrainControllerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get request to train controller servlet");
        List<Train> trainList = null;
        try {
            trainList = trainService.getTrainList();
            logger.debug("Current list of trains successfully gotten");
        } catch (ApplicationException e) {
            logger.error("Error during getting current list of trains", e);
            req.setAttribute("message", ErrorMessages.TRAINS_MANAGEMENT_ERROR.get());
            logger.error("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        req.setAttribute("trainList", trainList);
        logger.debug("Redirecting to trains view page");
        req.getRequestDispatcher("/view/admin_panel/trains.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Post request to train controller servlet");
        String action = req.getParameter("action");

        if (action != null) {
            logger.debug("Found command");
            switch (action) {
                case "add" -> executeAdd(req, resp);
                case "delete" -> executeDelete(req, resp);
                default -> {
                    logger.debug("Unknown command. Redirecting to trains controller");
                    resp.sendRedirect(req.getContextPath() + "/admin_edit/trains");
                }
            }
        }
    }

    private void executeDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            trainService.deleteTrain(req.getParameterValues("checkbox"));
        } catch (ApplicationException e) {
            logger.error("Error during removing trains", e);
            req.setAttribute("message", ErrorMessages.TRAINS_MANAGEMENT_ERROR.get());
            logger.error("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        logger.debug("Train deleted successfully");
        resp.sendRedirect(req.getContextPath()+"/admin_edit/trains");
    }

    private void executeAdd(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            trainService.addTrain(req.getParameter("name"), req.getParameterValues("coach"));
        } catch (ApplicationException e) {
            logger.error("Error during adding train", e);
            req.setAttribute("message", ErrorMessages.TRAINS_MANAGEMENT_ERROR.get());
            logger.error("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        logger.debug("Train added successfully");
        resp.sendRedirect(req.getContextPath()+"/admin_edit/trains");
    }

    @Override
    public void destroy() {
        logger.info("Trains controller servlet destroyed");
    }

    @Override
    public void init() throws ServletException {
        trainService = (TrainService) ServiceFactory.getService(ServiceType.TRAIN_SERVICE);
        logger.info("Trains controller servlet initialized");
    }
}
