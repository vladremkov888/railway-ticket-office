package com.company.app.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/admin_edit")
public class AdminPanelServlet extends HttpServlet {
    private final static Logger logger = LogManager.getLogger(AdminPanelServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get request to admin panel servlet");

        String action = req.getParameter("action");
        if (action != null) {
            switch (action) {
                case "edit_stations" -> {
                    logger.debug("Request to edit station in admin panel. Redirecting to /admin_edit/stations");
                    resp.sendRedirect(req.getContextPath() + "/admin_edit/stations");
                }
                case "edit_routes" -> {
                    logger.debug("Request to edit routes in admin panel. Redirecting to /admin_edit/routes");
                    resp.sendRedirect(req.getContextPath() + "/admin_edit/routes");
                }
                case "view_trains" -> {
                    logger.debug("Request to view train list in admin panel. Redirecting to /admin_edit/trains");
                    resp.sendRedirect(req.getContextPath() + "/admin_edit/trains");
                }
                default -> {
                    logger.debug("Unknown action in admin panel redirecting to /admin_edit");
                    resp.sendRedirect(req.getContextPath() + "/admin_edit");
                }
            }
        } else {
            logger.debug("Request to view admin panel");
            req.getRequestDispatcher("/view/admin_panel.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Post request to admin panel servlet. Nothing to do here");
        resp.sendRedirect(req.getContextPath()+"/admin_edit");
    }

    @Override
    public void destroy() {
        logger.info("Admin panel servlet destroyed");
        super.destroy();
    }

    @Override
    public void init() throws ServletException {
        logger.info("Admin panel servlet initialized");
        super.init();
    }
}
