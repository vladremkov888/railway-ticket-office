package com.company.app.controller;

import com.company.app.entity.User;
import com.company.app.exception.ErrorMessages;
import com.company.app.service.MailSender;
import com.company.app.service.implementation.MailSenderImpl;
import jakarta.mail.MessagingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

@WebServlet("/do")
public class Utils extends HttpServlet {

    private final static Logger logger = LogManager.getLogger(Utils.class);
    private MailSender mailSender;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get request to utils servlet");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Post request to utils servlet");
        String action = req.getParameter("command");
        if (action == null) {
            return;
        }
        switch (action) {
            case "email" -> sendEmail(req, resp);
            case "lang" -> changeLanguage(req, resp);
        }
    }

    private void sendEmail(HttpServletRequest req, HttpServletResponse resp) {
        User user = (User) req.getSession().getAttribute("user");
        ResourceBundle bundle = ResourceBundle.getBundle("lang.text",
                (Locale) req.getSession().getAttribute("locale"));
        try {
            mailSender.sendEmail(user.getEmail(),
                    bundle.getString("mail.title"),
                    bundle.getString("mail.text.greetings1")+
                            user.getFirstName()+" " +
                            user.getLastName()
                            + bundle.getString("mail.text.greetings2"));
            req.setAttribute("success", true);
        } catch (MessagingException e) {
            logger.error("Unable to send email");
            req.setAttribute("success", false);
        }
    }

    private void changeLanguage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.getSession().setAttribute("locale", new Locale(req.getParameter("lang")));
        } catch (Exception e) {
            logger.debug("Unable to change language", e);
            req.setAttribute("message", ErrorMessages.CHANGE_LANG_ERROR.get());
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        resp.sendRedirect(req.getContextPath());
    }

    @Override
    public void destroy() {
        logger.info("Utils servlet destroyed");
    }

    @Override
    public void init() throws ServletException {
        mailSender = new MailSenderImpl();
        logger.info("Utils servlet initialised");
    }
}