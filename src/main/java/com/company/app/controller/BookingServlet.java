package com.company.app.controller;

import com.company.app.database.DAO.mysql.BookingManagerImpl;
import com.company.app.database.DAO.mysql.RouteDAOImpl;
import com.company.app.database.DAO.mysql.StationDAOImpl;
import com.company.app.database.DAO.mysql.TrainDAOImpl;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.*;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.ErrorMessages;
import com.company.app.service.BookingService;
import com.company.app.service.factory.ServiceFactory;
import com.company.app.service.factory.ServiceType;
import com.company.app.service.implementation.BookingServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/book")
public class BookingServlet extends HttpServlet {

    private BookingService bookingService;
    private final static Logger logger = LogManager.getLogger(BookingServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get request to booking servlet");
        String action = req.getParameter("action");
        if (action == null) {
            req.getRequestDispatcher("/view/booking.jsp").forward(req, resp);
            return;
        }

        if (action.equals("viewAll")) {
            List<Route> routeList;
            try {
                routeList = bookingService.getRouteListToDisplay(
                        req.getParameter("start"),
                        req.getParameter("finish"),
                        req.getParameter("date"));
                List<Route> routeList2 = new ArrayList<>();
                bookingService.setRouteStationsNames(routeList);
                bookingService.setRouteTrainName(routeList);
                for (Route el : routeList) {
                    logger.debug("Found routes to display");
                    if (bookingService.isFreeSeatsAvailable(el.getId())) {
                        logger.debug("Free seats available");
                        el.setCoachesInfo(bookingService.getCoachesInfo(el.getId()));
                        el.setPriceList(bookingService.getPriceList(el.getCoachesInfo().keySet().stream().toList()));
                        routeList2.add(el);
                    }
                }
                req.setAttribute("routeList", routeList2);
                req.setAttribute("action", "viewAll");
                req.getRequestDispatcher("/view/booking.jsp").forward(req, resp);
            } catch (ApplicationException e) {
                logger.error("Error during displaying routes", e);
                req.setAttribute("message", ErrorMessages.ROUTE_DISPLAY_ERROR.get());
                logger.debug("Forward to exceptions handling page");
                req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
            }
            return;
        }

        Route r = null;
        try {
            r = bookingService.getRouteToDisplay(req.getParameter("id"));
        } catch (ApplicationException e) {
            logger.error("Error during displaying route", e);
            req.setAttribute("message", ErrorMessages.ROUTE_DISPLAY_ERROR.get());
            logger.debug("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        if (r == null) {
            logger.debug("Route not found");
            resp.sendRedirect(req.getContextPath() + "/book");
        } else {
            try {
                r.setCoaches(bookingService.getWagons(r.getId()));
                r.setCoach(bookingService.getCoachToDisplay(r.getId(), req.getParameter("coach")));
            } catch (ApplicationException e) {
                logger.error("Error during displaying route", e);
                req.setAttribute("message", ErrorMessages.ROUTE_DISPLAY_ERROR.get());
                logger.debug("Forward to exceptions handling page");
                req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
            }
            logger.debug("Found route");
            req.setAttribute("route", r);
            req.setAttribute("action", "view");
            req.getRequestDispatcher("/view/booking.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Post request to booking servlet");
        User user = (User) req.getSession().getAttribute("user");
        if (user == null) {
            logger.debug("Unable to book tickets. User not authorized");
            //resp.sendRedirect(req.getContextPath() + "/auth");
        } else {
            String role = user.getRole();
            if (!role.equals("user")) {
                logger.debug("Unable to book tickets. User's role is not allowed");
                resp.sendRedirect(req.getContextPath() + "/auth");
            }
            String[] seats = req.getParameterValues("seat");
            if (seats.length == 0) {
                logger.debug("No tickets selected");
                resp.sendRedirect(req.getContextPath() + "/cabinet/cart");
            }
            if (seats.length > 6) {
                logger.debug("Too many tickets selected");
                resp.sendRedirect(req.getContextPath() + "/cabinet/cart");
            }
            List<Ticket> ticketList;
            try {
                ticketList = bookingService.bookTickets(seats, user, req.getParameter("route_id"));
            } catch (ApplicationException e) {
                logger.error("Error during booking seats", e);
                req.setAttribute("message", ErrorMessages.ROUTE_BOOKING_ERROR.get());
                logger.debug("Forward to exceptions handling page");
                req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
                return;
            }
            req.getSession().setAttribute("tickets", ticketList);
            logger.debug("Selected tickets booked successfully");
            resp.sendRedirect(req.getContextPath() + "/cabinet/cart");
        }
    }

    @Override
    public void destroy() {
        logger.info("Booking servlet destroyed");
    }

    @Override
    public void init() throws ServletException {
        bookingService = (BookingService) ServiceFactory.getService(ServiceType.BOOKING_SERVICE);
        logger.info("Booking servlet initialized");
    }
}
