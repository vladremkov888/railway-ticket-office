package com.company.app.controller;

import com.company.app.database.DAO.mysql.*;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.Route;
import com.company.app.entity.Station;
import com.company.app.entity.Train;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.ErrorMessages;
import com.company.app.service.*;
import com.company.app.service.factory.ServiceFactory;
import com.company.app.service.factory.ServiceType;
import com.company.app.service.implementation.BookingServiceImpl;
import com.company.app.service.implementation.RouteServiceImpl;
import com.company.app.service.implementation.StationServiceImpl;
import com.company.app.service.implementation.TrainServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.*;

@WebServlet("/admin_edit/routes")
public class RouteControllerServlet extends HttpServlet {

    private RouteService routeService;
    private TrainService trainService;
    private StationService stationService;
    private BookingService bookingService;
    private final static Logger logger = LogManager.getLogger(RouteControllerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get request to route controller servlet");
        List<Route> routeList = getRouteList(req, resp);
        try {
            bookingService.setRouteStationsNames(routeList);
            bookingService.setRouteTrainName(routeList);
        } catch (ApplicationException e) {
            logger.debug("Display routes error");
            req.setAttribute("message", ErrorMessages.ROUTES_MANAGEMENT_ERROR.get());
            logger.debug("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        req.setAttribute("routeList", routeList);
        req.setAttribute("trainList", getTrainList(req, resp));
        req.setAttribute("stationList", getStationList(req, resp));
        logger.debug("Redirecting to routes view page");
        req.getRequestDispatcher("/view/admin_panel/routes.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Post request to route controller servlet");
        String action = req.getParameter("action");

        if (action != null) {
            logger.debug("Found command");
            switch (action) {
                case "add" -> executeAdd(req, resp);
                case "edit" -> executeEdit(req, resp);
                case "commit" -> executeCommit(req, resp);
                case "cancel" -> executeCancel(req, resp);
                case "delete" -> executeDelete(req, resp);
                default -> {
                    logger.debug("Unknown command. Redirecting to routes controller");
                    resp.sendRedirect(req.getContextPath() + "/admin_edit?action=edit_routes");
                }
            }
        }
    }

    private void executeCancel(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.debug("Cancel command handling. Edited routes are not saved");
        resp.sendRedirect(req.getContextPath() + "/admin_edit?action=edit_routes");
    }

    private void executeDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Delete selected stations command handling");
        try {
            routeService.deleteRoutes(req.getParameterValues("checkbox"));
            logger.debug("Selected routes successfully deleted");
        } catch (ApplicationException e) {
            logger.error("Error during removing routes", e);
            req.setAttribute("message", ErrorMessages.ROUTES_MANAGEMENT_ERROR.get());
            logger.debug("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        resp.sendRedirect(req.getContextPath() + "/admin_edit?action=edit_routes");
    }

    private void executeCommit(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Commit edited changes command handling");
        try {
            if(routeService.editRoutes(
                    req.getParameterValues("arrival"),
                    req.getParameterValues("departure"),
                    req.getParameterValues("train"),
                    req.getParameterValues("from"),
                    req.getParameterValues("to"),
                    req.getParameterValues("checkbox"))) {
                logger.debug("Redirecting to routes controller");
                resp.sendRedirect(req.getContextPath()+"/admin_edit/routes");
            } else {
                logger.error("Routes management error. Redirecting to message page");
                req.setAttribute("message", ErrorMessages.ROUTES_MANAGEMENT_ERROR.get());
                req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
            }
        } catch (ApplicationException e) {
            logger.error("Routes management error. Redirecting to message page", e);
            req.setAttribute("message", ErrorMessages.ROUTES_MANAGEMENT_ERROR.get());
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        resp.sendRedirect(req.getContextPath() + "/admin_edit?action=edit_routes");
    }

    private void executeEdit(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Edit command handling");
        List<Route> routeList = getRouteList(req, resp);
        req.setAttribute("action", "edit");
        req.setAttribute("routeList", routeList);
        req.setAttribute("trainList", getTrainList(req, resp));
        req.setAttribute("stationList", getStationList(req, resp));
        try {
            bookingService.setRouteStationsNames(routeList);
            bookingService.setRouteTrainName(routeList);
        } catch (ApplicationException e) {
            logger.debug("Edit routes error");
            req.setAttribute("message", ErrorMessages.ROUTES_MANAGEMENT_ERROR.get());
            logger.debug("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        String[] changeList = req.getParameterValues("checkbox");
        if(changeList != null) {
            List<Integer> changeList1 = Arrays.stream(changeList)
                    .map(Integer::parseInt)
                    .toList();
            req.setAttribute("changeList", changeList1);
            logger.debug("Forward to edit routes view page");
            req.getRequestDispatcher("/view/admin_panel/routes.jsp").forward(req, resp);
        } else {
            resp.sendRedirect(req.getContextPath()+"/admin_edit?action=edit_routes");
        }
    }

    private void executeAdd(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Add command handling");
        try {
            if (!routeService.addRoute(
                    req.getParameter("arrival"),
                    req.getParameter("departure"),
                    req.getParameter("train"),
                    req.getParameter("from"),
                    req.getParameter("to"))) {
                //Fix this!!!!
                logger.debug("Invalid route");
                req.setAttribute("message", ErrorMessages.ROUTES_MANAGEMENT_ERROR.get());
                logger.debug("Forward to exceptions handling page");
                req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
            } else {
                logger.debug("Added new route successfully");
                resp.sendRedirect(req.getContextPath() + "/admin_edit?action=edit_routes");
            }
        } catch (ApplicationException e) {
            logger.error("Error during adding new route", e);
            req.setAttribute("message", ErrorMessages.ROUTES_MANAGEMENT_ERROR.get());
            logger.debug("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
    }

    private List<Route> getRouteList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Route> routeList = null;
        try {
            routeList = routeService.getRouteList();
            logger.debug("Current list of routes successfully gotten");
        } catch (ApplicationException e) {
            logger.error("Error during getting current list of routes", e);
            req.setAttribute("message", ErrorMessages.ROUTES_MANAGEMENT_ERROR.get());
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }

        return routeList;
    }

    private List<Train> getTrainList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Train> trainList = null;
        try {
            trainList = trainService.getTrainListWithoutCoaches();
            logger.debug("Current list of trains successfully gotten");
        } catch (ApplicationException e) {
            logger.error("Error during getting current list of trains", e);
            req.setAttribute("message", ErrorMessages.ROUTES_MANAGEMENT_ERROR.get());
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        return trainList;
    }

    private List<Station> getStationList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Station> stationList = null;
        try {
            stationList = stationService.getStationList();
            logger.debug("Current list of stations successfully gotten");
        } catch (ApplicationException e) {
            logger.error("Error during getting current list of stations", e);
            req.setAttribute("message", ErrorMessages.ROUTES_MANAGEMENT_ERROR.get());
            logger.error("Forward to exceptions handling page");
            req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
        }
        return stationList;
    }

    @Override
    public void destroy() {
        logger.info("Route controller servlet destroyed");
    }

    @Override
    public void init() throws ServletException {
        routeService = (RouteService) ServiceFactory.getService(ServiceType.ROUTE_SERVICE);
        trainService = (TrainService) ServiceFactory.getService(ServiceType.TRAIN_SERVICE);
        stationService = (StationService) ServiceFactory.getService(ServiceType.STATION_SERVICE);
        bookingService = (BookingService) ServiceFactory.getService(ServiceType.BOOKING_SERVICE);
        logger.info("Route controller servlet initialized");
    }
}
