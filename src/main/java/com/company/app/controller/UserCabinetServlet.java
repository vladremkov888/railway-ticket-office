package com.company.app.controller;

import com.company.app.database.DAO.mysql.TicketDAOImpl;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.User;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.ErrorMessages;
import com.company.app.service.CartService;
import com.company.app.service.factory.ServiceFactory;
import com.company.app.service.factory.ServiceType;
import com.company.app.service.implementation.CartServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet("/cabinet")
public class UserCabinetServlet extends HttpServlet {

    private final static Logger logger = LogManager.getLogger(UserCabinetServlet.class);
    private CartService cartService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Get request to user cabinet controller servlet");
        User user = (User) req.getSession().getAttribute("user");
        if (user == null) {
            logger.debug("User not authorized. Redirecting to authorization servlet");
            resp.sendRedirect(req.getContextPath() + "/auth");
        } else {
            logger.debug("User authorized");
            String action = req.getParameter("action");
            if (action != null && action.equals("my_tickets")) {
                try {
                    req.setAttribute("ticketList", cartService.getTicketList(user.getId()));
                } catch (ApplicationException e) {
                    logger.error("Error during getting current list of trains", e);
                    req.setAttribute("message", ErrorMessages.MY_TICKETS_DISPLAY_ERROR.get());
                    logger.error("Forward to exceptions handling page");
                    req.getRequestDispatcher("/view/message.jsp").forward(req, resp);
                }
                req.getRequestDispatcher("/view/tickets.jsp").forward(req, resp);
            } else {
                req.getRequestDispatcher("/view/cabinet.jsp").forward(req, resp);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Post request to user cabinet servlet");
        String action = req.getParameter("out");
        if (action.equals("out")) {
            User user = (User) req.getSession().getAttribute("user");
            if (user == null) {
                resp.sendRedirect(req.getContextPath() + "/auth");
            } else {
                req.getSession().removeAttribute("user");
                resp.sendRedirect(req.getContextPath() + "/auth");
            }
        } else {
            resp.sendRedirect(req.getContextPath() + "/cabinet");
        }
    }

    @Override
    public void destroy() {
        logger.info("User cabinet servlet destroyed");
    }

    @Override
    public void init() throws ServletException {
        cartService = (CartService) ServiceFactory.getService(ServiceType.CART_SERVICE);
        logger.info("User cabinet servlet initialized");
    }
}
