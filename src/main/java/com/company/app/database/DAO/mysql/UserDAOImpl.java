package com.company.app.database.DAO.mysql;

import com.company.app.database.DAO.AbstractDAO;
import com.company.app.database.DAO.UserDAO;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.User;
import com.company.app.database.utils.DbUtils;
import com.company.app.exception.DbInteractionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.company.app.database.utils.DbConstants.*;

public class UserDAOImpl extends AbstractDAO implements UserDAO {

    private final static Logger logger = LogManager.getLogger(UserDAOImpl.class);

    public UserDAOImpl() {}

    public UserDAOImpl(DataSource ds) {
        this.dataSource = ds;
    }

    Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public boolean insertUser(User user) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return insertUser(user, con);
        } catch (SQLException e) {
            logger.debug("Unable to insert user");
            throw new DbInteractionException("Insert user exception", e);
        }
    }

    public User getUser(String login) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getUser(login, con);
        } catch (SQLException e) {
            logger.debug("Unable to get user");
            throw new DbInteractionException("Get user exception", e);
        }
    }

    boolean insertUser(User user, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(INSERT_USER);

        stmt.setString(1, user.getLogin());
        stmt.setString(2, user.getPassword());
        stmt.setString(3, user.getEmail());
        stmt.setString(4, user.getFirstName());
        stmt.setString(5, user.getLastName());
        stmt.setString(6, user.getPhoneNumber());
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    User getUser(String login, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_USER_BY_LOGIN);

        stmt.setString(1, login);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            User user = new User();
            user.setId(rs.getInt("id"));
            user.setLogin(login);
            user.setPassword(rs.getString("password"));
            user.setFirstName(rs.getString("firstname"));
            user.setLastName(rs.getString("lastname"));
            user.setEmail(rs.getString("email"));
            user.setPhoneNumber(rs.getString("phone"));
            user.setRole(rs.getString("role"));
            return user;
        }

        DbUtils.close(stmt);
        return null;
    }
}
