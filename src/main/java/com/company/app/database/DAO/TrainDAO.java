package com.company.app.database.DAO;

import com.company.app.entity.Coach;
import com.company.app.entity.Seat;
import com.company.app.entity.Train;
import com.company.app.exception.DbInteractionException;

import java.util.List;

public interface TrainDAO {
    boolean insertTrain(Train train) throws DbInteractionException;

    boolean insertCoach(Coach coach, int trainId) throws DbInteractionException;

    boolean insertSeat(Seat seat, int coachId) throws DbInteractionException;

    Seat getSeat(int id) throws DbInteractionException;

    Train getTrain(int id) throws DbInteractionException;

    Train getTrainWithoutCoaches(int id) throws DbInteractionException;

    List<Coach> getCoaches(int TrainId) throws DbInteractionException;

    Coach getCoach(int coachId) throws DbInteractionException;

    List<Seat> getSeatList(int coachId) throws DbInteractionException;

    List<Train> getTrains() throws DbInteractionException;

    List<Train> getTrainsWithoutCoaches() throws DbInteractionException;

    boolean removeTrain(int id) throws DbInteractionException;
}
