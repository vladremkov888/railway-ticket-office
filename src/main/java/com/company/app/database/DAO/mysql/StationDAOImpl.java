package com.company.app.database.DAO.mysql;

import com.company.app.database.DAO.AbstractDAO;
import com.company.app.database.DAO.StationDAO;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.Station;
import com.company.app.database.utils.DbUtils;
import com.company.app.exception.DbInteractionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.company.app.database.utils.DbConstants.*;


public class StationDAOImpl extends AbstractDAO implements StationDAO {

    private final static Logger logger = LogManager.getLogger(StationDAOImpl.class);

    public StationDAOImpl() {}

    public StationDAOImpl(DataSource ds) {
        this.dataSource = ds;
    }

    Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public boolean insertStation(Station station) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return insertStation(station, con);
        } catch (SQLException e) {
            logger.debug("Unable to insert station");
            throw new DbInteractionException("Insert station exception", e);
        }
    }

    public Station getStation(String name) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getStation(name, con);
        } catch (SQLException e) {
            logger.debug("Unable to get station by name");
            throw new DbInteractionException("Get station by name exception", e);
        }
    }

    public Station getStation(int id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getStation(id, con);
        } catch (SQLException e) {
            logger.debug("Unable to get station by id");
            throw new DbInteractionException("Get station by id exception", e);
        }
    }

    public boolean updateStation(Station updatedStation) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return updateStation(updatedStation, con);
        } catch (SQLException e) {
            logger.debug("Unable to update station");
            throw new DbInteractionException("Update station exception", e);
        }
    }

    public boolean removeStation(int id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return removeStation(id, con);
        } catch (SQLException e) {
            logger.debug("Unable to remove station");
            throw new DbInteractionException("Remove station exception", e);
        }
    }

    public List<Station> getStations() throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getStations(con);
        } catch (SQLException e) {
            logger.debug("Unable to get station list");
            throw new DbInteractionException("Get station list exception", e);
        }
    }

    boolean insertStation(Station station, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(INSERT_STATION);

        stmt.setString(1, station.getName());
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    Station getStation(String name, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_STATION_BY_NAME);

        stmt.setString(1, name);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            Station station = new Station(rs.getString("name"));
            station.setId(rs.getInt("id"));
            DbUtils.close(stmt);
            DbUtils.close(con);
            return station;
        }

        DbUtils.close(stmt);
        return null;
    }

    Station getStation(int id, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_STATION_BY_ID);

        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            Station station = new Station(rs.getString("name"));
            station.setId(rs.getInt("id"));
            DbUtils.close(stmt);
            DbUtils.close(con);
            return station;
        }

        DbUtils.close(stmt);
        return null;
    }

    boolean updateStation(Station updatedStation, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(UPDATE_STATION);

        stmt.setString(1, updatedStation.getName());
        stmt.setInt(2, updatedStation.getId());
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    boolean removeStation(int id, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(DELETE_STATION);

        stmt.setInt(1, id);
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    List<Station> getStations(Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_STATIONS);

        ResultSet rs = stmt.executeQuery();
        List<Station> stationList = new ArrayList<>();
        while (rs.next()) {
            Station station = new Station(rs.getString("name"));
            station.setId(rs.getInt("id"));
            stationList.add(station);
        }

        DbUtils.close(stmt);
        return stationList;
    }
}
