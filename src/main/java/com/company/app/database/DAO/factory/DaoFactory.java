package com.company.app.database.DAO.factory;

import com.company.app.database.DAO.AbstractDAO;

public class DaoFactory {
    public static AbstractDAO getDao(DaoType type) {
        return type.getDao();
    }
}
