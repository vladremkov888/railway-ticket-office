package com.company.app.database.DAO.mysql;

import com.company.app.database.DAO.AbstractDAO;
import com.company.app.database.DAO.Transaction;
import com.company.app.database.utils.DbUtils;
import com.company.app.entity.*;
import com.company.app.exception.DbInteractionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TransactionImpl extends AbstractDAO implements Transaction {
    private final TrainDAOImpl trainDAO;
    private final StationDAOImpl stationDAO;
    private final RouteDAOImpl routeDAO;
    private final BookingManagerImpl bookingManager;
    private final TicketDAOImpl ticketDAO;

    private final static Logger logger = LogManager.getLogger(StationDAOImpl.class);

    public TransactionImpl(TrainDAOImpl trainDAO,
                           BookingManagerImpl bookingManager,
                           StationDAOImpl stationDAO,
                           RouteDAOImpl routeDAO,
                           TicketDAOImpl ticketDAO) {
        super();
        this.trainDAO = trainDAO;
        this.stationDAO = stationDAO;
        this.routeDAO = routeDAO;
        this.bookingManager = bookingManager;
        this.ticketDAO = ticketDAO;
    }

    Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public boolean insertTrain(Train train) throws DbInteractionException {
        Connection con = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            trainDAO.insertTrain(train, con);
            for (Coach el : train.getCoachList()) {
                trainDAO.insertCoach(el, train.getId(), con);
                for (Seat el1: el.getSeatList()) {
                    trainDAO.insertSeat(el1, el.getId(), con);
                }
            }
            con.commit();
        } catch (SQLException e) {
            DbUtils.rollback(con);
            logger.debug("Unable to insert train");
            throw new DbInteractionException("Insert train exception", e);
        } finally {
            DbUtils.close(con);
        }
        return true;
    }

    public boolean addTrainToRoute(int trainId, int routeId) throws DbInteractionException {
        Connection con = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            List<Coach> coachList = trainDAO.getCoaches(trainId);
            for (Coach coach : coachList) {
                for (Seat seat : coach.getSeatList()) {
                    bookingManager.bookSeat(routeId, coach.getType(), seat, con);
                }
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            DbUtils.rollback(con);
            logger.debug("Unable to set train to route");
            throw new DbInteractionException("Set train to route exception", e);
        } finally {
            DbUtils.close(con);
        }
    }

    public boolean updateStations(List<Station> stationList) throws DbInteractionException {
        Connection con = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            for (Station el: stationList) {
                stationDAO.updateStation(el, con);
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            DbUtils.rollback(con);
            logger.debug("Unable to update stations");
            throw new DbInteractionException("Update stations exception", e);
        } finally {
            DbUtils.close(con);
        }
    }

    public boolean deleteStations(List<Integer> ids) throws DbInteractionException {
        Connection con = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            for (Integer el: ids) {
                stationDAO.removeStation(el, con);
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            DbUtils.rollback(con);
            logger.debug("Unable to delete stations");
            throw new DbInteractionException("Delete stations exception", e);
        } finally {
            DbUtils.close(con);
        }
    }

    public boolean updateRoutes(List<Route> routeList) throws DbInteractionException {
        Connection con = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            for (Route el: routeList) {
                bookingManager.removeTrainFromRoute(el.getId());
                routeDAO.updateRoute(el, con);
                addTrainToRoute(el.getTrainId(), el.getId());
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            DbUtils.rollback(con);
            logger.debug("Unable to update routes");
            throw new DbInteractionException("Update routes exception", e);
        } finally {
            DbUtils.close(con);
        }
    }

    public boolean deleteRoutes(List<Integer> ids) throws DbInteractionException {
        Connection con = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            for (Integer el: ids) {
                routeDAO.deleteRoute(el, con);
                bookingManager.removeTrainFromRoute(el, con);
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            DbUtils.rollback(con);
            logger.debug("Unable to delete routes");
            throw new DbInteractionException("Delete routes exception", e);
        } finally {
            DbUtils.close(con);
        }
    }

    public boolean buyTickets(List<Ticket> tickets) throws DbInteractionException {
        Connection con = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            for (Ticket ticket : tickets) {
                bookingManager.setSeatTypeToBusy(ticket.getRoute_id(), ticket.getSeat_id());
                ticketDAO.insertTicket(ticket);
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            DbUtils.rollback(con);
            logger.debug("Unable to buy tickets");
            throw new DbInteractionException("Buy tickets exception", e);
        } finally {
            DbUtils.close(con);
        }
    }

    public boolean deleteTrains(List<Integer> ids) throws DbInteractionException {
        Connection con = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            for (Integer el: ids) {
                trainDAO.removeTrain(el, con);
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            DbUtils.rollback(con);
            logger.debug("Unable to delete trains");
            throw new DbInteractionException("Delete trains exception", e);
        } finally {
            DbUtils.close(con);
        }
    }
}
