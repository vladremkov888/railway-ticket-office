package com.company.app.database.DAO.mysql;

import com.company.app.database.DAO.AbstractDAO;
import com.company.app.database.DAO.TrainDAO;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.Coach;
import com.company.app.entity.Seat;
import com.company.app.entity.Train;
import com.company.app.database.utils.DbUtils;
import com.company.app.exception.DbInteractionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.company.app.database.utils.DbConstants.*;

public class TrainDAOImpl extends AbstractDAO implements TrainDAO  {

    private final static Logger logger = LogManager.getLogger(TrainDAOImpl.class);

    public TrainDAOImpl() {}

    public TrainDAOImpl(DataSource ds) {
        this.dataSource = ds;
    }

    Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public boolean insertTrain(Train train) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return insertTrain(train, con);
        } catch (SQLException e) {
            logger.debug("Unable to insert train");
            throw new DbInteractionException("Insert train exception", e);
        }
    }

    public boolean insertCoach(Coach coach, int trainId) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return insertCoach(coach, trainId, con);
        } catch (SQLException e) {
            logger.debug("Unable to insert coach");
            throw new DbInteractionException("Insert coach exception", e);
        }
    }

    public boolean insertSeat(Seat seat, int coachId) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return insertSeat(seat, coachId, con);
        } catch (SQLException e) {
            logger.debug("Unable to insert seat");
            throw new DbInteractionException("Insert seat exception", e);
        }
    }

    public Seat getSeat(int id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getSeat(id, con);
        } catch (SQLException e) {
            logger.debug("Unable to get seat");
            throw new DbInteractionException("Get seat exception", e);
        }
    }

    public Train getTrain(int id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            Train train = getTrain(id, con);
            train.setCoachList(getCoaches(train.getId(), con));
            for (Coach el: train.getCoachList()) {
                el.setSeatList(getSeatList(el.getId(), con));
            }
            return train;
        } catch (SQLException e) {
            logger.debug("Unable to get train");
            throw new DbInteractionException("Get train exception", e);
        }
    }

    public Train getTrainWithoutCoaches(int id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getTrain(id, con);
        } catch (SQLException e) {
            logger.debug("Unable to get train without coaches");
            throw new DbInteractionException("Get train without coaches exception", e);
        }
    }

    public List<Coach> getCoaches(int trainId) throws DbInteractionException {
        try (Connection con = getConnection()) {
            List<Coach> coachList = getCoaches(trainId, con);
            for (Coach el: coachList) {
                el.setSeatList(getSeatList(el.getId(), con));
            }
            return coachList;
        } catch (SQLException e) {
            logger.debug("Unable to get coach list");
            throw new DbInteractionException("Get coach list exception", e);
        }
    }

    public Coach getCoach(int coachId) throws DbInteractionException {
        try (Connection con = getConnection()) {
            Coach coach = getCoach(coachId, con);
            coach.setSeatList(getSeatList(coachId, con));
            return coach;
        } catch (SQLException e) {
            logger.debug("Unable to get coach");
            throw new DbInteractionException("Get coach exception", e);
        }
    }

    public List<Seat> getSeatList(int coachId) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getSeatList(coachId, con);
        } catch (SQLException e) {
            logger.debug("Unable to get seat list");
            throw new DbInteractionException("Get seat list exception", e);
        }
    }

    public List<Train> getTrains() throws DbInteractionException {
        try (Connection con = getConnection()) {
            List<Train> trainList = getTrains(con);
            for (Train el: trainList) {
                el.setCoachList(getCoaches(el.getId(), con));
                for (Coach el1: el.getCoachList()) {
                    el1.setSeatList(getSeatList(el1.getId(), con));
                }
            }
            return trainList;
        } catch (SQLException e) {
            logger.debug("Unable to get train list");
            throw new DbInteractionException("Get train list exception", e);
        }
    }

    public List<Train> getTrainsWithoutCoaches() throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getTrains(con);
        } catch (SQLException e) {
            logger.debug("Unable to get train list without coaches");
            throw new DbInteractionException("Get train list without coaches exception", e);
        }
    }

    @Override
    public boolean removeTrain(int id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return removeTrain(id, con);
        } catch (SQLException e) {
            logger.debug("Unable to remove train");
            throw new DbInteractionException("Remove train exception", e);
        }
    }

    boolean removeTrain(int id, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(DELETE_TRAIN);

        stmt.setInt(1, id);
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    boolean insertTrain(Train train, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(INSERT_TRAIN, Statement.RETURN_GENERATED_KEYS);

        stmt.setString(1, train.getName());
        stmt.execute();
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            train.setId((int) rs.getLong(1));
        }

        DbUtils.close(stmt);
        return true;
    }

    boolean insertCoach(Coach coach, int trainId, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(INSERT_COACH, Statement.RETURN_GENERATED_KEYS);

        stmt.setString(1, coach.getType());
        stmt.setInt(2, coach.getNumber());
        stmt.setInt(3, trainId);
        stmt.execute();
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            coach.setId((int) rs.getLong(1));
        }

        DbUtils.close(stmt);
        return true;
    }

    boolean insertSeat(Seat seat, int coachId, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(INSERT_SEAT);

        stmt.setInt(1, seat.getNumber());
        stmt.setInt(2, coachId);
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    Seat getSeat(int id, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_SEAT);

        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            Seat seat = new Seat();
            seat.setId(rs.getInt("id"));
            seat.setNumber(rs.getInt("number"));
            seat.setCoachId(rs.getInt("coach_id"));
            return seat;
        }

        DbUtils.close(stmt);
        return null;
    }

    Train getTrain(int id, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_TRAIN_BY_ID);

        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            Train train = new Train();
            train.setId(rs.getInt("id"));
            train.setName(rs.getString("name"));
            DbUtils.close(stmt);
            return train;
        }
        DbUtils.close(stmt);
        return null;
    }

    List<Coach> getCoaches(int trainId, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_COACHES);

        stmt.setInt(1, trainId);
        ResultSet rs = stmt.executeQuery();
        List<Coach> coachList = new ArrayList<>();
        while (rs.next()) {
            Coach coach = new Coach();
            coach.setId(rs.getInt("id"));
            coach.setNumber(rs.getInt("number"));
            coach.setType(rs.getString("type"));
            coachList.add(coach);
        }

        DbUtils.close(stmt);
        return coachList;
    }

    Coach getCoach(int coachId, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_COACH_BY_ID);

        stmt.setInt(1, coachId);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            Coach coach = new Coach();
            coach.setId(rs.getInt("id"));
            coach.setNumber(rs.getInt("number"));
            coach.setType(rs.getString("type"));
            DbUtils.close(stmt);
            return coach;
        }

        DbUtils.close(stmt);
        return null;
    }

    List<Seat> getSeatList(int coachId, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_SEATS);

        stmt.setInt(1, coachId);
        ResultSet rs = stmt.executeQuery();
        List<Seat> seatList = new ArrayList<>();
        while (rs.next()) {
            Seat seat = new Seat();
            seat.setId(rs.getInt("id"));
            seat.setNumber(rs.getInt("number"));
            seat.setCoachId(coachId);
            seatList.add(seat);
        }

        DbUtils.close(stmt);
        return seatList;
    }

    List<Train> getTrains(Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_TRAINS);

        ResultSet rs = stmt.executeQuery();
        List<Train> trainList = new ArrayList<>();
        while (rs.next()) {
            Train train = new Train();
            train.setId(rs.getInt("id"));
            train.setName(rs.getString("name"));
            trainList.add(train);
        }

        DbUtils.close(stmt);
        return trainList;
    }
}
