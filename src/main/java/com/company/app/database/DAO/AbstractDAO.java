package com.company.app.database.DAO;

import com.company.app.database.utils.DbDataSource;

import javax.sql.DataSource;

public abstract class AbstractDAO {
    protected DataSource dataSource;

    {
        dataSource = DbDataSource.getInstance().getDataSource();
    }

}
