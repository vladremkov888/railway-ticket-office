package com.company.app.database.DAO;

import com.company.app.entity.Ticket;
import com.company.app.exception.DbInteractionException;

import java.util.List;

public interface TicketDAO {
    boolean insertTicket(Ticket ticket) throws DbInteractionException;

    List<Ticket> getTickets(int userId) throws DbInteractionException;
}
