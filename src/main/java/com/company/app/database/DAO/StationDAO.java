package com.company.app.database.DAO;

import com.company.app.entity.Station;
import com.company.app.exception.DbInteractionException;

import java.util.List;

public interface StationDAO {
    boolean insertStation(Station station) throws DbInteractionException;

    Station getStation(String name) throws DbInteractionException;

    Station getStation(int id) throws DbInteractionException;

    boolean updateStation(Station updatedStation) throws DbInteractionException;

    boolean removeStation(int id) throws DbInteractionException;

    List<Station> getStations() throws DbInteractionException;
}
