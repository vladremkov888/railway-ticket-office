package com.company.app.database.DAO.mysql;

import java.sql.*;
import java.util.ArrayList;

import com.company.app.database.DAO.AbstractDAO;
import com.company.app.database.DAO.RouteDAO;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.Route;
import com.company.app.database.utils.DbUtils;
import com.company.app.exception.DbInteractionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.util.Date;
import java.util.List;

import static com.company.app.database.utils.DbConstants.*;

public class RouteDAOImpl extends AbstractDAO implements RouteDAO {

    private final static Logger logger = LogManager.getLogger(RouteDAOImpl.class);

    public RouteDAOImpl() {}

    public RouteDAOImpl(DataSource ds) {
        this.dataSource = ds;
    }

    Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public boolean insertRoute(Route route) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return insertRoute(route, con);
        } catch (SQLException e) {
            logger.debug("Unable to insert route");
            throw new DbInteractionException("Insert route exception", e);
        }
    }

    public boolean deleteRoute(int id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return deleteRoute(id, con);
        } catch (SQLException e) {
            logger.debug("Unable to delete route");
            throw new DbInteractionException("Delete route exception", e);
        }
    }

    public boolean updateRoute(Route r) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return updateRoute(r, con);
        } catch (SQLException e) {
            logger.debug("Unable to update route");
            throw new DbInteractionException("Update route exception", e);
        }
    }

    public Route getRoute(int id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getRoute(id, con);
        } catch (SQLException e) {
            logger.debug("Unable to get route");
            throw new DbInteractionException("Get route exception", e);
        }
    }

    public List<Route> getRoutes() throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getRoutes(con);
        } catch (SQLException e) {
            logger.debug("Unable to get route list");
            throw new DbInteractionException("Get route list exception", e);
        }
    }

    public List<Route> getRoutes(int sId, int fId, java.util.Date date) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getRoutes(sId, fId, date, con);
        } catch (SQLException e) {
            logger.debug("Unable to get filtered route list");
            throw new DbInteractionException("Get filtered route list exception", e);
        }
    }

    boolean insertRoute(Route route, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(INSERT_ROUTE, Statement.RETURN_GENERATED_KEYS);

        stmt.setLong(1, route.getArrival().getTime());
        stmt.setLong(2, route.getDeparture().getTime());
        stmt.setInt(3, route.getTrainId());
        stmt.setInt(4, route.getFromId());
        stmt.setInt(5, route.getToId());
        stmt.execute();
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            route.setId(rs.getInt(1));
        }

        DbUtils.close(stmt);
        return true;
    }

    boolean deleteRoute(int id, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(DELETE_ROUTE);

        stmt.setInt(1, id);
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    boolean updateRoute(Route r, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(UPDATE_ROUTE);

        stmt.setLong(1, r.getArrival().getTime());
        stmt.setLong(2, r.getDeparture().getTime());
        stmt.setInt(3, r.getTrainId());
        stmt.setInt(4, r.getFromId());
        stmt.setInt(5, r.getToId());
        stmt.setInt(6, r.getId());
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    Route getRoute(int id, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_ROUTE_BY_ID);

        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            Route route = new Route();
            route.setId(rs.getInt("id"));
            route.setArrival(new Date(rs.getLong("arrival")));
            route.setDeparture(new Date(rs.getLong("departure")));
            route.setFromId(rs.getInt("from_id"));
            route.setToId(rs.getInt("to_id"));
            route.setTrainId(rs.getInt("train_id"));
            DbUtils.close(stmt);
            DbUtils.close(con);
            return route;
        }

        DbUtils.close(stmt);
        return null;
    }

    List<Route> getRoutes(Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_ROUTES);

        ResultSet rs = stmt.executeQuery();
        List<Route> routeList = new ArrayList<>();
        while (rs.next()) {
            Route route = new Route();
            route.setId(rs.getInt("id"));
            route.setArrival(new java.util.Date(rs.getLong("arrival")));
            route.setDeparture(new java.util.Date(rs.getLong("departure")));
            route.setFromId(rs.getInt("from_id"));
            route.setToId(rs.getInt("to_id"));
            route.setTrainId(rs.getInt("train_id"));
            routeList.add(route);
        }

        DbUtils.close(stmt);
        return routeList;
    }

    List<Route> getRoutes(int sId, int fId, java.util.Date date, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_ROUTES_BY_STATIONS_AND_DATE);

            stmt.setInt(1, sId);
            stmt.setInt(2, fId);
            //stmt.setLong(3, date.getTime());

            ResultSet rs = stmt.executeQuery();
            List<Route> routeList = new ArrayList<>();
            while (rs.next()) {
                Route route = new Route();
                route.setId(rs.getInt("id"));
                route.setArrival(new java.util.Date(rs.getLong("arrival")));
                route.setDeparture(new java.util.Date(rs.getLong("departure")));
                route.setFromId(rs.getInt("from_id"));
                route.setToId(rs.getInt("to_id"));
                route.setTrainId(rs.getInt("train_id"));
                routeList.add(route);
            }

            DbUtils.close(stmt);
            return routeList;
    }
}
