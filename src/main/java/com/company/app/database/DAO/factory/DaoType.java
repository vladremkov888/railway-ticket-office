package com.company.app.database.DAO.factory;

import com.company.app.database.DAO.AbstractDAO;
import com.company.app.database.DAO.mysql.*;

public enum DaoType {
    BOOKING_MANAGER(new BookingManagerImpl()),
    ROUTE_DAO(new RouteDAOImpl()),
    STATION_DAO(new StationDAOImpl()),
    TICKET_DAO(new TicketDAOImpl()),
    TRAIN_DAO(new TrainDAOImpl()),
    TRANSACTION(new TransactionImpl(
            new TrainDAOImpl(),
            new BookingManagerImpl(),
            new StationDAOImpl(),
            new RouteDAOImpl(),
            new TicketDAOImpl())),
    USER_DAO(new UserDAOImpl());

    private final AbstractDAO dao;

    DaoType(AbstractDAO dao) {
        this.dao = dao;
    }

    public AbstractDAO getDao() {
        return dao;
    }
}
