package com.company.app.database.DAO.mysql;

import com.company.app.database.DAO.AbstractDAO;
import com.company.app.database.DAO.BookingManager;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.Seat;
import com.company.app.database.utils.DbUtils;
import com.company.app.exception.DbInteractionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.company.app.database.utils.DbConstants.*;

public class BookingManagerImpl extends AbstractDAO implements BookingManager {
    private final static Logger logger = LogManager.getLogger(BookingManagerImpl.class);

    public BookingManagerImpl() {}

    public BookingManagerImpl(DataSource ds) {
        this.dataSource = ds;
    }

    Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public boolean bookSeat(int routeId, String coachType, Seat seat) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return bookSeat(routeId, coachType, seat, con);
        } catch (SQLException e) {
            logger.debug("Unable to set train to route");
            throw new DbInteractionException("Set train to route exception", e);
        } catch (IllegalArgumentException e) {
            logger.debug("Unable to set train to route");
            throw new DbInteractionException("Set seat price exception", e);
        }
    }

    public boolean removeTrainFromRoute(int routeId) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return removeTrainFromRoute(routeId, con);
        } catch (SQLException e) {
            logger.debug("Unable to remove train from route");
            throw new DbInteractionException("Remove train from route exception", e);
        }
    }

    public boolean setSeatTypeToFree(int route_id, int seat_id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return setSeatTypeToFree(route_id, seat_id, con);
        } catch (SQLException e) {
            logger.debug("Unable to change seat status to free");
            throw new DbInteractionException("Change seat status to free exception", e);
        }
    }

    public boolean setSeatTypeToBusy(int route_id, int seat_id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return setSeatTypeToBusy(route_id, seat_id, con);
        } catch (SQLException e) {
            logger.debug("Unable to change seat status to busy");
            throw new DbInteractionException("Change seat status to busy exception", e);
        }
    }

    public List<Seat> getRouteSeats(int routeId) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getRouteSeats(routeId, con);
        } catch (SQLException e) {
            logger.debug("Unable to get route's booked seats");
            throw new DbInteractionException("Get route's booked seats exception", e);
        }
    }

    public int getSeatPrice(int seat_id, int route_id) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getSeatPrice(seat_id, con, route_id);
        } catch (SQLException e) {
            logger.debug("Unable to get seat's price");
            throw new DbInteractionException("Get seat's price exception", e);
        }
    }

    boolean bookSeat(int routeId, String coachType, Seat seat, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(INSERT_SEAT_BOOKING);

        stmt.setInt(1, routeId);
        stmt.setInt(2, seat.getId());
        stmt.setString(3, "free");
        stmt.setInt(4, getSeatPrice(coachType, seat.getNumber()));
        stmt.setInt(5, seat.getCoachId());
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    boolean removeTrainFromRoute(int routeId, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(DELETE_TRAIN_FROM_ROUTE);

        stmt.setInt(1, routeId);
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    boolean setSeatTypeToFree(int route_id, int seat_id, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(SET_SEAT_TYPE_TO_FREE);

        stmt.setInt(1, route_id);
        stmt.setInt(2, seat_id);
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    boolean setSeatTypeToBusy(int route_id, int seat_id, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(SET_SEAT_TYPE_TO_BUSY);

        stmt.setInt(1, route_id);
        stmt.setInt(2, seat_id);
        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    List<Seat> getRouteSeats(int routeId, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_ROUTE_SEATS);

        stmt.setInt(1, routeId);
        ResultSet rs = stmt.executeQuery();
        List<Seat> seatList = new ArrayList<>();
        while (rs.next()) {
            Seat seat = new Seat();
            seat.setId(rs.getInt("seat_id"));
            seat.setStatus(rs.getString("seat_status"));
            seat.setPrice(rs.getInt("seat_price"));
            seat.setCoachId(rs.getInt("coach_id"));
            seatList.add(seat);
        }

        DbUtils.close(stmt);
        return seatList;
    }

    int getSeatPrice(int seat_id, Connection con, int route_id) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_SEAT_BY_ID);
        int price = -1;

        stmt.setInt(1, seat_id);
        stmt.setInt(2, route_id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            price = rs.getInt("seat_price");
        }

        DbUtils.close(stmt);
        return price;
    }

    private int getSeatPrice(String type, int seatNumber) {
        if (seatNumber <= 0) {
            throw new IllegalArgumentException("Seat number may be positive int number");
        }

        return switch (type) {
            case COACH_TYPE_DE_LUXE -> (int) DEFAULT_DE_LUXE_SEAT_PRICE;
            case COACH_TYPE_COMPARTMENT -> (int) DEFAULT_COMPARTMENT_SEAT_PRICE;
                /*if (seatNumber / 2 == 1) {
                    return (int) (DEFAULT_COMPARTMENT_SEAT_PRICE * COMPARTMENT_UPPER_SEAT_MODIFIER);
                } else {
                    return (int) (DEFAULT_COMPARTMENT_SEAT_PRICE * COMPARTMENT_LOWER_SEAT_MODIFIER);
                }*/
            case COACH_TYPE_BERTH -> (int) DEFAULT_BERTH_SEAT_PRICE;
                /*if (seatNumber / 2 == 1) {
                    return (int) (DEFAULT_BERTH_SEAT_PRICE * BERTH_UPPER_SEAT_MODIFIER);
                } else {
                    return (int) (DEFAULT_BERTH_SEAT_PRICE * BERTH_LOWER_SEAT_MODIFIER);
                }*/
            default -> throw new IllegalArgumentException("Unknown coach type");
        };
    }

    private int getSeatPrice(String type, int seatNumber, int travelTimeInMilliseconds) {
        if (seatNumber <= 0 || travelTimeInMilliseconds <= 0) {
            throw new IllegalArgumentException("Seat number must be positive number. Travel time must be positive number");
        }

        switch (type) {
            case COACH_TYPE_DE_LUXE:
                return (int) DEFAULT_DE_LUXE_SEAT_PRICE;
            case COACH_TYPE_COMPARTMENT:
                if (seatNumber / 2 == 1) {
                    return (int) ((DEFAULT_COMPARTMENT_SEAT_PRICE + 1000 * (travelTimeInMilliseconds / 86_400_000))
                            * COMPARTMENT_UPPER_SEAT_MODIFIER);
                } else {
                    return (int) ((DEFAULT_COMPARTMENT_SEAT_PRICE + 1000 * (travelTimeInMilliseconds / 86_400_000))
                            * COMPARTMENT_LOWER_SEAT_MODIFIER);
                }
            case COACH_TYPE_BERTH:
                if (seatNumber / 2 == 1) {
                    return (int) ((DEFAULT_BERTH_SEAT_PRICE + 1000 * (travelTimeInMilliseconds / 86_400_000))
                            * BERTH_UPPER_SEAT_MODIFIER);
                } else {
                    return (int) ((DEFAULT_BERTH_SEAT_PRICE + 1000 * (travelTimeInMilliseconds / 86_400_000))
                            * BERTH_LOWER_SEAT_MODIFIER);
                }
            default:
                throw new IllegalArgumentException("Unknown coach type");
        }
    }
}
