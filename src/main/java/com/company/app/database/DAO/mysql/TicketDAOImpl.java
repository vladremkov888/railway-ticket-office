package com.company.app.database.DAO.mysql;

import com.company.app.database.DAO.AbstractDAO;
import com.company.app.database.DAO.TicketDAO;
import com.company.app.database.utils.DbDataSource;
import com.company.app.entity.Ticket;
import com.company.app.database.utils.DbUtils;
import com.company.app.exception.DbInteractionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.company.app.database.utils.DbConstants.*;

public class TicketDAOImpl extends AbstractDAO implements TicketDAO {

    private final static Logger logger = LogManager.getLogger(TicketDAOImpl.class);

    public TicketDAOImpl() {}

    public TicketDAOImpl(DataSource ds) {
        this.dataSource = ds;
    }

    Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public List<Ticket> getTickets(int userId) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return getTickets(userId, con);
        } catch (SQLException e) {
            logger.debug("Unable to get ticket list");
            throw new DbInteractionException("Get ticket list exception", e);
        }
    }

    public boolean insertTicket(Ticket ticket) throws DbInteractionException {
        try (Connection con = getConnection()) {
            return insertTicket(ticket, con);
        } catch (SQLException e) {
            logger.debug("Unable to insert ticket");
            throw new DbInteractionException("Insert ticket exception", e);
        }
    }

    boolean insertTicket(Ticket ticket, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(INSERT_TICKET);

        stmt.setString(1, ticket.getFirstname());
        stmt.setString(2, ticket.getLastname());
        stmt.setString(3, ticket.getTicketType());
        stmt.setInt(4, ticket.getPrice());
        stmt.setInt(5, ticket.getCoach());
        stmt.setInt(6, ticket.getSeat());
        stmt.setInt(7, ticket.getUser());
        stmt.setInt(8, ticket.getRoute_id());
        stmt.setInt(9, ticket.getSeat_id());
        stmt.setLong(10, ticket.getTimeStamp().getTime());

        stmt.execute();

        DbUtils.close(stmt);
        return true;
    }

    List<Ticket> getTickets(int userId, Connection con) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(GET_TICKETS_BY_USER_ID);

        stmt.setInt(1, userId);
        ResultSet rs = stmt.executeQuery();
        List<Ticket> ticketList = new ArrayList<>();
        while (rs.next()) {
            Ticket ticket = new Ticket();
            ticket.setId(rs.getInt("id"));
            ticket.setFirstname(rs.getString("firstname"));
            ticket.setLastname(rs.getString("lastname"));
            ticket.setTicketType(rs.getString("ticket_type"));
            ticket.setPrice(rs.getInt("price"));
            ticket.setCoach(rs.getInt("coach"));
            ticket.setSeat(rs.getInt("seat"));
            ticket.setUser(rs.getInt("user_id"));
            ticket.setTimeStamp(new Date(rs.getLong("time_stamp")));
            ticketList.add(ticket);
        }

        DbUtils.close(stmt);
        return ticketList;
    }
}
