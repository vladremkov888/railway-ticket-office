package com.company.app.database.DAO;

import com.company.app.entity.Seat;
import com.company.app.exception.DbInteractionException;

import java.util.List;

public interface BookingManager {
    boolean bookSeat(int routeId, String coachType, Seat seat) throws DbInteractionException;

    boolean removeTrainFromRoute(int routeId) throws DbInteractionException;

    boolean setSeatTypeToFree(int route_id, int seat_id) throws DbInteractionException;

    boolean setSeatTypeToBusy(int route_id, int seat_id) throws DbInteractionException;

    List<Seat> getRouteSeats(int routeId) throws DbInteractionException;

    int getSeatPrice(int seat_id, int route_id) throws DbInteractionException;
}
