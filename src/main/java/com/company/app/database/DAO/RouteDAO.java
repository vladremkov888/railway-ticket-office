package com.company.app.database.DAO;

import com.company.app.entity.Route;
import com.company.app.exception.DbInteractionException;

import java.util.List;

public interface RouteDAO {
    boolean insertRoute(Route route) throws DbInteractionException;

    boolean deleteRoute(int id) throws DbInteractionException;

    boolean updateRoute(Route r) throws DbInteractionException;

    Route getRoute(int id) throws DbInteractionException;

    List<Route> getRoutes() throws DbInteractionException;

    List<Route> getRoutes(int sId, int fId, java.util.Date date) throws DbInteractionException;
}
