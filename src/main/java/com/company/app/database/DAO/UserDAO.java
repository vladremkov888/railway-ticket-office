package com.company.app.database.DAO;

import com.company.app.entity.User;
import com.company.app.exception.DbInteractionException;

public interface UserDAO {
    boolean insertUser(User user) throws DbInteractionException;

    User getUser(String login) throws DbInteractionException;
}
