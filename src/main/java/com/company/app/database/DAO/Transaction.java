package com.company.app.database.DAO;

import com.company.app.entity.Route;
import com.company.app.entity.Station;
import com.company.app.entity.Ticket;
import com.company.app.entity.Train;
import com.company.app.exception.DbInteractionException;

import java.util.List;

public interface Transaction {
    boolean insertTrain(Train train) throws DbInteractionException;

    boolean addTrainToRoute(int trainId, int routeId) throws DbInteractionException;

    boolean updateStations(List<Station> stationList) throws DbInteractionException;

    boolean deleteStations(List<Integer> ids) throws DbInteractionException;

    boolean updateRoutes(List<Route> routeList) throws DbInteractionException;

    boolean deleteRoutes(List<Integer> ids) throws DbInteractionException;

    boolean buyTickets(List<Ticket> tickets) throws DbInteractionException;

    boolean deleteTrains(List<Integer> ids) throws DbInteractionException;
}
