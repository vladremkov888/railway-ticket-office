package com.company.app.database.utils;

import com.company.app.controller.StationsControllerServlet;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.DbInteractionException;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DbDataSource {
    private static DbDataSource dbDataSource;
    private final DataSource ds;

    private final static Logger logger = LogManager.getLogger(DbDataSource.class);

    private DbDataSource() {
        try {
            logger.info("Connecting to database");
            logger.info("Setup datasource");
            ds = getDataSourceFromDBCP2();
            logger.info("Datasource setup successfully");
        } catch (Exception e) {
            logger.fatal("Datasource setup error");
            throw new RuntimeException("Datasource init error", e);
        }
    }

    private DataSource getDataSourceFromDBCP2() throws ApplicationException {
        final BasicDataSource ds;
        try (InputStream input = new FileInputStream("src/main/resources/main_db.properties")) {
            Properties prop = new Properties();
            prop.load(input);

            ds = new BasicDataSource();
            ds.setUsername(prop.getProperty("user"));
            ds.setPassword(prop.getProperty("password"));
            ds.setUrl(prop.getProperty("url"));
            ds.setDriverClassName(prop.getProperty("driver"));
            ds.setMaxTotal(Integer.parseInt(prop.getProperty("max_total")));
            ds.setMaxIdle(Integer.parseInt(prop.getProperty("max_idle")));
        } catch (IOException e) {
            logger.error("Database connection properties missing");
            throw new ApplicationException(e);
        }
        return ds;
    }

    //Causes a lot of problems
    private DataSource getDataSourceFromContext() throws NamingException {
        final DataSource ds;
        Context initContext = new InitialContext();
        Context envContext = (Context) initContext.lookup("java:/comp/env");
        ds = (DataSource) envContext.lookup("jdbc/railway_office");
        return ds;
    }

    public static synchronized DbDataSource getInstance() {
        if(dbDataSource == null) {
            dbDataSource = new DbDataSource();
        }
        return dbDataSource;
    }

    public DataSource getDataSource() {
        logger.trace("Data source provided");
        return ds;
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
