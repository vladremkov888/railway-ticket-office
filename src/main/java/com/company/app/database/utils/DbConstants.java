package com.company.app.database.utils;

public class DbConstants {
    //UserDAO requests
    public static final String INSERT_USER = "insert into user(login, password, email, firstname, lastname, phone, role)" +
            "values(?, ?, ?, ?, ?, ?, 'user')";
    public static final String GET_USER_BY_LOGIN = "select * from user where login=?";
    public static final String GET_USER_BY_LOGIN_AND_PASSWORD = "select * from user where login=? and password=?";

    //StationDAO requests
    public static final String INSERT_STATION = "insert into station(name) values(?)";
    public static final String GET_STATION_BY_NAME = "select * from station where name=?";
    public static final String GET_STATION_BY_ID = "select * from station where id=?";
    public static final String GET_STATIONS = "select * from station";
    public static final String UPDATE_STATION = "update station set name=? where id=?";
    public static final String DELETE_STATION = "delete from station where id=?";

    //TrainDAO requests
    public static final String INSERT_TRAIN = "insert into train(name) values(?)";
    public static final String INSERT_COACH = "insert into coach(type, number, train_id) values(?, ?, ?)";
    public static final String INSERT_SEAT = "insert into seat(number, coach_id) values(?, ?)";
    public static final String GET_TRAIN_BY_ID = "select * from train where id=?";
    public static final String GET_COACH_BY_ID = "select * from coach where id=?";
    public static final String GET_TRAINS = "select * from train";
    public static final String GET_COACHES = "select * from coach where train_id=?";
    public static final String GET_SEATS = "select * from seat where coach_id=?";
    public static final String GET_SEAT = "select * from seat where id=?";
    public static final String DELETE_TRAIN= "delete from train where id=?";

    //RouteDAO requests
    public static final String INSERT_ROUTE = "insert into route(arrival, departure, train_id, from_id, to_id) " +
            "values(?, ?, ?, ?, ?)";
    public static final String GET_ROUTE_BY_ID = "select * from route where id=?";
    public static final String GET_ROUTES = "select * from route";
    //public static final String GET_ROUTES_BY_STATIONS_AND_DATE = "select * from route where from_id=?, to_id=?, departure=?";
    public static final String GET_ROUTES_BY_STATIONS_AND_DATE = "select * from route where from_id=? and to_id=?";
    public static final String DELETE_ROUTE = "delete from route where id=?";
    public static final String UPDATE_ROUTE = "update route set arrival=?, departure=?, train_id=?, from_id=?, to_id=? where id=?";

    //PassengerDAO requests
    public static final String INSERT_TICKET = "insert into ticket(firstname, lastname, ticket_type, " +
            "price, coach, seat, user_id, seat_booking_route_id, seat_booking_seat_id, time_stamp) " +
            "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    public static final String GET_TICKETS_BY_USER_ID = "select * from ticket where user_id=?";

    //BookingManager requests
    public static final String INSERT_SEAT_BOOKING = "insert into seat_booking(route_id, seat_id, seat_status, seat_price, coach_id)" +
            "values(?, ?, ?, ?, ?)";
    public static final String DELETE_TRAIN_FROM_ROUTE = "delete from seat_booking where route_id=?";
    public static final String GET_ROUTE_SEATS = "select * from seat_booking where route_id=?";
    public static final String GET_SEAT_BY_ID = "select * from seat_booking where seat_id=? and route_id=?";
    public static final String SET_SEAT_TYPE_TO_FREE = "update seat_booking set seat_status='free' where route_id=? and seat_id=?";
    public static final String SET_SEAT_TYPE_TO_BUSY = "update seat_booking set seat_status='busy' where route_id=? and seat_id=?";

    //BookingManager price list
    public static final double DEFAULT_DE_LUXE_SEAT_PRICE = 3000;
    public static final double DEFAULT_COMPARTMENT_SEAT_PRICE = 2000;
    public static final double DEFAULT_BERTH_SEAT_PRICE = 1000;
    public static final double COMPARTMENT_UPPER_SEAT_MODIFIER = 1.0;
    public static final double COMPARTMENT_LOWER_SEAT_MODIFIER = 1.15;
    public static final double BERTH_UPPER_SEAT_MODIFIER = 1.0;
    public static final double BERTH_LOWER_SEAT_MODIFIER = 1.15;

    //Coach types
    public static final String COACH_TYPE_DE_LUXE = "DeLuxe";
    public static final String COACH_TYPE_BERTH = "Berth";
    public static final String COACH_TYPE_COMPARTMENT = "Compartment";
}
