package com.company.app.database.utils;

import com.company.app.controller.AdminPanelServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class DbUtils {

    private final static Logger logger = LogManager.getLogger(AdminPanelServlet.class);

    public static void close(AutoCloseable obj) {
        if (obj!=null) {
            try {
                obj.close();
            } catch (Exception e) {
                logger.warn("Resource closing exception");
            }
        }
    }
    public static void rollback(Connection con) {
        try {
            if (con!=null) {
                con.rollback();
            }
        } catch (SQLException e) {
            logger.warn("Database rollback exception");
        }
    }
}
