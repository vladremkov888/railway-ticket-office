package com.company.app.exception;

public class FatalApplicationException extends ApplicationException {
    public FatalApplicationException() {
    }

    public FatalApplicationException(String message) {
        super(message);
    }

    public FatalApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public FatalApplicationException(Throwable cause) {
        super(cause);
    }
}
