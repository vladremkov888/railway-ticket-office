package com.company.app.exception;

public class UnknownApplicationException extends ApplicationException {
    public UnknownApplicationException() {
    }

    public UnknownApplicationException(String message) {
        super(message);
    }

    public UnknownApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownApplicationException(Throwable cause) {
        super(cause);
    }
}
