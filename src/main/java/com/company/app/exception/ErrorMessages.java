package com.company.app.exception;

public enum ErrorMessages {
    AUTHORIZATION_ERROR("message.error.authorization"),
    ROUTE_DISPLAY_ERROR("message.error.book.display"),
    //Seats booking error
    ROUTE_BOOKING_ERROR("message.error.book.do"),
    BUY_TICKET_ERROR("message.error.buy"),
    SEND_MAIL_ERROR("message.error.mail"),
    REGISTRATION_ERROR("message.error.registration"),
    ROUTES_MANAGEMENT_ERROR("message.error.management.routes"),
    STATIONS_MANAGEMENT_ERROR("message.error.management.stations"),
    TRAINS_MANAGEMENT_ERROR("message.error.management.trains"),
    MY_TICKETS_DISPLAY_ERROR("message.error.cabinet.tickets.display"),
    CHANGE_LANG_ERROR("message.error.utils.lang");


    private final String mes;

    ErrorMessages(String mes) {
        this.mes = mes;
    }

    public String get() {
        return mes;
    }
}