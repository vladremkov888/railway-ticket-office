package com.company.app.exception;

public class DbInteractionException extends ApplicationException {
    public DbInteractionException() {
    }

    public DbInteractionException(String message) {
        super(message);
    }

    public DbInteractionException(String message, Throwable cause) {
        super(message, cause);
    }

    public DbInteractionException(Throwable cause) {
        super(cause);
    }
}
