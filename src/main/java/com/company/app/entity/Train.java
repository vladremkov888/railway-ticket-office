package com.company.app.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Train {
    private int id;
    private String name;
    private List<Coach> coachList;

    public Train() {
        coachList = new ArrayList<>();
        id = 0;
    }

    public Train(int id, String name, List<Coach> coachList) {
        this.id = id;
        this.name = name;
        this.coachList = coachList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Coach> getCoachList() {
        return coachList;
    }

    public void setCoachList(List<Coach> coachList) {
        this.coachList = coachList;
    }

    public void addCoach(Coach coach) {
        coachList.add(coach);
    }

    public Map<String, Long> getCoachesCount() {
        return coachList.stream()
                .map(Coach::getType)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Train train = (Train) o;
        return Objects.equals(name, train.name) && Objects.equals(coachList, train.coachList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, coachList);
    }

    @Override
    public String toString() {
        return "Train{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", coachList=" + coachList +
                '}';
    }
}
