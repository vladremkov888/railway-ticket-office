package com.company.app.entity;

import java.util.Objects;

public class Seat {
    private int id;
    private int number;
    private int price;
    private String status;

    private int coachId;

    public Seat() {
        id = 0;
    }

    public Seat(int id, int number, int coachId) {
        this.id = id;
        this.number = number;
        this.coachId = coachId;
    }

    public Seat(int id, int number, int price, String status, int coachId) {
        this.id = id;
        this.number = number;
        this.price = price;
        this.status = status;
        this.coachId = coachId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getCoachId() {
        return coachId;
    }

    public void setCoachId(int coachId) {
        this.coachId = coachId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seat seat = (Seat) o;
        return number == seat.number && coachId == seat.coachId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, coachId);
    }

    @Override
    public String toString() {
        return "Seat{" +
                "id=" + id +
                ", number=" + number +
                ", coachId=" + coachId +
                '}';
    }
}
