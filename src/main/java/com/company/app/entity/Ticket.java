package com.company.app.entity;

import java.util.Date;
import java.util.Objects;

public class Ticket {
    private int id;
    private String firstname;
    private String lastname;
    private String ticketType;
    private int user;
    private int coach;
    private int seat;
    private int price;
    private int route_id;
    private int seat_id;
    private Date timeStamp;
    private int coach_id;

    public Ticket() {
        id = 0;
    }

    public Ticket(int id, String firstname, String lastname, String ticketType, int user, int coach, int seat, int price) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.ticketType = ticketType;
        this.user = user;
        this.coach = coach;
        this.seat = seat;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public int getCoach() {
        return coach;
    }

    public void setCoach(int coach) {
        this.coach = coach;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getRoute_id() {
        return route_id;
    }

    public void setRoute_id(int route_id) {
        this.route_id = route_id;
    }

    public int getSeat_id() {
        return seat_id;
    }

    public void setSeat_id(int seat_id) {
        this.seat_id = seat_id;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getStatus() {
        return (timeStamp.compareTo(new Date())<1) ? "Expired" : "Active";
    }

    public int getCoach_id() {
        return coach_id;
    }

    public void setCoach_id(int coach_id) {
        this.coach_id = coach_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return coach == ticket.coach && seat == ticket.seat && price == ticket.price
                && Objects.equals(firstname, ticket.firstname) && Objects.equals(lastname, ticket.lastname)
                && Objects.equals(ticketType, ticket.ticketType) && Objects.equals(user, ticket.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstname, lastname, ticketType, user, coach, seat, price);
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", ticketType='" + ticketType + '\'' +
                ", user=" + user +
                ", coach=" + coach +
                ", seat=" + seat +
                ", price=" + price +
                ", route_id=" + route_id +
                ", seat_id=" + seat_id +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
