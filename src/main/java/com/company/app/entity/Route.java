package com.company.app.entity;

import java.util.*;

public class Route {
    private int id;
    private Date arrival;
    private Date departure;
    private int trainId;
    private int fromId;
    private int toId;

    private String trainName;
    private String fromName;
    private String toName;
    private Map<String, Integer> coachesInfo;
    private List<Integer> priceList;
    private List<Coach> coaches;

    private Coach coach;

    public Route() {
        id = 0;
    }

    public Route(int id, Date arrival, Date departure, int trainId, int fromId, int toId) {
        this.id = id;
        this.arrival = arrival;
        this.departure = departure;
        this.trainId = trainId;
        this.fromId = fromId;
        this.toId = toId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    public int getToId() {
        return toId;
    }

    public void setToId(int toId) {
        this.toId = toId;
    }

    public int getTrainId() {
        return trainId;
    }

    public void setTrainId(int trainId) {
        this.trainId = trainId;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public Map<String, Integer> getCoachesInfo() {
        return coachesInfo;
    }

    public void setCoachesInfo(Map<String, Integer> coachesInfo) {
        this.coachesInfo = coachesInfo;
    }

    public List<Integer> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<Integer> priceList) {
        this.priceList = priceList;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public List<Coach> getCoaches() {
        return coaches;
    }

    public void setCoaches(List<Coach> coaches) {
        this.coaches = coaches;
    }

    public String getTravelTime() {
        long time = departure.getTime() - arrival.getTime();
        StringBuilder sb = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        int days = (int) cal.getTime().getTime() / 86_400_000;
        int hours = cal.get(Calendar.HOUR_OF_DAY);
        int minutes = cal.get(Calendar.MINUTE);
        if(days > 0) {
            sb.append(days).append(", ");
            sb.append(hours).append(":");
            sb.append(minutes);
        } else {
            if(hours > 0) {
                sb.append(hours).append(":");
                sb.append(minutes);
            }
        }
        return sb.toString();
    }

    public String getArrivalTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(arrival.getTime());
        StringBuilder sb = new StringBuilder();
        sb.append(cal.get(Calendar.YEAR)).append("-");
        if(cal.get(Calendar.MONTH)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.MONTH)).append("-");
        if(cal.get(Calendar.DAY_OF_MONTH)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.DAY_OF_MONTH));
        sb.append(": ");
        if(cal.get(Calendar.HOUR_OF_DAY)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.HOUR_OF_DAY)).append(":");
        if(cal.get(Calendar.MINUTE)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.MINUTE));
        return sb.toString();
    }

    public String getDepartureTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(departure.getTime());
        StringBuilder sb = new StringBuilder();
        sb.append(cal.get(Calendar.YEAR)).append("-");
        if(cal.get(Calendar.MONTH)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.MONTH)).append("-");
        if(cal.get(Calendar.DAY_OF_MONTH)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.DAY_OF_MONTH));
        sb.append(": ");
        if(cal.get(Calendar.HOUR_OF_DAY)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.HOUR_OF_DAY)).append(":");
        if(cal.get(Calendar.MINUTE)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.MINUTE));
        return sb.toString();
    }

    public String getHTMLFormatTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime());
        StringBuilder sb = new StringBuilder();
        sb.append(cal.get(Calendar.YEAR)).append("-");
        if(cal.get(Calendar.MONTH)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.MONTH)).append("-");
        if(cal.get(Calendar.DAY_OF_MONTH)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.DAY_OF_MONTH));
        sb.append("T");
        if(cal.get(Calendar.HOUR_OF_DAY)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.HOUR_OF_DAY)).append(":");
        if(cal.get(Calendar.MINUTE)<10){
            sb.append(0);
        }
        sb.append(cal.get(Calendar.MINUTE));
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return trainId == route.trainId && fromId == route.fromId && toId == route.toId
                && Objects.equals(arrival, route.arrival)
                && Objects.equals(departure, route.departure);
    }

    @Override
    public int hashCode() {
        return Objects.hash(arrival, departure, trainId, fromId, toId);
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", arrival=" + arrival +
                ", departure=" + departure +
                ", trainId=" + trainId +
                ", from=" + fromId +
                ", to=" + toId +
                '}';
    }
}
