package com.company.app.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.company.app.database.utils.DbConstants.*;

public class Coach {
    private int id;
    private String type;
    private int number;
    private List<Seat> seatList;

    public Coach() {
        seatList = new ArrayList<>();
        id = 0;
    }

    public Coach(int id, String type, int number, List<Seat> seatList) {
        this.id = id;
        this.type = type;
        this.number = number;
        this.seatList = seatList;
    }

    public static Coach getDeLuxeCoach() {
        Coach coach = new Coach();
        coach.setId(0);
        coach.setType(COACH_TYPE_DE_LUXE);
        coach.setNumber(0);
        for(int i = 1; i < 21; i++) {
            Seat seat = new Seat();
            seat.setId(0);
            seat.setNumber(i);
            coach.addSeat(seat);
        }
        return coach;
    }

    public static Coach getCompartmentCoach() {
        Coach coach = new Coach();
        coach.setId(0);
        coach.setType(COACH_TYPE_COMPARTMENT);
        coach.setNumber(0);
        for(int i = 1; i < 37; i++) {
            Seat seat = new Seat();
            seat.setId(0);
            seat.setNumber(i);
            coach.addSeat(seat);
        }
        return coach;
    }

    public static Coach getBerthCoach() {
        Coach coach = new Coach();
        coach.setId(0);
        coach.setType(COACH_TYPE_BERTH);
        coach.setNumber(0);
        for(int i = 1; i < 55; i++) {
            Seat seat = new Seat();
            seat.setId(0);
            seat.setNumber(i);
            coach.addSeat(seat);
        }
        return coach;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<Seat> getSeatList() {
        return seatList;
    }

    public void setSeatList(List<Seat> seatList) {
        this.seatList = seatList;
    }

    public void addSeat(Seat seat) {
        seatList.add(seat);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coach coach = (Coach) o;
        return number == coach.number && Objects.equals(type, coach.type) && Objects.equals(seatList, coach.seatList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, number, seatList);
    }

    @Override
    public String toString() {
        return "Coach{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", number=" + number +
                ", seatList=" + seatList +
                '}';
    }
}
