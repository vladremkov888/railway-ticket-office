package com.company.app.validation;

import com.company.app.entity.Station;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StationValidator {
    private final Station station;

    private static final String nameValidationPattern = "[\\w\\d]{1,30}";

    public StationValidator(Station station) {
        this.station = station;
    }

    public boolean isStationValid() {
        if (station.getName() == null) {
            return false;
        }
        Pattern p = Pattern.compile(nameValidationPattern);
        Matcher m = p.matcher(station.getName());
        return m.matches();
    }
}