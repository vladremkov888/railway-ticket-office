package com.company.app.validation;

import com.company.app.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator {
    private static final String PASSWORD_VALIDATION_PATTERN = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,20}$";
    private static final String LOGIN_VALIDATION_PATTERN = "[A-Za-z\\d]{3,12}";
    private static final String FIRSTNAME_VALIDATION_PATTERN = "[A-Z][a-z]{0,63}";
    private static final String LASTNAME_VALIDATION_PATTERN = "[A-Z][a-z]{0,63}";
    private static final String EMAIL_VALIDATION_PATTERN = "^[a-zA-Z\\d_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z\\d.-]+$";
    private static final String PHONE_NUMBER_VALIDATION_PATTERN = "\\+\\d{12}";

    private final static Logger logger = LogManager.getLogger(UserValidator.class);

    private final User user;

    public UserValidator(User user) {
        this.user = user;
    }

    public boolean isUserValid() {
        return isLoginValid() &&
                isPasswordValid() &&
                isFirstnameValid() &&
                isLastnameValid() &&
                isEmailValid() &&
                isPhoneNumberValid();
    }

    public boolean isPasswordValid() {
        if(user.getPassword()==null) {
            return false;
        }
        Pattern p = Pattern.compile(PASSWORD_VALIDATION_PATTERN);
        Matcher m = p.matcher(user.getPassword());
        logger.trace("Is user password valid: "+m.matches());
        return m.matches();
    }

    public boolean isLoginValid() {
        if(user.getLogin()==null) {
            return false;
        }
        Pattern p = Pattern.compile(LOGIN_VALIDATION_PATTERN);
        Matcher m = p.matcher(user.getLogin());
        logger.trace("Is user login valid: "+m.matches());
        return m.matches();
    }

    public boolean isFirstnameValid() {
        if(user.getFirstName()==null) {
            return false;
        }
        Pattern p = Pattern.compile(FIRSTNAME_VALIDATION_PATTERN);
        Matcher m = p.matcher(user.getFirstName());
        logger.trace("Is user firstname valid: "+m.matches());
        return m.matches();
    }

    public boolean isLastnameValid() {
        if(user.getLastName()==null) {
            return false;
        }
        Pattern p = Pattern.compile(LASTNAME_VALIDATION_PATTERN);
        Matcher m = p.matcher(user.getLastName());
        logger.trace("Is user lastname valid: "+m.matches());
        return m.matches();
    }

    public boolean isEmailValid() {
        if(user.getEmail()==null) {
            return false;
        }
        Pattern p = Pattern.compile(EMAIL_VALIDATION_PATTERN);
        Matcher m = p.matcher(user.getEmail());
        logger.trace("Is user email valid: "+m.matches());
        return m.matches();
    }

    public boolean isPhoneNumberValid() {
        if(user.getPhoneNumber()==null) {
            return false;
        }
        Pattern p = Pattern.compile(PHONE_NUMBER_VALIDATION_PATTERN);
        Matcher m = p.matcher(user.getPhoneNumber());
        logger.trace("Is user phone number valid: "+m.matches());
        return m.matches();
    }
}
