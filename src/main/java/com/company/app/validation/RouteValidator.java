package com.company.app.validation;

import com.company.app.entity.Route;

public class RouteValidator {
    private Route route;

    public RouteValidator(Route route) {
        this.route = route;
    }

    public boolean isRouteValid() {
        return isDateValid() &&
                isTrainIdValid() &&
                isFromValid() &&
                isToValid();
    }

    public boolean isDateValid() {
        return route.getDeparture().compareTo(route.getArrival()) < 0;
    }

    public boolean isTrainIdValid() {
        return route.getTrainId() > 0;
    }

    public boolean isFromValid() {
        return route.getFromId() > 0;
    }

    public boolean isToValid() {
        return route.getToId() > 0;
    }
}
