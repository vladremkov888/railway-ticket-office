package com.company.app.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("*.jsp")
public class JSPAccessFilter implements Filter {

    private final static Logger logger = LogManager.getLogger(AdminPanelAccessFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("JSP access filter initialized");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        try {
            HttpServletRequest req = (HttpServletRequest) servletRequest;
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            resp.sendRedirect(req.getContextPath()+"/cabinet");
            logger.debug("Jsp access prevented");
        } catch (Exception e) {
            logger.error("Failed to prevent jsp access", e);
        }
    }

    @Override
    public void destroy() {
        logger.info("JSP access filter destroyed");
    }
}
