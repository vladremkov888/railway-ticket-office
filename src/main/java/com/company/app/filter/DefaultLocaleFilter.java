package com.company.app.filter;

import com.company.app.database.utils.DbDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

@WebFilter("/*")
public class DefaultLocaleFilter implements Filter {

    private final static Logger logger = LogManager.getLogger(DefaultLocaleFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("Locale filter initialized");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        try {
            HttpServletRequest req = (HttpServletRequest) servletRequest;
            HttpSession ses = req.getSession();
            Locale locale = (Locale) ses.getAttribute("locale");
            if (locale == null) {
                ses.setAttribute("locale", new Locale("en"));
                logger.debug("Default locale set to (en)");
            }
        } catch (Exception e) {
            logger.warn("Failed to set default locale");
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        logger.info("Locale filter destroyed");
    }
}
