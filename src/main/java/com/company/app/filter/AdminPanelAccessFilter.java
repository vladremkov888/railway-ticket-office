package com.company.app.filter;

import com.company.app.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/admin_edit/*")
public class AdminPanelAccessFilter implements Filter {

    private final static Logger logger = LogManager.getLogger(AdminPanelAccessFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("Admin panel access filter initialized");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        try {
            HttpServletRequest req = (HttpServletRequest) servletRequest;
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            HttpSession ses = req.getSession();
            User user = (User) ses.getAttribute("user");
            if (user != null && user.getRole().equals("admin")) {
                logger.debug("Admin access the admin panel");
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                logger.debug("User access the admin panel. Connection rejected");
                resp.sendRedirect(req.getContextPath()+"/cabinet");
            }
        } catch (Exception e) {
            logger.error("Access rights check failed", e);
        }
    }

    @Override
    public void destroy() {
        logger.info("Admin panel access filter destroyed");
    }
}
