package com.company.app.filter;

import com.company.app.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/book/*")
public class BookingAccessFilter implements Filter {

    private final static Logger logger = LogManager.getLogger(DefaultLocaleFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("Booking access filter initialized");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        HttpSession ses = req.getSession();
        User user = (User) ses.getAttribute("user");
        if(req.getMethod().equalsIgnoreCase("POST")){
            if (user == null) {
                logger.debug("Unauthorized user is trying to book tickets");
                resp.sendRedirect(req.getContextPath()+"/auth");
            } else if (user.getRole().equals("admin")) {
                logger.debug("Admin is trying to book tickets");
                resp.sendRedirect(req.getContextPath()+"/auth");
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        logger.info("Booking access filter destroyed");
    }
}
