package com.company.app.service;

import com.company.app.entity.User;
import com.company.app.exception.ApplicationException;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    User registerUser(User user) throws ApplicationException;

    User logInUser(String login, String password) throws ApplicationException;

    User editUser() throws ApplicationException;

    boolean deleteUser() throws ApplicationException;
}
