package com.company.app.service;

import com.company.app.entity.Station;
import com.company.app.exception.ApplicationException;

import java.util.List;

public interface StationService {
    List<Station> getStationList() throws ApplicationException;

    boolean addStation(String name) throws ApplicationException;

    boolean editStations(String[] keys, String[] stationNames) throws ApplicationException;

    boolean deleteStations(String[] checkboxIds) throws ApplicationException;
}
