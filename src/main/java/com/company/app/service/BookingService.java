package com.company.app.service;

import com.company.app.entity.Coach;
import com.company.app.entity.Route;
import com.company.app.entity.Ticket;
import com.company.app.entity.User;
import com.company.app.exception.ApplicationException;

import java.util.List;
import java.util.Map;

public interface BookingService {

    List<Route> getRouteListToDisplay(String start, String finish, String sDate) throws ApplicationException;

    Route getRouteToDisplay(String sId) throws ApplicationException;

    Coach getCoachToDisplay(int routeId, String coachId) throws ApplicationException;

    List<Ticket> bookTickets(String[] seats, User user, String route_id) throws ApplicationException;

    List<Coach> getWagons(int routeId) throws ApplicationException;

    Map<String, Integer> getCoachesInfo(int routeId) throws ApplicationException;

    boolean isFreeSeatsAvailable(int routeId) throws ApplicationException;

    List<Integer> getPriceList(List<String> coachTypeList) throws ApplicationException;

    void setRouteStationsNames(List<Route> list) throws ApplicationException;

    void setRouteTrainName(List<Route> list) throws ApplicationException;
}
