package com.company.app.service;

import com.company.app.entity.Ticket;
import com.company.app.exception.ApplicationException;

import java.util.List;

public interface CartService {
    boolean buyTickets(List<Ticket> ticketList) throws ApplicationException;

    List<Ticket> getTicketList(int userId) throws ApplicationException;

}
