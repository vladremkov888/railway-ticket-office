package com.company.app.service;

import com.company.app.database.DAO.*;
import com.company.app.database.DAO.factory.DaoFactory;
import com.company.app.database.DAO.factory.DaoType;

public abstract class AbstractService {
    protected BookingManager bookingManager;
    protected RouteDAO routeDAO;
    protected StationDAO stationDAO;
    protected TicketDAO ticketDAO;
    protected TrainDAO trainDAO;
    protected Transaction transaction;
    protected UserDAO userDAO;

    {
        bookingManager = (BookingManager) DaoFactory.getDao(DaoType.BOOKING_MANAGER);
        routeDAO = (RouteDAO) DaoFactory.getDao(DaoType.ROUTE_DAO);
        stationDAO = (StationDAO) DaoFactory.getDao(DaoType.STATION_DAO);
        ticketDAO = (TicketDAO) DaoFactory.getDao(DaoType.TICKET_DAO);
        trainDAO = (TrainDAO) DaoFactory.getDao(DaoType.TRAIN_DAO);
        transaction = (Transaction) DaoFactory.getDao(DaoType.TRANSACTION);
        userDAO = (UserDAO) DaoFactory.getDao(DaoType.USER_DAO);
    }
}
