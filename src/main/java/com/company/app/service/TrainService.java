package com.company.app.service;

import com.company.app.entity.Train;
import com.company.app.exception.ApplicationException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface TrainService {
    List<Train> getTrainList() throws ApplicationException;

    List<Train> getTrainListWithoutCoaches() throws ApplicationException;

    boolean addTrain(String name, String[] coaches) throws ApplicationException;

    boolean editTrain() throws ApplicationException;

    boolean deleteTrain(String[] ids) throws ApplicationException;
}
