package com.company.app.service.factory;

import com.company.app.service.AbstractService;

public class ServiceFactory {
    public static AbstractService getService(ServiceType type) {
        return type.getService();
    }
}
