package com.company.app.service.factory;

import com.company.app.service.AbstractService;
import com.company.app.service.implementation.*;

public enum ServiceType {
    BOOKING_SERVICE(new BookingServiceImpl()),
    CART_SERVICE(new CartServiceImpl()),
    ROUTE_SERVICE(new RouteServiceImpl()),
    STATION_SERVICE(new StationServiceImpl()),
    TRAIN_SERVICE(new TrainServiceImpl()),
    USER_SERVICE(new UserServiceImpl());

    private final AbstractService service;

    ServiceType(AbstractService service) {
        this.service = service;
    }

    public AbstractService getService() {
        return service;
    }
}
