package com.company.app.service;

import com.company.app.entity.Route;
import com.company.app.exception.ApplicationException;

import java.util.List;

public interface RouteService {
    List<Route> getRouteList() throws ApplicationException;

    boolean addRoute(String arrival, String departure, String train, String from, String to) throws ApplicationException;

    boolean editRoutes(String[] arrival, String[] departure, String[] train, String[] from, String[] to, String[] checkbox) throws ApplicationException;

    boolean deleteRoutes(String[] checkbox) throws ApplicationException;
}
