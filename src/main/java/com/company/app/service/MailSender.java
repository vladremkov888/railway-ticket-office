package com.company.app.service;

import jakarta.mail.MessagingException;

public interface MailSender {
    void sendEmail(String toAddress, String subject, String message) throws MessagingException;
}
