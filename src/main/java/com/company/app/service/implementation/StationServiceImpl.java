package com.company.app.service.implementation;

import com.company.app.controller.StationsControllerServlet;
import com.company.app.database.DAO.StationDAO;
import com.company.app.database.DAO.Transaction;
import com.company.app.entity.Station;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.DbInteractionException;
import com.company.app.service.AbstractService;
import com.company.app.service.StationService;
import com.company.app.validation.StationValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class StationServiceImpl extends AbstractService implements StationService {
    private final static Logger logger = LogManager.getLogger(StationsControllerServlet.class);

    public StationServiceImpl() {}

    public StationServiceImpl(StationDAO stationDAO, Transaction transaction) {
        this.stationDAO = stationDAO;
        this.transaction = transaction;
    }

    @Override
    public List<Station> getStationList() throws ApplicationException {
        List<Station> stations;
        try {
            stations = stationDAO.getStations();
            return stations;
        } catch (DbInteractionException e) {
            logger.error("Error during getting stations");
            throw new ApplicationException("Error during getting station list", e);
        }
    }

    @Override
    public boolean addStation(String name) throws ApplicationException {
        Station station = new Station(name);
        StationValidator validator = new StationValidator(station);
        if(!validator.isStationValid()) {
            logger.debug("Station is invalid");
            return false;
        }

        try {
            stationDAO.insertStation(station);
        } catch (DbInteractionException e) {
            logger.error("Adding new station error");
            throw new ApplicationException("Error during adding new station", e);
        }

        return true;
    }

    public boolean editStations(String[] keys, String[] stationNames) throws ApplicationException {
        logger.debug("Stations service impl #edit stations");
        if(stationNames.length == 0) {
            return true;
        }
        if(stationNames.length != keys.length) {
            logger.error("Error during editing stations");
            throw new ApplicationException("Error during editing stations");
        }

        List<Station> stationList = Arrays.stream(stationNames)
                .map(Station::new)
                .toList();
        List<Integer> keysList = Arrays.stream(keys)
                .map(Integer::parseInt)
                .toList();

        try {
            for (int i = 0; i < stationList.size(); i++) {
                stationList.get(i).setId(keysList.get(i));
            }
            transaction.updateStations(stationList);
        } catch (DbInteractionException e) {
            logger.error("Error during editing stations");
            throw new ApplicationException("Error during editing stations", e);
        }

        return true;
    }

    @Override
    public boolean deleteStations(String[] stationIds) throws ApplicationException {
        logger.debug("Stations service impl #delete station");
        if(stationIds.length == 0) {
            return true;
        }

        List<Integer> keysList = Arrays.stream(stationIds)
                .map(Integer::parseInt)
                .toList();

        try {
            transaction.deleteStations(keysList);
        } catch (DbInteractionException e) {
            logger.error("Error during deleting stations");
            throw new ApplicationException("Error during deleting stations", e);
        }

        return true;
    }
}
