package com.company.app.service.implementation;


import com.company.app.service.MailSender;
import jakarta.mail.*;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

public class MailSenderImpl implements MailSender {

    private final static Logger logger = LogManager.getLogger(MailSender.class);

    private final Properties properties;
    private final String user;
    private final String pass;

    public MailSenderImpl() {
        properties = new Properties();
        File file = new File("D:\\Tomcat\\webapps\\railway-ticket-office\\WEB-INF\\classes\\smtpserver.properties");
        System.out.println(file.getAbsoluteFile());
        try (InputStream input = new FileInputStream(file.getAbsoluteFile())) {
            properties.load(input);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        user = properties.getProperty("user");
        pass = properties.getProperty("pass");
    }

    public void sendEmail(String toAddress, String subject, String message) throws MessagingException {
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, pass);
            }
        };

        Session session = Session.getInstance(properties, authenticator);

        Message emessage = new MimeMessage(session);
        emessage.setFrom(new InternetAddress(user));
        emessage.setRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
        emessage.setSubject(subject);
        emessage.setText(message);
        emessage.setSentDate(new Date());

        Transport.send(emessage);
        logger.debug("Send email successfully");
    }
}
