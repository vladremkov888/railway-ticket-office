package com.company.app.service.implementation;

import com.company.app.database.DAO.UserDAO;
import com.company.app.entity.User;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.DbInteractionException;
import com.company.app.service.AbstractService;
import com.company.app.service.UserService;
import com.company.app.validation.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserServiceImpl extends AbstractService implements UserService {
    private final static Logger logger = LogManager.getLogger(UserServiceImpl.class);

    public UserServiceImpl() {}

    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public User registerUser(User user) throws ApplicationException {
        logger.debug("User service impl #register user");

        UserValidator userValidator = new UserValidator(user);
        if(userValidator.isUserValid()) {
            try {
                if(userDAO.getUser(user.getLogin())==null) {
                    userDAO.insertUser(user);
                    return user;
                }
            } catch (DbInteractionException e) {
                logger.error("Error during user registration");
                throw new ApplicationException("Fail to register user", e);
            }
        }

        logger.debug("Invalid user or user already exists");
        return null;
    }

    @Override
    public User logInUser(String login, String password) throws ApplicationException {
        User user;

        try {
            user = userDAO.getUser(login);
            if(user != null) {
                if(user.getPassword().equals(password)) {
                    return user;
                }
                logger.debug("Invalid password");
            }
            logger.debug("Invalid login");
            return null;
        } catch (DbInteractionException e) {
            logger.error("User authorization error");
            throw new ApplicationException("User log in error", e);
        }
    }

    @Override
    public User editUser() throws ApplicationException {
        return null;
    }

    @Override
    public boolean deleteUser() throws ApplicationException {
        return false;
    }
}
