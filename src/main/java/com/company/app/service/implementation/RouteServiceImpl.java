package com.company.app.service.implementation;

import com.company.app.database.DAO.BookingManager;
import com.company.app.database.DAO.RouteDAO;
import com.company.app.database.DAO.Transaction;
import com.company.app.entity.Route;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.DbInteractionException;
import com.company.app.service.AbstractService;
import com.company.app.service.RouteService;
import com.company.app.validation.RouteValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class RouteServiceImpl extends AbstractService implements RouteService {
    private final static Logger logger = LogManager.getLogger(RouteServiceImpl.class);

    public RouteServiceImpl() {}

    public RouteServiceImpl(RouteDAO routeDAO, BookingManager bookingManager, Transaction transaction) {
        this.routeDAO = routeDAO;
        this.bookingManager = bookingManager;
        this.transaction = transaction;
    }

    @Override
    public List<Route> getRouteList() throws ApplicationException {
        List<Route> routes;
        try {
            routes = routeDAO.getRoutes();
            logger.debug("Routes service. Successfully gotten routes");
            return routes;
        } catch (DbInteractionException e) {
            logger.error("Error during getting routes");
            throw new ApplicationException("Error during getting route list", e);
        }
    }

    @Override
    public boolean addRoute(String arrival, String departure, String train, String from, String to) throws ApplicationException {
        Date arrival1;
        Date departure1;
        int trainId1;
        int from1;
        int to1;
        try {
            arrival1 = parseDate(arrival);
            departure1 = parseDate(departure);
            trainId1 = Integer.parseInt(train);
            from1 = Integer.parseInt(from);
            to1 = Integer.parseInt(to);
        } catch (Exception e) {
            logger.warn("Route parameters parsing error", e);
            return false;
        }

        Route route = new Route();
        route.setArrival(arrival1);
        route.setDeparture(departure1);
        route.setTrainId(trainId1);
        route.setFromId(from1);
        route.setToId(to1);

        RouteValidator validator = new RouteValidator(route);
        if(!validator.isRouteValid()) {
            logger.debug("Route is invalid");
            return false;
        }

        try {
            routeDAO.insertRoute(route);
            transaction.addTrainToRoute(route.getTrainId(), route.getId());
        } catch (DbInteractionException e) {
            logger.error("Adding new route error", e);
            throw new ApplicationException("Error during adding new route", e);
        }

        return true;
    }

    @Override
    public boolean editRoutes(String[] arrival, String[] departure, String[] train, String[] from, String[] to, String[] checkbox) throws ApplicationException {

        if(checkbox.length < 1) {
            return true;
        }

        List<Integer> key = new ArrayList<>();
        List<Date> arrivals = new ArrayList<>();
        List<Date> departures = new ArrayList<>();
        List<Integer> trainId = new ArrayList<>();
        List<Integer> froms = new ArrayList<>();
        List<Integer> tos = new ArrayList<>();

        try {
            for (int i = 0; i < checkbox.length; i++) {
                key.add(Integer.parseInt(checkbox[i]));
                arrivals.add(parseDate(arrival[i]));
                departures.add(parseDate(departure[i]));
                trainId.add(Integer.parseInt(train[i]));
                froms.add(Integer.parseInt(from[i]));
                tos.add(Integer.parseInt(to[i]));
            }
        } catch (Exception e) {
            logger.error("Error during routes' parameters parsing", e);
            return false;
        }

        List<Route> routes = new ArrayList<>();
        for (int i = 0; i < key.size(); i++) {
            Route route = new Route();
            route.setId(key.get(i));
            route.setDeparture(departures.get(i));
            route.setArrival(arrivals.get(i));
            route.setTrainId(trainId.get(i));
            route.setFromId(froms.get(i));
            route.setToId(tos.get(i));
            RouteValidator validator = new RouteValidator(route);
            if(validator.isRouteValid()) {
                routes.add(route);
            } else {
                logger.error("One of the routes is invalid");
                return false;
            }
        }

        try {
            transaction.updateRoutes(routes);
        } catch (DbInteractionException e) {
            logger.error("Updating routes error", e);
            throw new DbInteractionException("Routes updating exception", e);
        }

        return true;
    }

    @Override
    public boolean deleteRoutes(String[] checkbox) throws ApplicationException {
        if(checkbox.length == 0) {
            return true;
        }

        List<Integer> ids = Arrays.stream(checkbox)
                .map(Integer::parseInt)
                .toList();

        try {
            transaction.deleteRoutes(ids);
        } catch (DbInteractionException e) {
            logger.error("Error during deleting routes");
            throw new ApplicationException("Error during deleting routes", e);
        }

        return true;
    }

    private Date parseDate(String s) throws ParseException {
        String[] ss = s.split("T");
        String[] sss = ss[1].split(":");
        int hours = Integer.parseInt(sss[0]);
        int minutes = Integer.parseInt(sss[1]);
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(ss[0]);
        date.setTime(date.getTime() + hours * 3_600_000L + minutes * 60_000L);
        return date;
    }
}
