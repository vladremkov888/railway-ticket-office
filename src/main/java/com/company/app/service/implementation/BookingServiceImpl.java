package com.company.app.service.implementation;

import com.company.app.database.DAO.BookingManager;
import com.company.app.database.DAO.RouteDAO;
import com.company.app.database.DAO.StationDAO;
import com.company.app.database.DAO.TrainDAO;
import com.company.app.entity.*;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.DbInteractionException;
import com.company.app.service.AbstractService;
import com.company.app.service.BookingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.company.app.database.utils.DbConstants.*;

public class BookingServiceImpl extends AbstractService implements BookingService {

    private final static Logger logger = LogManager.getLogger(BookingServiceImpl.class);

    public BookingServiceImpl() {}

    @Override
    public List<Route> getRouteListToDisplay(String start, String finish, String sDate) throws ApplicationException {
        Date date;
        try {
            date = parseDate(sDate);
        } catch (ParseException e) {
            logger.error("Date parsing exception", e);
            throw new ApplicationException("Parsing parameter exception", e);
        }
        if(start==null || finish==null) {
            logger.error("Invalid parameters error");
            throw new ApplicationException("Null parameters exception");
        }

        List<Route> routeList = new ArrayList<>();
        Station startStation;
        Station finishStation;

        try {
            startStation = stationDAO.getStation(start);
            finishStation = stationDAO.getStation(finish);
        }catch (DbInteractionException e) {
            logger.error("Get station by name exception", e);
            throw new ApplicationException("Get station by name exception", e);
        }
        if(startStation==null || finishStation==null) {
            logger.debug("No stations found. Returning blank list");
            return routeList;
        }

        try {
            routeList = routeDAO.getRoutes(startStation.getId(), finishStation.getId(), date);
        }catch (DbInteractionException e) {
            logger.error("Error during getting sorted route list");
            throw new ApplicationException("Get routes error", e);
        }

        logger.debug("Found "+routeList.size()+" routes to display");
        return routeList;
    }

    @Override
    public Route getRouteToDisplay(String sId) throws ApplicationException {
        int id;
        try {
            id = Integer.parseInt(sId);
        } catch (NumberFormatException e) {
            logger.error("Parameter parsing exception", e);
            throw new ApplicationException("Parameter parsing exception", e);
        }

        Route r = routeDAO.getRoute(id);
        if (r==null) {
            return null;
        }

        try {
            r.setFromName(stationDAO.getStation(r.getFromId()).getName());
            r.setToName(stationDAO.getStation(r.getToId()).getName());
            r.setTrainName(trainDAO.getTrainWithoutCoaches(r.getTrainId()).getName());
        } catch (DbInteractionException e) {
            logger.error("Naming error", e);
            throw new ApplicationException("Route fields naming exception", e);
        }

        return r;
    }

    @Override
    public Map<String, Integer> getCoachesInfo(int routeId) throws ApplicationException {
        try {
            List<Coach> coachList = getWagons(routeId);
            Map<String, Integer> coachInfo = new HashMap<>();
            for (Coach coach : coachList) {
                String type = coach.getType();
                int freeSeatsNumber = (int) coach.getSeatList()
                        .stream()
                        .filter((x) -> x.getStatus().equals("free"))
                        .count();
                if (coachInfo.containsKey(type)) {
                    coachInfo.replace(type, coachInfo.get(type) + freeSeatsNumber);
                } else {
                    coachInfo.put(coach.getType(), freeSeatsNumber);
                }
            }
            return coachInfo;
        } catch (Exception e) {
            logger.error("Get coaches information exception", e);
            throw new ApplicationException("Get coaches information exception", e);
        }
    }

    @Override
    public boolean isFreeSeatsAvailable(int routeId) throws ApplicationException {
        List<Seat> list;
        try {
            list = bookingManager.getRouteSeats(routeId);
        } catch (DbInteractionException e) {
            logger.error("Get route seats exception", e);
            throw new ApplicationException("Get route seats exception", e);
        }
        logger.debug("Number of seats in route "+routeId+" is "+list.size());
        for (Seat s: list) {
            System.out.println(s.getStatus());
        }
        return list.stream().anyMatch((x) -> x.getStatus().equals("free"));
    }

    @Override
    public List<Integer> getPriceList(List<String> coachTypeList) throws ApplicationException {
        List<Integer> priceList = new ArrayList<>();
        for (String el: coachTypeList) {
            switch (el) {
                case COACH_TYPE_DE_LUXE -> priceList.add((int) DEFAULT_DE_LUXE_SEAT_PRICE);
                case COACH_TYPE_BERTH -> priceList.add((int) DEFAULT_BERTH_SEAT_PRICE);
                case COACH_TYPE_COMPARTMENT -> priceList.add((int) DEFAULT_COMPARTMENT_SEAT_PRICE);
            }
        }
        return priceList;
    }

    public Coach getCoachToDisplay(int routeId, String coachSId) throws ApplicationException {
        int coachId;
        try {
            coachId = Integer.parseInt(coachSId);
        }catch (NumberFormatException e) {
            throw new ApplicationException("Parameter parsing exception", e);
        }
        if (coachId==0) {
            return getWagons(routeId).get(0);
        }
        List<Coach> coachList = getWagons(routeId)
                .stream()
                .filter((x)->x.getId()==coachId)
                .toList();
        if(coachList.isEmpty()) {
            throw new ApplicationException("Coach not found exception");
        }
        return coachList.get(0);
    }

    public List<Ticket> bookTickets(String[] seats, User user, String route_id) throws ApplicationException {
        List<Integer> keys;
        int routeId;
        try {
            keys = Arrays.stream(seats).map(Integer::parseInt).toList();
            routeId = Integer.parseInt(route_id);
        } catch (NumberFormatException e) {
            throw new ApplicationException("Parameters parsing exception", e);
        }
        List<Seat> seatList = new ArrayList<>();

        for (Integer el: keys) {
            seatList.add(trainDAO.getSeat(el));
        }
        List<Ticket> ticketList = new ArrayList<>();
        int i = 0;
        for (Seat el: seatList) {
            Ticket ticket = new Ticket();
            ticket.setId(i);
            ticket.setFirstname(user.getFirstName());
            ticket.setLastname(user.getLastName());
            ticket.setTicketType("Common");
            ticket.setSeat(el.getNumber());
            ticket.setUser(user.getId());
            ticket.setCoach(trainDAO.getCoach(el.getCoachId()).getNumber());
            ticket.setPrice(bookingManager.getSeatPrice(el.getId(), routeId));
            ticket.setRoute_id(routeId);
            ticket.setSeat_id(el.getId());
            ticket.setCoach_id(el.getCoachId());
            ticket.setTimeStamp(routeDAO.getRoute(routeId).getDeparture());
            i++;
            ticketList.add(ticket);
        }
        return ticketList;
    }

    public List<Coach> getWagons(int routeId) throws ApplicationException {
        try {
            List<Seat> seatList = bookingManager.getRouteSeats(routeId);
            Map<Integer, List<Seat>> map = seatList.stream()
                    .collect(Collectors.groupingBy(Seat::getCoachId));
            Set<Integer> set = map.keySet();
            List<Coach> coachList = new ArrayList<>();
            for (Integer el : set) {
                coachList.add(trainDAO.getCoach(el));
            }

            for (Coach coach : coachList) {
                List<Seat> seats = coach.getSeatList();
                List<Seat> seats1 = map.get(coach.getId());
                for (int j = 0; j < seats.size(); j++) {
                    Seat seat = seats.get(j);
                    Seat seat1 = seats1.get(j);
                    seat.setPrice(seat1.getPrice());
                    seat.setStatus(seat1.getStatus());
                }
            }

            return coachList;
        } catch (Exception e) {
            throw new ApplicationException("Extract coaches exception", e);
        }
    }

    public void setRouteStationsNames(List<Route> list) throws ApplicationException {
        try {
            for (Route el : list) {
                el.setFromName(stationDAO.getStation(el.getFromId()).getName());
                el.setToName(stationDAO.getStation(el.getToId()).getName());
            }
        } catch (DbInteractionException e) {
            logger.error("Set routes stations names error", e);
            throw new ApplicationException("Set route's stations' names exception", e);
        }
    }

    public void setRouteTrainName(List<Route> list) throws ApplicationException{
        try {
            for (Route el : list) {
                el.setTrainName(trainDAO.getTrain(el.getTrainId()).getName());
            }
        } catch (DbInteractionException e) {
            logger.error("Unable to set route's train name", e);
            throw new ApplicationException("Set route's train name exception", e);
        }
    }

    private Date parseDate(String s) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").parse(s);
    }
}
