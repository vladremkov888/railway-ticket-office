package com.company.app.service.implementation;

import com.company.app.database.DAO.TicketDAO;
import com.company.app.database.DAO.TrainDAO;
import com.company.app.database.DAO.Transaction;
import com.company.app.entity.Ticket;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.DbInteractionException;
import com.company.app.service.AbstractService;
import com.company.app.service.CartService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class CartServiceImpl extends AbstractService implements CartService {

    private final static Logger logger = LogManager.getLogger(CartServiceImpl.class);

    public List<Ticket> getTicketList(int userId) throws ApplicationException {
        try {
            return ticketDAO.getTickets(userId);
        } catch (DbInteractionException e) {
            logger.error("Unable to get ticket list");
            throw new ApplicationException("Get ticket list exception", e);
        }
    }

    public boolean buyTickets(List<Ticket> ticketList) throws ApplicationException {
        try {
            transaction.buyTickets(ticketList);
        } catch (DbInteractionException e) {
            logger.error("Unable to buy tickets");
            throw new ApplicationException("Buy tickets exception", e);
        }
        return true;
    }
}
