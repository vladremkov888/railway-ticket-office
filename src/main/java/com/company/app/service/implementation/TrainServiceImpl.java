package com.company.app.service.implementation;

import com.company.app.database.DAO.TrainDAO;
import com.company.app.database.DAO.Transaction;
import com.company.app.entity.Coach;
import com.company.app.entity.Train;
import com.company.app.exception.ApplicationException;
import com.company.app.exception.DbInteractionException;
import com.company.app.service.AbstractService;
import com.company.app.service.TrainService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrainServiceImpl extends AbstractService implements TrainService {
    private final static Logger logger = LogManager.getLogger(TrainServiceImpl.class);

    public TrainServiceImpl() {}

    public TrainServiceImpl(TrainDAO trainDAO, Transaction transaction) {
        this.trainDAO = trainDAO;
        this.transaction = transaction;
    }

    @Override
    public List<Train> getTrainList() throws ApplicationException {
        List<Train> trains;
        try {
            trains = trainDAO.getTrains();
            return trains;
        } catch (DbInteractionException e) {
            logger.error("Error during getting trains");
            throw new ApplicationException("Error during getting train list", e);
        }
    }

    public List<Train> getTrainListWithoutCoaches() throws ApplicationException {
        List<Train> trains;
        try {
            trains = trainDAO.getTrainsWithoutCoaches();
            return trains;
        } catch (DbInteractionException e) {
            logger.error("Error during getting trains");
            throw new ApplicationException("Error during getting train list", e);
        }
    }

    @Override
    public boolean addTrain(String name, String[] coaches) throws ApplicationException {
        if(coaches==null || coaches.length<1) {
            logger.error("Unable to add train. No coaches");
            throw new ApplicationException("no coaches exception");
        }
        Train train = new Train();
        List<Coach> coachList = new ArrayList<>();
        for (String el: coaches) {
            switch (el) {
                case "Berth"->coachList.add(Coach.getBerthCoach());
                case "Compartment"->coachList.add(Coach.getCompartmentCoach());
                case "DeLuxe"->coachList.add(Coach.getDeLuxeCoach());
                default -> {
                    logger.error("Unable to add train. Invalid coach -> "+el);
                    throw new ApplicationException("Invalid coach -> "+el);
                }
            }
        }
        train.setName(name);
        train.setCoachList(coachList);

        transaction.insertTrain(train);
        return true;
    }

    @Override
    public boolean editTrain() throws ApplicationException {
        return false;
    }

    @Override
    public boolean deleteTrain(String[] ids) throws ApplicationException {
        if (ids == null || ids.length<1) {
            return true;
        }
        List<Integer> list;
        try {
            list = Arrays.stream(ids).map(Integer::parseInt).toList();
        } catch (NumberFormatException e) {
            logger.debug("Unable to remove train");
            throw new ApplicationException("Remove train exception", e);
        }
        transaction.deleteTrains(list);
        return true;
    }
}
