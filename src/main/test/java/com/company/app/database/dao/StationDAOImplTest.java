package com.company.app.database.dao;

import com.company.app.database.DAO.StationDAO;
import com.company.app.database.DAO.mysql.StationDAOImpl;
import com.company.app.entity.Station;
import com.company.app.exception.DbInteractionException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class StationDAOImplTest {
    private static Connection con;

    private static StationDAOImpl stationDAO;
    private static final String CREATE_STATION_TABLE = "CREATE TABLE IF NOT EXISTS `railway_office_test`.`station` (" +
            "  `id` INT NOT NULL AUTO_INCREMENT," +
            "  `name` VARCHAR(255) NOT NULL," +
            "  PRIMARY KEY (`id`));";
    private static final String DROP_STATION_TABLE = "DROP TABLE station";

    @BeforeAll
    static void globalSetUp() throws SQLException {
        DataSource ds = DataSourceManager.getDataSource();
        con = ds.getConnection();
        stationDAO = new StationDAOImpl(ds);
    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
    }

    @BeforeEach
    void setUp() throws SQLException {
        con.createStatement().executeUpdate(CREATE_STATION_TABLE);
    }

    @AfterEach
    void tearDown() throws SQLException {
        con.createStatement().executeUpdate(DROP_STATION_TABLE);
    }

    @ParameterizedTest
    @MethodSource("testRegularStationCases")
    void testValidInputCases(Station s) throws DbInteractionException {
        assertTrue(stationDAO.insertStation(s));
    }

    @Test
    void testInvalidInputCase() {
        assertThrows(DbInteractionException.class, ()->stationDAO.insertStation(new Station()));
    }

    @Test
    void testGet() throws DbInteractionException {
        Station station1 = new Station("Name1");
        stationDAO.insertStation(station1);

        Station station2 = stationDAO.getStation("Name1");
        assertEquals(station1, station2);
        assertEquals(station1, stationDAO.getStation(station2.getId()));
    }

    @Test
    void testGetList() throws DbInteractionException {
        List<Station> stationList = Arrays.asList(
                new Station("Name1"),
                new Station("Name2"),
                new Station("Name3"),
                new Station("Name4")
        );

        for (Station x : stationList) {
            stationDAO.insertStation(x);
        }

        assertEquals(stationList, stationDAO.getStations());

        List<Station> stationList1 = new ArrayList<>(stationList);
        stationList1.add(new Station("Name5"));
        stationList1.add(new Station("Name6"));
        stationDAO.insertStation(new Station("Name5"));
        stationDAO.insertStation(new Station("Name6"));

        assertEquals(stationList1, stationDAO.getStations());
    }

    @Test
    void testRemove() throws DbInteractionException {
        Station station1 = new Station("Name1");
        Station station2 = new Station("Name2");
        stationDAO.insertStation(station1);
        stationDAO.insertStation(station2);

        assertEquals(2, stationDAO.getStations().size());

        stationDAO.removeStation(stationDAO.getStation("Name1").getId());

        assertEquals(1, stationDAO.getStations().size());

        stationDAO.insertStation(station1);
        int id = stationDAO.getStation("Name2").getId();
        stationDAO.removeStation(id);
        stationDAO.removeStation(id);
        stationDAO.removeStation(id);

        assertEquals(1, stationDAO.getStations().size());
    }

    @Test
    void testUpdate() throws DbInteractionException {
        Station station1 = new Station("Name1");
        Station station2 = new Station("Name2");
        stationDAO.insertStation(station1);

        int id = stationDAO.getStation("Name1").getId();
        station2.setId(id);
        stationDAO.updateStation(station2);

        assertNotEquals(station1, stationDAO.getStation(id));
        assertEquals(station2, stationDAO.getStation(id));
    }

    @Test
    void testRegularCases() throws DbInteractionException {
        stationDAO.insertStation(new Station("Name1"));
        stationDAO.insertStation(new Station("Name2"));
        stationDAO.insertStation(new Station("Name3"));

        List<Station> list = stationDAO.getStations();
        assertEquals(3, list.size());

        list.get(0).setName("1emaN");
        list.get(1).setName("2emaN");
        stationDAO.updateStation(list.get(0));
        stationDAO.updateStation(list.get(1));
        stationDAO.removeStation(list.get(2).getId());

        assertEquals(2, stationDAO.getStations().size());
        assertEquals(list.get(0), stationDAO.getStation("1emaN"));
        assertNull(stationDAO.getStation(list.get(2).getId()));
    }

    static Stream<Arguments> testRegularStationCases() {
        return Stream.of(
                new Station("Station1"),
                new Station("Station2"),
                new Station("Station3")
        ).map(Arguments::of);
    }
}
