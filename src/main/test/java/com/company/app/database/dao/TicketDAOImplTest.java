package com.company.app.database.dao;

import com.company.app.database.DAO.mysql.TicketDAOImpl;
import com.company.app.entity.Ticket;
import com.company.app.exception.DbInteractionException;
import org.junit.jupiter.api.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TicketDAOImplTest {
    private static Connection con;

    private static TicketDAOImpl ticketDAO;
    private static final String CREATE_TICKET_TABLE = "CREATE TABLE IF NOT EXISTS `railway_office_test`.`ticket` (" +
            "  `id` INT NOT NULL AUTO_INCREMENT," +
            "  `firstname` VARCHAR(255) NOT NULL," +
            "  `lastname` VARCHAR(255) NOT NULL," +
            "  `ticket_type` VARCHAR(45) NOT NULL," +
            "  `price` INT NOT NULL," +
            "  `seat_booking_route_id` INT NOT NULL," +
            "  `seat_booking_seat_id` INT NOT NULL," +
            "  `coach` INT NOT NULL," +
            "  `seat` INT NOT NULL," +
            "  `user_id` INT NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  INDEX `fk_passenger_seat_booking1_idx` (`seat_booking_route_id` ASC, `seat_booking_seat_id` ASC) VISIBLE," +
            "  INDEX `fk_passenger_user1_idx` (`user_id` ASC) VISIBLE," +
            "  CONSTRAINT `fk_passenger_seat_booking1`" +
            "    FOREIGN KEY (`seat_booking_route_id` , `seat_booking_seat_id`)" +
            "    REFERENCES `railway_office`.`seat_booking` (`route_id` , `seat_id`)" +
            "    ON DELETE CASCADE" +
            "    ON UPDATE CASCADE," +
            "  CONSTRAINT `fk_passenger_user1`" +
            "    FOREIGN KEY (`user_id`)" +
            "    REFERENCES `railway_office`.`user` (`id`)" +
            "    ON DELETE CASCADE" +
            "    ON UPDATE CASCADE);";
    private static final String DROP_TICKET_TABLE = "DROP TABLE ticket";

    @BeforeAll
    static void globalSetUp() throws SQLException {
        DataSource ds = DataSourceManager.getDataSource();
        con = ds.getConnection();
        ticketDAO = new TicketDAOImpl(ds);
    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
    }

    @BeforeEach
    void setUp() throws SQLException {
        con.createStatement().executeUpdate(CREATE_TICKET_TABLE);
    }

    @AfterEach
    void tearDown() throws SQLException {
        con.createStatement().executeUpdate(DROP_TICKET_TABLE);
    }

    @Test
    void testInsert() throws DbInteractionException {
        /*assertTrue(ticketDAO.insertTicket(getValidTicket()));
        assertThrows(DbInteractionException.class, ()->ticketDAO.insertTicket(getInvalidTicket()));*/
    }

    @Test
    void testGetList() throws DbInteractionException {
        /*Ticket ticket = getValidTicket();
        ticketDAO.insertTicket(ticket);
        ticketDAO.insertTicket(ticket);
        ticketDAO.insertTicket(ticket);

        List<Ticket> ticketList = ticketDAO.getTickets(ticket.getUser());
        assertEquals(3, ticketList.size());
        assertEquals(ticketList.get(0), ticket);*/
    }

    Ticket getValidTicket() {
        return new Ticket(0,
                "Firstname",
                "Lastname",
                "ticketType",
                1,
                2,
                3,
                4);
    }

    Ticket getInvalidTicket() {
        return new Ticket();
    }
}
