package com.company.app.database.dao;

import com.company.app.database.DAO.UserDAO;
import com.company.app.database.DAO.mysql.UserDAOImpl;
import com.company.app.entity.User;
import com.company.app.exception.DbInteractionException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class UserDAOImplTest {
    private static Connection con;
    private static DataSource ds;
    private static final String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS `railway_office_test`.`user` (" +
            "  `id` INT NOT NULL AUTO_INCREMENT," +
            "  `login` VARCHAR(64) NOT NULL," +
            "  `password` VARCHAR(32) NOT NULL," +
            "  `email` VARCHAR(255) NOT NULL," +
            "  `firstname` VARCHAR(64) NOT NULL," +
            "  `lastname` VARCHAR(64) NOT NULL," +
            "  `phone` VARCHAR(45) NULL," +
            "  `role` VARCHAR(45) NOT NULL," +
            "  PRIMARY KEY (`id`));";
    private static final String DROP_USER_TABLE = "DROP TABLE user";

    @BeforeAll
    static void globalSetUp() throws SQLException {
        ds = DataSourceManager.getDataSource();
        con = ds.getConnection();
    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
    }

    @BeforeEach
    void setUp() throws SQLException {
        con.createStatement().executeUpdate(CREATE_USER_TABLE);
    }

    @AfterEach
    void tearDown() throws SQLException {
        con.createStatement().executeUpdate(DROP_USER_TABLE);
    }

    @ParameterizedTest
    @MethodSource("testRegularUserCases")
    void testRegularInsertCases(User user) throws DbInteractionException {
        UserDAOImpl userDAO = new UserDAOImpl(ds);
        assertTrue(userDAO.insertUser(user));
    }

    @ParameterizedTest
    @MethodSource("testInvalidUserCases")
    void testInvalidInsertCases(User user) {
        UserDAOImpl userDAO = new UserDAOImpl(ds);
        assertThrows(DbInteractionException.class, () -> userDAO.insertUser(user));
    }

    @Test
    void testInsertAndGet() throws DbInteractionException {
        UserDAOImpl userDAO = new UserDAOImpl(ds);
        User user1 = new User(0,
                "login",
                "pass123",
                "Firstname",
                "Lastname",
                "email_em@mail.org",
                null);

        userDAO.insertUser(user1);
        user1.setRole("user");
        assertEquals(user1 ,userDAO.getUser("login"));

        User user2 = new User(0,
                "login2",
                "pass1234",
                "Firstname1",
                "Lastname1",
                "email_em1@mail.org",
                "+380000000000");
        userDAO.insertUser(user2);
        user2.setRole("user");
        assertEquals(user2, userDAO.getUser("login2"));
        assertEquals(user1, userDAO.getUser("login"));
        assertNull(userDAO.getUser("login3"));
    }

    static Stream<Arguments> testInvalidUserCases() {
        return Stream.of(
                new User(-1,
                        null,
                        null,
                        null,
                        null,
                        null,
                        "085788436746376"),
                new User(0,
                        "login",
                        "pass",
                        null,
                        null,
                        null,
                        null),
                new User()
        ).map(Arguments::of);
    }

    static Stream<Arguments> testRegularUserCases() {
        return Stream.of(
                new User(0,
                        "login",
                        "password",
                        "Firstname",
                        "Lastname",
                        "email@mail.com",
                        "+380000000000"),
                new User(0,
                        "login",
                        "password",
                        "Ім'я",
                        "Фамілія",
                        "email@mail.com",
                        null)
        ).map(Arguments::of);
    }
}
