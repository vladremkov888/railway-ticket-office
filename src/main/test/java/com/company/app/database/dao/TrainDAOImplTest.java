package com.company.app.database.dao;

import com.company.app.database.DAO.mysql.TrainDAOImpl;
import com.company.app.entity.Coach;
import com.company.app.entity.Seat;
import com.company.app.entity.Train;
import com.company.app.exception.DbInteractionException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class TrainDAOImplTest {
    private static Connection con;

    private static TrainDAOImpl trainDAO;
    private static final String CREATE_TRAIN_TABLE = "CREATE TABLE IF NOT EXISTS `railway_office_test`.`train` (" +
            "  `id` INT NOT NULL AUTO_INCREMENT," +
            "  `name` VARCHAR(255) NOT NULL," +
            "  PRIMARY KEY (`id`));";
    private static final String CREATE_COACH_TABLE = "CREATE TABLE IF NOT EXISTS `railway_office_test`.`coach` (" +
            "  `id` INT NOT NULL AUTO_INCREMENT," +
            "  `type` VARCHAR(255) NOT NULL," +
            "  `number` INT NOT NULL," +
            "  `train_id` INT NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  INDEX `fk_coach_train1_idx` (`train_id` ASC) INVISIBLE," +
            "  CONSTRAINT `fk_coach_train1`" +
            "    FOREIGN KEY (`train_id`)" +
            "    REFERENCES `railway_office`.`train` (`id`)" +
            "    ON DELETE CASCADE" +
            "    ON UPDATE CASCADE);";
    private static final String CREATE_SEAT_TABLE = "CREATE TABLE IF NOT EXISTS `railway_office_test`.`seat` (" +
            "  `id` INT NOT NULL AUTO_INCREMENT," +
            "  `number` INT NOT NULL," +
            "  `coach_id` INT NOT NULL," +
            "  PRIMARY KEY (`id`)," +
            "  INDEX `fk_seats_coach_idx` (`coach_id` ASC) VISIBLE," +
            "  CONSTRAINT `fk_seats_coach`" +
            "    FOREIGN KEY (`coach_id`)" +
            "    REFERENCES `railway_office`.`coach` (`id`)" +
            "    ON DELETE CASCADE" +
            "    ON UPDATE CASCADE);";
    private static final String DROP_TRAIN_TABLE = "DROP TABLE train";
    private static final String DROP_COACH_TABLE = "DROP TABLE coach";
    private static final String DROP_SEAT_TABLE = "DROP TABLE seat";

    @BeforeAll
    static void globalSetUp() throws SQLException {
        DataSource ds = DataSourceManager.getDataSource();
        con = ds.getConnection();
        trainDAO = new TrainDAOImpl(ds);
    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
    }

    @BeforeEach
    void setUp() throws SQLException {
        con.createStatement().executeUpdate(CREATE_TRAIN_TABLE);
        con.createStatement().executeUpdate(CREATE_COACH_TABLE);
        con.createStatement().executeUpdate(CREATE_SEAT_TABLE);
    }

    @AfterEach
    void tearDown() throws SQLException {
        con.createStatement().executeUpdate(DROP_TRAIN_TABLE);
        con.createStatement().executeUpdate(DROP_COACH_TABLE);
        con.createStatement().executeUpdate(DROP_SEAT_TABLE);
    }

    @ParameterizedTest
    @MethodSource("testValidTrainCases")
    void testInsertValidTrain(Train train) throws DbInteractionException {
        assertTrue(trainDAO.insertTrain(train));
    }

    @ParameterizedTest
    @MethodSource("testInvalidTrainCases")
    void testInsertInvalidTrain(Train train) {
        assertThrows(DbInteractionException.class, ()->trainDAO.insertTrain(train));
    }

    @ParameterizedTest
    @MethodSource("testValidCoachCases")
    void testInsertValidCoach(Coach coach) throws DbInteractionException {
        Random rnd = new Random();
        assertTrue(trainDAO.insertCoach(coach, rnd.nextInt(10)+1));
    }

    @ParameterizedTest
    @MethodSource("testInvalidCoachCases")
    void testInsertInvalidCoach(Coach coach) {
        Random rnd = new Random();
        assertThrows(DbInteractionException.class, ()->trainDAO.insertCoach(coach, rnd.nextInt(10)+1));
    }

    @ParameterizedTest
    @MethodSource("testValidSeatCases")
    void testInsertValidSeat(Seat seat) throws DbInteractionException {
        Random rnd = new Random();
        assertTrue(trainDAO.insertSeat(seat, rnd.nextInt(10)+1));
    }

    @Test
    void testRegularCases() throws DbInteractionException {
        Train train = new Train(0, "A300", new ArrayList<>());
        train.addCoach(new Coach(0, "berth", 1, new ArrayList<>()));
        train.addCoach(new Coach(0, "deluxe", 1, new ArrayList<>()));
        train.addCoach(new Coach(0, "compartment", 1, new ArrayList<>()));
        List<Coach> coachList = train.getCoachList();

        trainDAO.insertTrain(train);
        List<Coach> coachList1 = trainDAO.getCoaches(train.getId());
        assertNotEquals(coachList, coachList1);
        trainDAO.insertCoach(coachList.get(0), train.getId());
        trainDAO.insertCoach(coachList.get(1), train.getId());
        trainDAO.insertCoach(coachList.get(2), train.getId());
        List<Coach> coachList2 = trainDAO.getCoaches(train.getId());
        assertEquals(coachList, coachList2);

        for (Seat el: getValidSeats()) {
            trainDAO.insertSeat(el, coachList2.get(1).getId());
        }
        assertEquals(3, trainDAO.getSeatList(coachList2.get(1).getId()).size());
        trainDAO.insertTrain(new Train(0, "F980", null));
        List<Train> trains = trainDAO.getTrainsWithoutCoaches();
        assertEquals(2, trains.size());
        assertEquals(0, trains.get(0).getCoachList().size());
        assertEquals(0, trains.get(1).getCoachList().size());
    }

    static Stream<Arguments> testValidTrainCases() {
        return Stream.of(
                new Train(0, "R600", new ArrayList<>()),
                new Train(0, "W780", List.of(new Coach())),
                new Train(0, "M001", List.of(new Coach(), new Coach())),
                new Train(0, "G555", null)
        ).map(Arguments::of);
    }

    static Stream<Arguments> testInvalidTrainCases() {
        return Stream.of(
                new Train(0, null, new ArrayList<>()),
                new Train(0, null, null)
        ).map(Arguments::of);
    }

    static Stream<Arguments> testValidCoachCases() {
        return getValidCoaches().stream().map(Arguments::of);
    }

    static Stream<Arguments> testInvalidCoachCases() {
        Random rnd = new Random();
        return Stream.of(
                new Coach(0, null, rnd.nextInt(10), null),
                new Coach(0, null, -1, List.of(new Seat()))
        ).map(Arguments::of);
    }

    static Stream<Arguments> testValidSeatCases() {
        return getValidSeats().stream().map(Arguments::of);
    }

    static List<Seat> getValidSeats() {
        Random rnd = new Random();
        return List.of(
                new Seat(0, rnd.nextInt(30), 500, null ,-1),
                new Seat(0, rnd.nextInt(30), 700, null ,-1),
                new Seat(0, rnd.nextInt(30), 300, null ,-1)
        );
    }

    static List<Coach> getValidCoaches() {
        Random rnd = new Random();
        return List.of(
                new Coach(0, "berth", rnd.nextInt(10), null),
                new Coach(0, "deluxe", rnd.nextInt(10), new ArrayList<>()),
                new Coach(0, "compartment", rnd.nextInt(10), List.of(new Seat()))
        );
    }
}
