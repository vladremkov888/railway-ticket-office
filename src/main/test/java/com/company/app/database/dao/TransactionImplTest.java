package com.company.app.database.dao;

import com.company.app.database.DAO.Transaction;
import com.company.app.database.DAO.mysql.TransactionImpl;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class TransactionImplTest {
    private static Connection con;

    private static Transaction transaction;

    @BeforeAll
    static void globalSetUp() throws SQLException {
        DataSource ds = DataSourceManager.getDataSource();
        con = ds.getConnection();
        transaction = new TransactionImpl(
                null,
                null,
                null,
                null,
                null);
    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
    }

    @BeforeEach
    void setUp() throws SQLException {
        //TODO
    }

    @AfterEach
    void tearDown() throws SQLException {
        //TODO
    }
}