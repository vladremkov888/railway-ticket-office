package com.company.app.database.dao;

import com.company.app.database.DAO.mysql.BookingManagerImpl;
import com.company.app.database.utils.DbConstants;
import com.company.app.entity.Seat;
import com.company.app.exception.DbInteractionException;
import org.junit.jupiter.api.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BookingManagerImplTest {
    private static Connection con;

    private static BookingManagerImpl bookingManager;
    private static final String CREATE_SEAT_BOOKING_TABLE = "CREATE TABLE IF NOT EXISTS `railway_office_test`.`seat_booking` (" +
            "  `route_id` INT NOT NULL," +
            "  `seat_id` INT NOT NULL," +
            "  `seat_status` VARCHAR(45) NOT NULL," +
            "  `seat_price` INT NOT NULL," +
            "  `coach_id` INT NOT NULL," +
            "  PRIMARY KEY (`route_id`, `seat_id`))";
    private static final String DROP_SEAT_BOOKING_TABLE = "DROP TABLE seat_booking";

    @BeforeAll
    static void globalSetUp() throws SQLException {
        DataSource ds = DataSourceManager.getDataSource();
        con = ds.getConnection();
        bookingManager = new BookingManagerImpl(ds);
    }

    @AfterAll
    static void globalTearDown() throws SQLException {
        con.close();
    }

    @BeforeEach
    void setUp() throws SQLException {
        con.createStatement().executeUpdate(CREATE_SEAT_BOOKING_TABLE);
    }

    @AfterEach
    void tearDown() throws SQLException {
        con.createStatement().executeUpdate(DROP_SEAT_BOOKING_TABLE);
    }

    @Test
    void testBookSeat() throws DbInteractionException {
        Seat seat1 = new Seat(0, 1, -1, null, 1);
        assertTrue(bookingManager.bookSeat(1, "Compartment", seat1));
    }

    @Test
    void testInvalidBookSeat() {
        assertThrows(DbInteractionException.class, ()->bookingManager.bookSeat(-1, null, new Seat()));
    }

    @Test
    void testRemoveTrainFromRoute() throws DbInteractionException {
        Seat seat1 = new Seat(0, 1, 1);
        Seat seat2 = new Seat(1, 2, 1);
        Seat seat3 = new Seat(2, 3, 1);
        bookingManager.bookSeat(1, "Berth", seat1);
        bookingManager.bookSeat(1, "Berth", seat2);
        bookingManager.bookSeat(1, "Berth", seat3);

        assertEquals(3, bookingManager.getRouteSeats(1).size());
        bookingManager.removeTrainFromRoute(1);
        assertEquals(0, bookingManager.getRouteSeats(1).size());
    }

    @Test
    void testChangeSeatStatus() throws DbInteractionException {
        Seat seat1 = new Seat(0, 1, 1);
        Seat seat2 = new Seat(1, 2, 1);
        bookingManager.bookSeat(1, "Berth", seat1);
        bookingManager.bookSeat(1, "Berth", seat2);

        List<Seat> seatList = bookingManager.getRouteSeats(1);
        assertEquals("free", seatList.get(0).getStatus());
        assertEquals("free", seatList.get(1).getStatus());

        bookingManager.setSeatTypeToBusy(1, seatList.get(0).getId());
        bookingManager.setSeatTypeToBusy(1, seatList.get(1).getId());
        List<Seat> seatList1 = bookingManager.getRouteSeats(1);
        assertEquals("busy", seatList1.get(0).getStatus());
        assertEquals("busy", seatList1.get(1).getStatus());
    }

    @Test
    void testGetSeatPrice() throws DbInteractionException {
        Seat seat1 = new Seat(0, 1, 1);
        Seat seat2 = new Seat(1, 2, 1);
        bookingManager.bookSeat(1, "Compartment", seat1);
        bookingManager.bookSeat(1, "Berth", seat2);
        List<Seat> seatList = bookingManager.getRouteSeats(1);

        assertEquals(DbConstants.DEFAULT_COMPARTMENT_SEAT_PRICE, bookingManager.getSeatPrice(seatList.get(0).getId(), 1));
        assertEquals(DbConstants.DEFAULT_BERTH_SEAT_PRICE, bookingManager.getSeatPrice(seatList.get(1).getId(), 1));
    }
}
