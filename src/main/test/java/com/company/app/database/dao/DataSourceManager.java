package com.company.app.database.dao;

import org.apache.commons.dbcp2.BasicDataSource;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DataSourceManager {
    private static final BasicDataSource ds;

    static {
        try (InputStream input = new FileInputStream("src/main/resources/test_db.properties")) {
            Properties prop = new Properties();
            prop.load(input);

            ds = new BasicDataSource();
            ds.setUsername(prop.getProperty("user"));
            ds.setPassword(prop.getProperty("password"));
            ds.setUrl(prop.getProperty("url"));
            ds.setDriverClassName(prop.getProperty("driver"));
            ds.setMaxTotal(Integer.parseInt(prop.getProperty("max_total")));
            ds.setMaxIdle(Integer.parseInt(prop.getProperty("max_idle")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static DataSource getDataSource() {
        return ds;
    }
}
