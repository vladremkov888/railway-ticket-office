package com.company.app.filter;

import com.company.app.entity.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

import static org.mockito.Mockito.*;

public class DefaultLocaleFilterTest {
    private static DefaultLocaleFilter filter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain chain;
    private static HttpSession session;

    @BeforeAll
    static void init() throws ServletException {
        filter = new DefaultLocaleFilter();
        filter.init(null);
    }

    @AfterAll
    static void destroy() {
        filter.destroy();
    }

    @BeforeEach
    void setUp() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        chain = mock(FilterChain.class);
        session = mock(HttpSession.class);
    }

    @Test
    void testDoFilterNullLocale() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);;
        when(session.getAttribute("locale")).thenReturn(null);
        filter.doFilter(request, response, chain);

        verify(session).setAttribute("locale", new Locale("en"));
    }

    @Test
    void testDoFilterNotNullLocale() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("locale")).thenReturn(null);
        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }
}
