package com.company.app.filter;

import com.company.app.entity.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class AdminPanelAccessFilterTest {
    private static AdminPanelAccessFilter filter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain chain;
    private static HttpSession session;

    @BeforeAll
    static void init() throws ServletException {
        filter = new AdminPanelAccessFilter();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        chain = mock(FilterChain.class);
        session = mock(HttpSession.class);
        filter.init(null);
    }

    @AfterAll
    static void destroy() {
        filter.destroy();
    }

    @Test
    void testDoFilterAdminAccess() throws ServletException, IOException {
        User user = new User();
        user.setRole("admin");

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("user")).thenReturn(user);
        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }

    @Test
    void testDoFilterUserAccess() throws IOException {
        User user = new User();
        user.setRole("user");

        when(request.getSession()).thenReturn(session);
        when(request.getContextPath()).thenReturn("path");
        when(session.getAttribute("user")).thenReturn(user);
        filter.doFilter(request, response, chain);

        verify(response).sendRedirect("path/cabinet");
    }
}
