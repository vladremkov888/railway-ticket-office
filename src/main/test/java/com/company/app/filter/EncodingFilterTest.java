package com.company.app.filter;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class EncodingFilterTest {
    private static EncodingFilter filter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain chain;

    @BeforeAll
    static void init() throws ServletException {
        filter = new EncodingFilter();
        filter.init(null);
    }

    @AfterAll
    static void destroy() {
        filter.destroy();
    }

    @BeforeEach
    void setUp() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        chain = mock(FilterChain.class);
    }

    @Test
    void testDoFilter() throws ServletException, IOException {
        filter.doFilter(request, response, chain);

        verify(response).setCharacterEncoding("UTF-8");
        verify(chain).doFilter(request, response);
    }
}
