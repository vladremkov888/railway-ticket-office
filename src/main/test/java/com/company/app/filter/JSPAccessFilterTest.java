package com.company.app.filter;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class JSPAccessFilterTest {
    private static JSPAccessFilter filter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain chain;

    @BeforeAll
    static void init() throws ServletException {
        filter = new JSPAccessFilter();
        filter.init(null);
    }

    @AfterAll
    static void destroy() {
        filter.destroy();
    }

    @BeforeEach
    void setUp() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        chain = mock(FilterChain.class);
    }

    @Test
    void testDoFilter() throws IOException {
        when(request.getContextPath()).thenReturn("path");
        filter.doFilter(request, response, chain);

        verify(response).sendRedirect("path/cabinet");
    }
}