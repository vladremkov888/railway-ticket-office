package com.company.app.filter;

import com.company.app.entity.User;
import org.junit.jupiter.api.*;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class BookingAccessFilterTest {
    private static BookingAccessFilter filter;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static FilterChain chain;
    private static HttpSession session;

    @BeforeAll
    static void init() throws ServletException {
        filter = new BookingAccessFilter();
        filter.init(null);
    }

    @AfterAll
    static void destroy() {
        filter.destroy();
    }

    @BeforeEach
    void setUp() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        chain = mock(FilterChain.class);
        session = mock(HttpSession.class);
    }

    @Test
    void testDoFilterPostAdmin() throws ServletException, IOException {
        User user = new User();
        user.setRole("admin");

        when(request.getSession()).thenReturn(session);
        when(request.getMethod()).thenReturn("POST");
        when(request.getContextPath()).thenReturn("path");
        when(session.getAttribute("user")).thenReturn(user);
        filter.doFilter(request, response, chain);

        verify(response).sendRedirect("path/auth");
    }

    @Test
    void testDoFilterPostNullUser() throws IOException, ServletException {
        when(request.getSession()).thenReturn(session);
        when(request.getMethod()).thenReturn("POST");
        when(request.getContextPath()).thenReturn("path");
        when(session.getAttribute("user")).thenReturn(null);
        filter.doFilter(request, response, chain);

        verify(response).sendRedirect("path/auth");
    }

    @Test
    void testDoFilterGet() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getMethod()).thenReturn("GET");
        when(request.getContextPath()).thenReturn("path");
        filter.doFilter(request, response, chain);

        verify(chain).doFilter(request, response);
    }
}
