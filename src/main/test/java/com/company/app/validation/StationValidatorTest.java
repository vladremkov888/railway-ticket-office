package com.company.app.validation;

import com.company.app.entity.Station;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StationValidatorTest {
    @ParameterizedTest
    @MethodSource("validStationCases")
    void testValidCases(Station station) {
        StationValidator validator = new StationValidator(station);
        assertTrue(validator.isStationValid());
    }

    @ParameterizedTest
    @MethodSource("invalidStationCases")
    void testInvalidCases(Station station) {
        StationValidator validator = new StationValidator(station);
        assertFalse(validator.isStationValid());
    }

    static Stream<Arguments> validStationCases() {
        return Stream.of(
                new Station("Alfa"),
                new Station("Beta1"),
                new Station("G"),
                new Station("1"),
                new Station("12345678901234567890qwertyuiop")
        ).map(Arguments::of);
    }

    static Stream<Arguments> invalidStationCases() {
        return Stream.of(
                new Station(),
                new Station(""),
                new Station("12345678901234567890qwertyuiopl"),
                new Station("[]_/.,@`")
        ).map(Arguments::of);
    }
}
