package com.company.app.validation;

import com.company.app.entity.Station;
import com.company.app.entity.User;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserValidatorTest {
    @ParameterizedTest
    @MethodSource("validUserCases")
    void testValidCases(User user) {
        UserValidator validator = new UserValidator(user);
        assertTrue(validator.isUserValid());
    }

    @ParameterizedTest
    @MethodSource("invalidUserCases")
    void testInvalidCases(User user) {
        UserValidator validator = new UserValidator(user);
        assertFalse(validator.isUserValid());
    }

    static Stream<Arguments> validUserCases() {
        return Stream.of(
                new User(0,
                        "mrSmith999",
                        "abcd1234",
                        "Emanuyl",
                        "Smith",
                        "man_smith@mail.org",
                        "+380000000000"),
                new User(0,
                        "Av0kAdo3301",
                        "f6gs67dfghufhdu",
                        "Regny",
                        "Mollor",
                        "regmol@mail.ua",
                        "+380999999999")
        ).map(Arguments::of);
    }

    static Stream<Arguments> invalidUserCases() {
        return Stream.of(
                new User(0,
                        "",
                        "//,,?<<Ll`l[][]|Pkvihyy7",
                        "uy1",
                        "hfg",
                        "hgfgyg6g6ft",
                        "hfgfgfgdkdj"),
                new User(0,
                        null,
                        null,
                        "kira",
                        "lois",
                        "noils_mail.com",
                        "888887")
        ).map(Arguments::of);
    }
}
