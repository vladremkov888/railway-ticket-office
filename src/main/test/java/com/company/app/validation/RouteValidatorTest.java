package com.company.app.validation;

import com.company.app.entity.Route;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class RouteValidatorTest {

    @ParameterizedTest
    @MethodSource("validRouteCases")
    void testValidCases(Route route) {
        RouteValidator validator = new RouteValidator(route);
        assertTrue(validator.isRouteValid());
    }

    @ParameterizedTest
    @MethodSource("invalidRouteCases")
    void testInvalidCases(Route route) {
        RouteValidator validator = new RouteValidator(route);
        assertFalse(validator.isRouteValid());
    }

    static Stream<Arguments> validRouteCases() {
        Random rnd = new Random();
        List<Route> routes = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            routes.add(new Route(0,
                    new Date(rnd.nextInt(100)+100),
                    new Date(rnd.nextInt(100)),
                    (rnd.nextInt(10)+1),
                    (rnd.nextInt(10)+1),
                    (rnd.nextInt(10)+1)));
        }
        return routes.stream().map(Arguments::of);
    }

    static Stream<Arguments> invalidRouteCases() {
        Random rnd = new Random();
        return Stream.of(
                new Route(0, new Date(rnd.nextInt(100)+100), new Date(rnd.nextInt(100)), 0, 7, -1),
                new Route(0, new Date(rnd.nextInt(100)), new Date(rnd.nextInt(100)+100), 6, 8, 25),
                new Route(0, new Date(rnd.nextInt(100)+100), new Date(rnd.nextInt(100)), 99, -4, 0),
                new Route(0, new Date(rnd.nextInt(100)), new Date(rnd.nextInt(100)+100), 47, 12, 5)
        ).map(Arguments::of);
    }
}
