package com.company.app.service;

import com.company.app.database.DAO.BookingManager;
import com.company.app.database.DAO.RouteDAO;
import com.company.app.database.DAO.Transaction;
import com.company.app.entity.Route;
import com.company.app.exception.ApplicationException;
import com.company.app.service.implementation.RouteServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RouteServiceImplTest {
    private static RouteServiceImpl routeService;
    private static RouteDAO routeDAO;
    private static BookingManager bookingManager;
    private static Transaction transaction;

    @BeforeAll
    static void globalSetUp() {
        routeDAO = mock(RouteDAO.class);
        bookingManager = mock(BookingManager.class);
        transaction = mock(Transaction.class);
        routeService = new RouteServiceImpl(routeDAO, bookingManager, transaction);
    }

    @Test
    void testAddAndGet() throws ApplicationException, ParseException {
        Route route = new Route(0,
                parseDate("2000-11-12T11:10"),
                parseDate("2000-11-12T08:10"),
                2, 1, 2);
        Route route2 = new Route(1,
                parseDate("2000-11-12T10:10"),
                parseDate("2000-11-12T09:10"),
                2, 1, 2);
        when(routeDAO.insertRoute(route)).thenReturn(true);
        when(routeDAO.insertRoute(route2)).thenReturn(true);
        when(routeDAO.getRoutes()).thenReturn(List.of(route, route2), List.of(route, route2));

        assertTrue(routeService.addRoute("2000-11-12T11:10",
                "2000-11-12T08:10",
                "1", "2", "1"));
        assertTrue(routeService.addRoute("2000-11-12T10:10",
                "2000-11-12T09:10",
                "2", "1", "2"));
        assertEquals(2, routeService.getRouteList().size());
        assertEquals(List.of(route, route2), routeService.getRouteList());
    }

    @Test
    void testRemove() throws ApplicationException, ParseException {
        Route route = new Route(0,
                parseDate("2000-11-12T11:10"),
                parseDate("2000-11-12T08:10"),
                2, 1, 2);
        Route route2 = new Route(1,
                parseDate("2000-11-12T10:10"),
                parseDate("2000-11-12T09:10"),
                2, 1, 2);
        when(routeDAO.insertRoute(route)).thenReturn(true);
        when(routeDAO.insertRoute(route2)).thenReturn(true);
        when(routeDAO.deleteRoute(0)).thenReturn(true);
        when(routeDAO.deleteRoute(1)).thenReturn(true);
        when(routeDAO.getRoutes()).thenReturn(List.of(route, route2), List.of(route2), List.of());

        routeService.addRoute("2000-11-12T11:10",
                "2000-11-12T08:10",
                "1", "2", "1");
        routeService.addRoute("2000-11-12T10:10",
                "2000-11-12T09:10",
                "2", "1", "2");
        assertEquals(2, routeService.getRouteList().size());

        String[] strings = new String[] {"0"};
        routeService.deleteRoutes(strings);
        assertEquals(1, routeService.getRouteList().size());
        strings = new String[] {"1"};
        routeService.deleteRoutes(strings);
        assertEquals(0, routeService.getRouteList().size());
    }

    @Test
    void testEdit() throws ParseException, ApplicationException {
        Route route = new Route(0,
                parseDate("2000-11-12T11:10"),
                parseDate("2000-11-12T08:10"),
                2, 1, 2);
        Route route2 = new Route(1,
                parseDate("2000-11-12T10:10"),
                parseDate("2000-11-12T09:10"),
                2, 1, 2);
        Route route3 = new Route(2,
                parseDate("2000-11-12T07:10"),
                parseDate("2000-11-12T04:10"),
                2, 3, 4);
        when(routeDAO.insertRoute(route)).thenReturn(true);
        when(routeDAO.insertRoute(route2)).thenReturn(true);
        when(transaction.updateRoutes(List.of(route3))).thenReturn(true);
        when(routeDAO.getRoutes()).thenReturn(List.of(route, route2), List.of(route, route2));

        routeService.addRoute("2000-11-12T11:10",
                "2000-11-12T08:10",
                "1", "2", "1");
        routeService.addRoute("2000-11-12T10:10",
                "2000-11-12T09:10",
                "2", "1", "2");
        assertEquals(2, routeService.getRouteList().size());
        routeService.editRoutes(new String[]{"2000-11-12T07:10"},
                new String[]{"2000-11-12T04:10"},
                new String[]{"2"},
                new String[]{"3"},
                new String[]{"4"},
                new String[]{"1"});
        List<Route> routes = routeService.getRouteList();
        assertEquals(route2, routes.get(1));
    }

    private Date parseDate(String s) throws ParseException {
        String[] ss = s.split("T");
        String[] sss = ss[1].split(":");
        int hours = Integer.parseInt(sss[0]);
        int minutes = Integer.parseInt(sss[1]);
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(ss[0]);
        date.setTime(date.getTime() + hours * 3_600_000L + minutes * 60_000L);
        return date;
    }
}
