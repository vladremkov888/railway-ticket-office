package com.company.app.service;

import com.company.app.database.DAO.TrainDAO;
import com.company.app.database.DAO.Transaction;
import com.company.app.service.implementation.TrainServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

public class TrainServiceImplTest {
    private static TrainServiceImpl trainService;
    private static TrainDAO trainDAO;

    @BeforeAll
    static void globalSetUp() {
        trainDAO = mock(TrainDAO.class);
        Transaction transaction = mock(Transaction.class);
        trainService = new TrainServiceImpl(trainDAO, transaction);
    }

    @Test
    void testAddAndGetTrains() {

    }

    @Test
    void testEditTrain() {

    }

    @Test
    void testRemoveTrain() {

    }
}
