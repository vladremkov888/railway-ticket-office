package com.company.app.service;

import com.company.app.database.DAO.StationDAO;
import com.company.app.database.DAO.Transaction;
import com.company.app.entity.Station;
import com.company.app.exception.ApplicationException;
import com.company.app.service.implementation.StationServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class StationServiceImplTest {
    private static StationServiceImpl stationService;
    private static StationDAO stationDAO;

    @BeforeAll
    static void globalSetUp() {
        stationDAO = mock(StationDAO.class);
        Transaction transaction = mock(Transaction.class);
        stationService = new StationServiceImpl(stationDAO, transaction);
    }

    @Test
    void testAddAndGetStations() throws ApplicationException {
        Station station = new Station("Station1");
        Station station1 = new Station("Station2");
        when(stationDAO.insertStation(station)).thenReturn(true, true);
        when(stationService.getStationList()).thenReturn(List.of(station), List.of(station, station1));

        stationService.addStation("Station1");
        assertEquals(1, stationService.getStationList().size());
        stationService.addStation("Station2");
        List<Station> stations = stationService.getStationList();
        assertEquals(List.of(station, station1), stations);
    }

    @Test
    void testAddAndRemove() throws ApplicationException {
        Station station1 = new Station("Station1");
        Station station2 = new Station("Station2");
        Station station3 = new Station("Station3");
        when(stationDAO.insertStation(station1)).thenReturn(true);
        when(stationDAO.insertStation(station2)).thenReturn(true);
        when(stationDAO.insertStation(station3)).thenReturn(true);
        when(stationDAO.getStations()).thenReturn(List.of(station1, station2, station3), List.of(station2));
        when(stationDAO.removeStation(1)).thenReturn(true);
        when(stationDAO.removeStation(3)).thenReturn(true);

        stationService.addStation("Station1");
        stationService.addStation("Station2");
        stationService.addStation("Station3");
        List<Station> stations = stationService.getStationList();
        assertEquals(3, stations.size());

        String[] ids = new String[] {"1", "3"};
        stationService.deleteStations(ids);
        List<Station> stations1 = stationService.getStationList();
        assertEquals(1, stations1.size());
        assertEquals(station2, stations1.get(0));
    }

    @Test
    void testEditStations() throws ApplicationException {
        Station station1 = new Station("Station1");
        Station station2 = new Station("Station2");
        when(stationDAO.insertStation(station1)).thenReturn(true);
        when(stationDAO.insertStation(station2)).thenReturn(true);
        when(stationDAO.updateStation(new Station("Station3"))).thenReturn(true);
        when(stationDAO.updateStation(new Station("Station4"))).thenReturn(true);
        when(stationDAO.getStations()).thenReturn(List.of(new Station("Station3"), new Station("Station4")));

        stationService.addStation("Station1");
        stationService.addStation("Station2");
        String[] keys = new String[] {"1", "2"};
        String[] names = new String[] {"Station3", "Station4"};
        stationService.editStations(keys, names);

        List<Station> stations = List.of(new Station("Station3"), new Station("Station4"));
        assertEquals(stations, stationService.getStationList());
    }
}
