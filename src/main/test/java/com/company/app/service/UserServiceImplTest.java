package com.company.app.service;

import com.company.app.database.DAO.UserDAO;
import com.company.app.entity.User;
import com.company.app.exception.ApplicationException;
import com.company.app.service.implementation.UserServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserServiceImplTest {
    private static UserServiceImpl userService;
    private static UserDAO userDAO;

    @BeforeAll
    static void globalSetUp() {
        userDAO = mock(UserDAO.class);
        userService = new UserServiceImpl(userDAO);
    }

    @Test
    void testRegisterAndLogIn() throws ApplicationException {
        User user = getUser();
        when(userDAO.insertUser(user)).thenReturn(true);
        when(userDAO.getUser(user.getLogin())).thenReturn(null, user);

        assertEquals(user, userService.registerUser(user));
        assertEquals(user, userService.logInUser(user.getLogin(), user.getPassword()));
    }

    @Test
    void testInvalidCases() throws ApplicationException {
        User user = getInvalidUser();

        assertNull(userService.registerUser(user));
        assertNull(userService.logInUser("some-login", "some-password"));
    }

    User getUser() {
        return new User(
                0,
                "login",
                "password123",
                "Firstname",
                "Lastname",
                "email@mail.org",
                "+380000000000");
    }
    User getInvalidUser() {
        return new User(
                -1,
                ".?>:ijfhy",
                null,
                "Firstnamer99",
                "Lastnamer9488",
                "email_mail.org",
                "000000000000080000000000"
        );
    }
}
